function targetBlank() {
    // remove subdomain of current site's url and setup regex
    var internal = location.host.replace("www.", "");
    internal = new RegExp(internal, "i");

    var a = document.getElementsByTagName('a'); // then, grab every link on the page
    for (var i = 0; i < a.length; i++) {
        var href = a[i].host; // set the host of each link
        if (!internal.test(href)) { // make sure the href doesn't contain current site's host
            a[i].setAttribute('target', '_blank'); // if it doesn't, set attributes
        }
    }
};

function transformPlayer() {
    // Normal
    $("audio").wrap(function () {
        return "<div class='container-player' data-title='" + $(this).attr("alt") + "'></div>";
    });
    $(".container-player").wrap(function () {
        return "<div class='container-player2' data-title='" + $(this).attr("data-title") + "'></div>";
    });
    $("audio").removeAttr("controls");
    new GreenAudioPlayer.init({
        selector: '.container-player',
        showDownloadButton: true,
    });
    $('.container-player2').prepend(function () {
        return "<div class=player-title>" + $(this).attr("data-title") + '</div>';
    })
    $(".enable-player").removeClass("enable-player");

    // small
    $(".enable-player-small").wrap(function () {
        return "<div class='container-small-player' data-title='" + $(this).attr("alt") + "'></div>";
    });
    $(".enable-player-small").removeAttr("controls");
    new GreenAudioPlayer.init({
        selector: '.container-small-player',
        showDownloadButton: true,
    });
    $(".enable-player-small").removeClass("enable-player-small");
    $(".container-small-player").removeClass("container-small-player").addClass("container-small-player-enabled");
}

// TAB
function opentab(evt, tabname) {
    // Declare all variables
    var i, tabcontent, tablinks;
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("home-tab");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tab-link");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabname).style.display = "block";
    evt.currentTarget.className += " active";
}

// hide body if search
function update_search() {
    $('.tntsearch-field').on('input change keyup copy paste cut', function () {
        if (!this.value) {
            document.querySelector('.tntsearch-results').style.display = 'none';
            document.querySelector('#body').style.display = 'block';
        } else {
            document.querySelector('#body').style.display = 'none';
        }
    });
    $('.tntsearch-clear').on('click', function () {
        document.querySelector('.tntsearch-results').style.display = 'none';
        document.querySelector('#body').style.display = 'block';
    });
}

function check_search() {
    if ($('.tntsearch-field')[0].value) {
        document.querySelector('#body').style.display = 'none';
    }
}

// ADD target _blank to external website
$(document).ready(function () {
    targetBlank();
    transformPlayer();
    check_search();
    update_search();
    var selection_tab = document.getElementById('selection-tab-link')
    if (selection_tab) {
        selection_tab.click();
    }
    $('.btn-down').prepend('<i class="fa fa-download"></i>');
    $('.btn').prepend('<i class="fa fa-external-link"></i>');

});
