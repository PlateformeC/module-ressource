<?php
namespace Grav\Theme;

use Grav\Common\Grav;
use Grav\Common\Theme;
use RocketTheme\Toolbox\Event\Event;

class NixholTemplate extends Theme
{
    public static function getSubscribedEvents() {
        return [
            'onTNTSearchIndex' => ['onTNTSearchIndex',0],
            'onTNTSearchQuery' => ['onTNTSearchQuery', 1000],
        ];
    }

    public function onTNTSearchIndex(Event $e)
    {
        $fields = $e['fields'];
        $page = $e['page'];
        $taxonomy = $page->taxonomy();
        if (isset($taxonomy['tag'])) {
            $fields->tag = implode(",", $taxonomy['tag']);
        }
        
        if (isset($taxonomy['author'])) {
            $fields->author = implode(",",$taxonomy['author']);
        }
    }
    
    public function onTNTSearchQuery(Event $e) 
    {
        $query = $this->grav['uri']->param('q');

        if ($query) {

            $page = $e['page'];
            $query = $e['query'];
            $options = $e['options'];
            $fields = $e['fields'];
            
            $gtnt = $e['gtnt'];

            $content = $gtnt->getCleanContent($page);
            $title = $page->title();

            $relevant = $gtnt->tnt->snippet($query, $content, $options['snippet']);

            if (strlen($relevant) <= 6) {
                $relevant = substr($content, 0, $options['snippet']);
            }
            
            //$fields->results[] = $page->route();
            $fields->hits[] = [
                'link' => $page->route(),
                /*'title' =>  $gtnt->tnt->highlight($title, $query, 'em', ['wholeWord' => false]),*/
                'content' =>  $gtnt->tnt->highlight($relevant, $query, 'em', ['wholeWord' => false]),
            ];
            $e->stopPropagation();
        }
    }

}
?>
