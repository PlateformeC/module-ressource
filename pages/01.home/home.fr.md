---
title: Ressources
feature1: /fiches/publication_atelier_partagé
feature2: /fiches/lieux-numeriques-entre-pratiques-populaires-et-re-appropriation-des-technologies
feature3: '/fiches/sun14_octobre20_instagram et censure des corps'
feature4: /fiches/publication_slowtech
feature5: /fiches/brodeuse_numerique_brothernv800e
feature6: /home
feature7: /fiches/publication_pfc
feature8: /fiches/archives_radio
feature9: /fiches/culture_libre_et_propriete_industrielle
feature10: /fiches/manifeste_pour_la_culture_numerique
feature11: /fiches/sun13_septembre20_aidants_numeriques
feature12: /fiches/mac_romain_le_mini_maker
body_classes: 'title-h1h2 header-dark header-transparent'
menu: Home
aura:
    pagetype: website
onpage_menu: false
content:
    items:
        -
            '@page.descendants': /fiches
    limit: 15
    order:
        by: date
        dir: desc
    pagination: true
recherches:
    items:
        -
            '@taxonomy.category': recherches
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
pratiques:
    items:
        -
            '@taxonomy.category': pratiques
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
metadata:
    'og:url': 'https://test.ressource.pingbase.net/test2/home'
    'og:type': website
    'og:title': 'Ressources | Espace Ressources Num&eacute;riques'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Ressources | Espace Ressources Num&eacute;riques'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'article:published_time': '2020-12-02T09:21:41+01:00'
    'article:modified_time': '2020-12-02T09:21:41+01:00'
    'article:author': Ping
---

