---
title: 'Mentions Légales'
aura:
    pagetype: website
metadata:
    'og:url': 'https://test.ressource.pingbase.net/test2/mentions-legales'
    'og:type': website
    'og:title': 'Mentions L&eacute;gales | Espace Ressources Num&eacute;riques'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Mentions L&eacute;gales | Espace Ressources Num&eacute;riques'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'article:published_time': '2020-12-11T15:40:30+01:00'
    'article:modified_time': '2020-12-11T15:40:30+01:00'
    'article:author': Ping
---

# Mentions Légales

## Éditeur

PiNG Association loi 1901 déclarée à la préfecture de Loire-Atlantique le 10 mars 2004
Numéro de récépissé en préfecture : W442001837 N° Siret : 453 508 319 00036 Code APE : 9499Z
Siège social : Pôle associatif du 38 Breil 38 rue du Breil – 44100 Nantes
Directeur de la publication : Ronan Marjolet Responsable éditorial : PiNG

## Réalisation

Ce site web a été réalisé par [Nicolas Montgermont](https://nimon.org/). Il utilise le CMS open source GRAV. L'hébergeur du site ressources.pingbase.net est la société OVH dont le siège social est situé au 2 rue Kellermann, 59100 Roubaix. L'infogérence est réalisée par la société [AUKFOOD](https://www.aukfood.fr/) dont le siège social est situé au 123 Boulevard Louis Blanc, 85000 La Roche-sur-Yon.

## Propriété intellectuelle

Sauf mention contraire, ce site et tous les contenus qui y figurent sont placés sous licence libre Creative Commons BY-NC-SA, ce qui signifie que vous êtes libres : ▸ de reproduire, distribuer et communiquer cette création au public, ▸ de modifier cette création,  sous les conditions suivantes :

Paternité : vous devez citer le nom de l’auteur original de la manière indiquée par l’auteur de l’oeuvre ou le titulaire des droits qui vous confère cette autorisation (mais pas d’une manière qui suggérerait qu’ils vous soutiennent ou approuvent votre utilisation de l’œuvre).

Pas d’utilisation commerciale : vous n’avez pas le droit d’utiliser cette création à des fins commerciales.

À chaque réutilisation ou distribution de cette création, vous devez faire apparaître clairement au public les conditions contractuelles de sa mise à disposition (CC).

## Cookies

L’Utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement sur son logiciel de navigation. En naviguant sur le site, il les accepte. Un cookie est un élément qui ne permet pas d’identifier l’Utilisateur mais sert à enregistrer des informations relatives à la navigation de celui-ci sur le site Internet. L’Utilisateur pourra désactiver ce cookie par l’intermédiaire des paramètres figurant au sein de son logiciel de navigation.

## Limitation de responsabilité

Ce site comporte des informations mises à disposition par des sociétés externes ou des liens hypertextes vers d’autres sites qui n’ont pas été développés par PiNG. Le contenu mis à disposition sur le site est fourni à titre informatif. L’existence d’un lien de ce site vers un autre site ne constitue pas une validation de ce site ou de son contenu. Il appartient à l’internaute d’utiliser ces informations avec discernement et esprit critique. La responsabilité de PiNG ne saurait être engagée du fait des informations, opinions et recommandations formulées par des tiers.

PiNG ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications techniques requises, soit de l’apparition d’un bug ou d’une incompatibilité.

PiNG ne pourra également être tenue responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site.

## Limitations contractuelles sur les données techniques

PiNG ne pourra être tenue responsable de dommages matériels liés à l’utilisation du site. De plus, l’utilisateur du site s’engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis à jour.
