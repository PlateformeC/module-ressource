---
title: Récits
media_order: bg_hero_klb4.jpg
body_classes: 'title-h1h2 header-dark header-transparent'
aura:
    pagetype: website
anchors:
    active: true
tagtitle: h2
hero_overlay: true
hero_showsearch: true
show_sidebar: false
show_searchsidebar: false
show_breadcrumbs: true
show_pagination: true
content:
    items:
        -
            '@taxonomy.category': recits
    limit: 9
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
metadata:
    'og:url': 'http://localhost/ressources.pingbase.net/recits'
    'og:type': website
    'og:title': 'R&eacute;cits | Espace Ressources Num&eacute;riques'
    'og:image': 'http://localhost/ressources.pingbase.net/recits/bg_hero_klb4.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1920'
    'og:image:height': '1455'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'R&eacute;cits | Espace Ressources Num&eacute;riques'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'http://localhost/ressources.pingbase.net/recits/bg_hero_klb4.jpg'
    'article:published_time': '2020-11-27T16:38:32+00:00'
    'article:modified_time': '2020-11-27T16:38:32+00:00'
    'article:author': Ping
---

#RÉCITS
