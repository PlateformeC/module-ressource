---
title: 'Les doigts dans la prise #10 : la reconnaissance faciale'
type_ressource: audio
feature_image: woman-with-a-mask-lorenzo-lippi.jpg
license: cc-by-nc-sa
published: true
date: '13-01-2020 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Meven Marchand Guidevay'
aura:
    pagetype: article
    description: 'Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne Le Fil de l’histoire.'
    image: woman-with-a-mask-lorenzo-lippi.jpg
metadata:
    description: 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/sun10_janvier20_reconnaissance%20faciale'
    'og:type': article
    'og:title': 'Les doigts dans la prise #10 : la reconnaissance faciale | Espace Ressources Num&eacute;riques'
    'og:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/sun10_janvier20_reconnaissance%20faciale/woman-with-a-mask-lorenzo-lippi.jpg'
    'og:image:type': image/webp
    'og:image:width': '725'
    'og:image:height': '711'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #10 : la reconnaissance faciale | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/sun10_janvier20_reconnaissance%20faciale/woman-with-a-mask-lorenzo-lippi.jpg'
    'article:published_time': '2020-01-13T16:31:00+01:00'
    'article:modified_time': '2020-12-09T12:57:26+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Pour écouter le podcast c’est par [ici](http://www.lesonunique.com/content/les-doigts-dans-la-prise-la-reconnaissance-faciale) !

![Les doigts dans la prise - La reconnaissance faciale](les_doigts_dans_la_prise_01.mp3)

C’est sorti comme ça, un 24 décembre juste avant la bûche. Malgré l’échec cuisant des projets de portiques de reconnaissance faciale à l’entrée de lycées de Nice et Marseille, jugés illégaux par la CNIL, Cédric O, secrétaire d’État au numérique, a annoncé au Parisien l’ouverture d’une phase d’expérimentation, de six mois à un an, de la reconnaissance faciale en temps réel sur les images de vidéosurveillance.1

Mais au fait, c’est quoi, la reconnaissance faciale ? La reconnaissance faciale, c’est un ensemble de techniques et de logiciels qui permettent d’automatiser l’identification des visages dans des images (des photographies ou des vidéos). On peut en distinguer deux types, d’une part la comparaison faciale en 2D, qui permet de reconnaître une personne fichée par comparaison avec une image gabarit. Et d’autre part l’identification à la volée qui est une reconnaissance faciale en direct dans l’espace public par la modélisation du visage qui est ensuite recherché dans une base de données.2

D’après Martin Drago, juriste et membre de l’Association La Quadrature du Net, « Lorsque Cédric O dit qu’il voudrait que cette technologie soit testée en France, c’est trompeur. La police et la gendarmerie ont déjà recours à la reconnaissance faciale. Elles utilisent le fichier Traitement des antécédents judiciaires (TAJ), qui regroupe des millions de personnes, et le croisent avec des photos de suspects pour tenter de les identifier. La vraie nouveauté ici est la notion de temps réel. »3

Dans un contexte mondial de multiplication des mouvements sociaux, l’obsession sécuritaire n’est certainement pas pour rien à l’empressement du gouvernement à la mise en place de cette technologie. Pourtant, la première raison semble économique : La patrie des droits de l’Homme, coutumière de l’exportation de son savoir-faire du maintient de l’ordre, de la Libye à Hong Kong4, ne compte pas se laisser distancer sur le marché florissant de la reconnaissance faciale, pourtant déjà bien occupé par les champions américains et chinois.

Mais voilà, comme le secrétaire d’État l’affirme : il faut « expérimenter pour que nos industries progressent ». Il est difficile de savoir s’il a acquis cette certitude lors de ses trois années chez Safran, qui développait alors sa filiale MorphoFACE, devenue depuis Idemia, l’un des leaders français de la biométrie.5 Mais quoi qu’il en soit, l’annonce du 24 décembre avait de quoi sonner comme un beau cadeau aux industriels avides de tests grandeur nature, afin de pouvoir entraîner leurs algorithmes sur de larges échantillons de population.

Sur le papier, cette période de test devrait se conclure par un débat public, sur le modèle des états généraux de la bioéthique. Mais l’on sait déjà que pour le président de l’Association nationale de la vidéo protection, la bonne question n’est pas d’autoriser ou non la reconnaissance faciale, mais de la définir « à la française ».6

Refusant des débats biaisés qui se positionnent déjà dans l’optique d’une utilisation généralisée de ces technologies, L’Observatoire des Libertés Numériques, La Quadrature du Net et 80 organisations dont la Ligue des Droits de l’Homme ont signé une [lettre commune](https://www.laquadrature.net/2019/12/19/rf_securitaire/) demandant l’interdiction de toutes les pratiques de reconnaissance faciale sécuritaire, présentes et à venir. Dans le même mouvement, avec la campagne [Technopolice](https://technopolice.fr/), des associations et syndicats soutiennent l’émergence et l’organisation de groupes locaux afin de lutter contre le développement de l’informatique de contrôle et de ses dispositifs.

Bien sûr, on entend de loin celles et ceux qui affirment qu’un modèle français de la reconnaissance faciale ne serait tout de même pas comparable à la dystopie chinoise (une caméra pour deux habitants). D’autres ont essayé, par exemple, en Inde, où la police expérimentait en 2018 la reconnaissance faciale sous le prétexte sympathique de la recherche des enfants perdus. Depuis 2019, le même logiciel y est utilisé pour repérer les participants récurrents aux manifestations anti-gouvernementales.7 Pas besoin d’être aussi bon logicien que Pythagore pour savoir comme lui que *« tout se répercute »* !

1 PIQUET, Caroline, Reconnaissance faciale : Cédric O n’est «pas certain» qu’Alicem soit un jour déployée, Le Parisien, 24/10/19, URL: http://www.leparisien.fr/high-tech/reconnaissance-faciale-cedric-o-n-est-pas-certain-qu-alicem-soit-un-jour-deployee-24-12-2019-8223705.php

2 TRÉGUER, Félix, La technopolice progresse partout, CQFD n°183, janvier 2020, p. 12

3 DONT, Barthélemy, Reconnaissance faciale en temps réel: «Nous ne voulons pas l’encadrer, nous voulons l’interdire», Korii, 31/12/19 URL: https://korii.slate.fr/et-caetera/reconnaissance-faciale-temps-reel-cedric-o-quadrature-du-net-interview-martin-drago

4 Amnesty International, Hong Kong, un inquiétant déploiement de canons à eau, 12/08/19 URL: https://www.amnesty.fr/controle-des-armes/actualites/hong-kong–un-inquietant-deploiement-de-canons-a-eau

5 https://twitter.com/oliviertesquet/status/1211262381881511936

6 TESQUET, Olivier, La face cachée de la reconnaissance faciale, Télérama n°3648, 14/12/19, p. 22

7 KOTOKY, Anurag, Police Use Face-Recognition Software as India Protests Intensify, Bloomberg, 28/12/19, URL : https://www.bloomberg.com/amp/news/articles/2019-12-28/police-use-face-recognition-software-as-india-protests-intensify
