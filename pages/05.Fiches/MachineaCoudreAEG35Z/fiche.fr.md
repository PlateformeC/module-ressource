---
title: 'Machine à coudre AEG35Z'
media_order: 340px-Aeg35z.jpg
type_ressource: text
feature_image: 340px-Aeg35z.jpg
license: cc-by-nc-sa
date: '01-01-2019 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
        - couture
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cet outil sert à la couture.'
    image: 340px-Aeg35z.jpg
metadata:
    description: 'Cet outil sert &agrave; la couture.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/machineacoudreaeg35z'
    'og:type': website
    'og:title': 'Machine &agrave; coudre AEG35Z | Espace Ressources Num&eacute;riques'
    'og:description': 'Cet outil sert &agrave; la couture.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/machineacoudreaeg35z/340px-Aeg35z.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '340'
    'og:image:height': '246'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Machine &agrave; coudre AEG35Z | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cet outil sert &agrave; la couture.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/machineacoudreaeg35z/340px-Aeg35z.jpg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T15:05:25+01:00'
    'article:author': Ping
---

*Cet outil sert à la couture.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/MachineaCoudreAEG35Z?classes=btn)
