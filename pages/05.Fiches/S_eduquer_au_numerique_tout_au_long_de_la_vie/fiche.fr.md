---
title: 'S''éduquer au numérique tout au long de la vie !'
type_ressource: text
feature_image: c2i2e_1.png
license: cc-by-nc-sa
date: '25-03-2014 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'médiation numérique'
        - 'espace numérique'
        - 'pass numérique'
    author:
        - 'Guy Pastre'
aura:
    pagetype: website
    description: 'Un article proposé par Guy Pastre, chargé de missions de la M@ison TIC (espace numérique à Grigny) et animateur du réseau CoRAIA (Coordination Rhône-Alpes de l’Internet Accompagné).'
    image: c2i2e_1.png
anchors:
    active: false
tagtitle: h2
hero_overlay: true
hero_showsearch: true
show_breadcrumbs: true
content:
    items:
        - '@self.children'
    limit: 12
    order:
        by: date
        dir: desc
    pagination: true
metadata:
    description: 'Un article propos&eacute; par Guy Pastre, charg&eacute; de missions de la M@ison TIC (espace num&eacute;rique &agrave; Grigny) et animateur du r&eacute;seau CoRAIA (Coordination Rh&ocirc;ne-Alpes de l&rsquo;Internet Accompagn&eacute;).'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/s_eduquer_au_numerique_tout_au_long_de_la_vie'
    'og:type': website
    'og:title': 'S''&eacute;duquer au num&eacute;rique tout au long de la vie ! | Espace Ressources Num&eacute;riques'
    'og:description': 'Un article propos&eacute; par Guy Pastre, charg&eacute; de missions de la M@ison TIC (espace num&eacute;rique &agrave; Grigny) et animateur du r&eacute;seau CoRAIA (Coordination Rh&ocirc;ne-Alpes de l&rsquo;Internet Accompagn&eacute;).'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/s_eduquer_au_numerique_tout_au_long_de_la_vie/c2i2e_1.png'
    'og:image:type': image/png
    'og:image:width': '1200'
    'og:image:height': '754'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'S''&eacute;duquer au num&eacute;rique tout au long de la vie ! | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Un article propos&eacute; par Guy Pastre, charg&eacute; de missions de la M@ison TIC (espace num&eacute;rique &agrave; Grigny) et animateur du r&eacute;seau CoRAIA (Coordination Rh&ocirc;ne-Alpes de l&rsquo;Internet Accompagn&eacute;).'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/s_eduquer_au_numerique_tout_au_long_de_la_vie/c2i2e_1.png'
    'article:published_time': '2014-03-25T00:00:00+01:00'
    'article:modified_time': '2020-11-24T15:04:23+01:00'
    'article:author': Ping
---

*Un article proposé par Guy Pastre, chargé de missions de la M@ison TIC (espace numérique à Grigny) et animateur du réseau CoRAIA (Coordination Rhône-Alpes de l’Internet Accompagné).*

## De la neutralité bienveillante…

Ne pas se cantonner à une simple formation aux pratiques numériques et porter l’ambition de  la construction d’une culture numérique partagée : tel est l’impératif auquel doivent répondre les passeurs professionnels que nous sommes, en lien avec l’ensemble des citoyens de la société devenue “numérique”.
Déclinée sous formes différentes, cette aspiration est partagée sur le principe par l’ensemble des acteurs “directs” de la communauté élargiedes Espaces Publics Numériques (EPN), malgré l’absence de soutien et de reconnaissance de l’enjeu fondamental que cela représente par les acteurs politiques et institutionnels.  

Un virage sémantique pour certains, un changement de paradigme pour d’autres, s’est opéré à Ajaccio lors des [Assises de la médiation numérique en 2011](http://assmednum.corse.fr/)). Durant ces rencontres une nouvelle posture transversale pluri-disciplinaire territorialisée a pris corps et sens : la médiation numérique.  Cette évolution est vécue, par une profession en mal de reconnaissance, comme majeure et valorisante car elle marque le passage de l’animateur multimédia techno-centré dans son lieu à celui de médiateur numérique ressource agile de son territoire.

Toutefois, passé le premier enthousiasme collectif, il convient d’être vigilant à ce que ce virage vers des missions et des postures d’animation “socio-numérique systémique” se vive comme une émulation renouvelée des actions de chacun, et non comme une injonction stigmatisante. En effet la tentation (avérée) serait de glisser vers un discours incantatoire élitiste, coupé de la réalité plurielle des animateurs et de leurs publics, en pointant du doigt les EPN “traditionnels” comme des structures de seconde zone “hors du coup”.

Au coeur de ces changements se pose la question centrale de la montée en compétences numériques (personnelles, citoyennes ou professionnelles). Celle-ci demande d’intégrer la diversité des lieux, la complémentarité des acteurs et la pluralité des approches dans la perspective d’un écosystème ouvert, apprenant et émancipateur. (cf ce rapport du [CNNum](http://www.cnnumerique.fr/inclusion/) par exemple).
Permettre à chacun de se construire un parcours d’usages dans une démarche d’appropriation des technologies (cf Serge Proulx - [Penser les usages des TIC aujourd'hui](https://www.google.fr/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0CDAQFjAA&url=http%3A%2F%2Fsergeproulx.uqam.ca%2Fwp-content%2Fuploads%2F2010%2F12%2F2005-proulx-penser-les-usa-43.pdf&ei=53wxU_qrD8GM0AWDwICIAw&usg=AFQjCNF5KtcfQWmlX8uQiiVDDdEQl1VYRg&bvm=bv.63587204,d.d2k) dans un dispositif de droit commun reste une nécessité récurrente au regard d’une réalité technologique en évolution permanente.
Dit autrement, il faudra toujours garantir à l’ensemble de la population  l'accès aux compétences numériques de base et à leur réactualisation de façon massive et permanente. Ce travail de fond prendra des formes renouvelées et l'enjeu de massification permanente oblige à penser des accompagnements renouvelés. À ce titre s'intéresser aujourd’hui par exemple à la dynamique des MOOC n’est pas dépourvu de sens pour des médiateurs numériques, en les interrogeant sur leur culture voir leurs compétences professionnelles actuelles.

Cette montée en compétences s’inscrit comme une condition nécessaire à l’appréhension du numérique, à la fois par les enjeux sociaux et éducatifs dont il est porteur, autant, voir davantage que par ses aspects socio-techniques.
C’est en sens qu’il me semble nécessaire et pertinent d’inscrire alors ce processus dans une la démarche “engagée” d’éducation tout au long de la vie, visant à l’autonomie et à l’émancipation de l’individu. Mais aujourd’hui, il me semble qu’elle est encore majoritairement traitée en creux et “par chapelle”.


## ...à la subjectivité militante

La démarche d’éducation tout au long de la vie nécessite, entre autre, un accompagnement qui permette à chacun de se construire des clés de lecture et une compréhension du monde et de la société à l’ère du numérique. Cette construction fait référence, intègre, bouleverse les systèmes de valeurs, aspirations politiques, sociales…personnelles (individuelles) et collectives.
Je constate par expérience qu’aujourd’hui l’évolution vers cette posture de “facilitateur” dans l’appropriation éducative et sociale des technologies par les citoyens bouscule de nombreux médiateurs (ou animateurs, selon le terme dans lequel ils se reconnaissent).  En effet, cela les amène sur des terrains éloignés du pur registre technique et les oblige à clarifier, expliciter et décortiquer des terrains jalonnés de parti pris politiques, idéologiques, éthiques,..
C’est ce que nous avons pu observer en Rhône-Alpes à travers les interrogations et les réticences lors des journées d’accompagnement des EPN à la mise en place du Pass Numérique Citoyen par la Région (Dispositif régional lancé en 2013 visant à délivrer des heures de culture numérique citoyenne via les acteurs de l’Internet accompagné).

Permettre aux animateurs d’EPN de faire évoluer les postures et les missions nécessite à la fois de :
- renforcer une identité professionnelle,
- consolider et élargir une culture professionnelle,
- s’approprier et maîtriser les enjeux sociétaux,
- élaborer des stratégies pédagogiques, éducatives et partenariales pour faire évoluer les postures et les missions.

Ce sont les axes que nous essayons d’explorer et de faire avancer en Rhône-Alpes à travers une action conjuguée de la [Région](http://www.solidnum.rhonealpes.fr/), de la [CoRAIA](http://www.coraia.org/) et de son réseau de partenaires.
Cette approche trouve également tout son sens en se mettant en perspective et en synergie avec le chantier de la culture numérique de base du citoyen.

Deux projets peuvent illustrer la façon d’avancer sur ces axes : le [Pass Numérique Citoyen](http://www.coraia.org/?page_id=4012) et la communauté de pratique [Formavia “ Culture numérique pour tous”](http://c2i2e.formavia.fr/).


## Le Pass Numérique

Ce dispositif, inscrit dans le volet Solidarité Numérique de la Politique Numérique Régionale, est début 2014 en phase de test.

Il a une double vocation:
- délivrer dix heures de culture numérique de base à chaque citoyen sur des thématiques identifiées comme prioritaires aujourd’hui,
- placer les EPN au coeur d’un dispositif leur donnant la possibilité de nouer des partenariats nouveaux et de s’adresser à des publics différents avec le soutien des ERA (Espaces Rhône-Alpes) de chaque département.

![passnum.png](passnum.png)

La CoRAIA a pour mission d’accompagner les animateurs à s’approprier le contenu des 4 thématiques ciblées :
- [gérer son identité numérique et protéger sa vie privée](http://www.coraia.org/?page_id=4256)
- [accéder à l’information, la qualifier, la produire](http://www.coraia.org/?page_id=4268)
- [mener des démarches avec l’administration, échanger avec les collectivités et l’état](http://www.coraia.org/?page_id=4271)
- [contribuer au développement du lien social local](http://www.coraia.org/?page_id=4274)

Les journées d’accompagnement des EPN permettent de confirmer que le Pass Numérique peut être un levier supplémentaire pour faciliter le processus d’évolution des EPN dans leurs postures et leurs missions grâce à la force et la légitimité institutionnelle de son portage par la Région.

Pour le moment, à travers le Pass Numérique, sans que cela ne soit l’objectif premier qui reste centré sur le citoyen et sa culture numérique, des effets sont observés sur la posture des EPN et des animateurs sur leur territoire : interrogations partagées autour des postures professionnelles, formation mutualisée sur les enjeux socio-numériques, mise en réseau élargie de parties-prenantes à l’accompagnement socio-éducatif, développement de compétences spécifiques (en fonction de leur sensibilité, leurs partenaires, leur territoire, leurs responsables et élus).

## Culture numérique professionnelle de base

La réflexion autour de ce que devrait être une culture numérique professionnelle de base pour les acteurs de la formation continue, de la médiation numérique et de façon élargie l’ensemble des acteurs concernés par la question de l’accompagnement au développement des usages numériques (acteurs du champ socio-éducatif, de l’insertion, de l’orientation, etc.) est née au sein du réseau régional des acteurs de la formation continue tout au long de la vie Formavia.
Certains réseaux d’EPN et la CoRAIA se sont engagés dans les travaux du réseau depuis 2007 ([pour en savoir plus](http://fr.slideshare.net/formavia/barcampclermontdec2013)).

Un des projets les plus récents a été une expérimentation qui, en s’appuyant sur la mise en place d’un C2I2E formateurs/animateurs, devait permettre de prototyper une offre régionale de formation pouvant rentrer dans les dispositifs de droit commun de la formation professionnelle.

Les 2 illustrations ci-après montrent :
- la ligne de temps des différentes étapes du projet,
- le prototype (version bêta) du parcours évolutif constituant l’offre de formation auquel le groupe de travail a abouti.

![c2i2e_1.png](c2i2e_1.png)

![c2i2e_2.png](c2i2e_2.png)

Une première rencontre avec certains OPCA a eu lieu, avec un retour positif et intéressé de leur part. En effet le constat du peu de place qu’occupe la culture numérique professionnelle face aux formations techniques et métiers omniprésentes était majoritairement partagé.
La question maintenant posée au niveau régional est celle du portage souhaité collectif de cette offre par des acteurs de la formation (et des epn ?) afin de lui conserver son aspect ouvert et coopératif ([pour en savoir plus sur Formavia et le projet](http://tinyurl.com/qe89fmu)).

Quelles plus-values immédiates pour les EPN à leur participation dans ce projet ?
- Obtenir le C2i2e : l'obtention de cette certification universitaire leur permet de valider des compétences numériques et pédagogiques qui peuvent les légitimer en tant qu’intervenants potentiels dans des dispositifs et établissements éducatifs (aménagement des rythmes scolaires, interventions sur la parentalité,...)
- Accompagner au  B2i Adultes : l’obtention du C2i2 est la garantie de pouvoir devenir accompagnateur et validateur B2i. Cela permet une évolution de posture et d’investir, pour ceux qui le souhaitent un champ jusque là réservé aux organismes de formation : celui de la validation de compétences numériques.
- Devenir des espaces ressources reconnus :  développer des partenariats locaux avec les organismes de formation notamment sur la questions des pré-requis et de la veille sur les usages et les technologies.


## Conclusion

L’accompagnement permanent des évolutions concomitantes des compétences numériques de base, d’une part des professionnels de l’action/médiation éducative, sociale et numérique, d’autre part des citoyens, ne pourra pas se faire dans une démarche uniforme et aseptisée de tout enjeu de société quel qu’en soit sa nature.

À noter au passage, qu’à mon sens, dans ce contexte, l’accompagnement du citoyen doit relever, en tout ou partie, d’une mission régalienne de service public pour en garantir l’égalité et l’équité d’accès à tous ! Une position qui, je le sais, ne fait pas forcément l'unanimité...

Toutefois, pour avancer ensemble dans ces chantiers stratégiques du moment, nous avons besoin me semble-t-il :
- de mutualiser les expériences régionales et de capitaliser sur les travaux menés
- de porter au niveau national la nécessaire reconnaissance des métiers(et fonctions) ainsi que des moyens nécessaires à l’accompagnement de leurs évolutions
- de travailler sur la clarification (et simplification) du champs des compétences numériques et de rendre lisible les certifications/validations d’expériences…
- de trouver les espaces d’élaboration et de formalisation permettant de donner sens et corps à l’ambition d’un “parcours éducatif au numérique tout au long de la vie”