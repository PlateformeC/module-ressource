---
title: 'Chroniques d’un Atelier Partagé #5'
media_order: DSC00631-870x550.jpg
type_ressource: text
feature_image: DSC00631-870x550.jpg
license: cc-by-nc-sa
serie:
    -
        page: /fiches/chroniques_dun_atelier_partage_1
    -
        page: /fiches/chroniques_dun_atelier_partage_2
    -
        page: /fiches/chroniques_dun_atelier_partage_3
    -
        page: /fiches/chroniques_dun_atelier_partage_4
date: '13-02-2019 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'atelier partagé'
        - low-tech
        - réparation
        - chroniques
    author:
        - 'Édouard Bourré-Guilbert'
aura:
    pagetype: website
    description: 'Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé.'
    image: DSC00631-870x550.jpg
metadata:
    description: 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/chroniques_dun_atelier_partage_5'
    'og:type': website
    'og:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #5 | Espace Ressources Num&eacute;riques'
    'og:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/chroniques_dun_atelier_partage_5/DSC00631-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #5 | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/chroniques_dun_atelier_partage_5/DSC00631-870x550.jpg'
    'article:published_time': '2019-02-13T00:00:00+01:00'
    'article:modified_time': '2020-11-26T10:57:24+01:00'
    'article:author': Ping
---

*Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé. En tant qu’explorateur associé de PiNG, Édouard nous livre son regard sur l’aventure de l’Atelier Partagé du Breil, que l’association anime depuis maintenant 3 ans. Une façon de nous faire découvrir ce qui compose un tel lieu : ses figures, ses temps forts, ses zones de tensions et ses petits miracles.*

Chère lectrice, cher lecteur, pour cette ultime chronique consacrée à cette pièce de vie pas comme les autres, nous allons parler reproduction. C’est parti mon kiki !

## ÉPISODE 5 : LA REPRODUCTION

Pour bien amorcer le sujet du jour, autorisons-nous une touche de lyrisme. L’association PiNG défend un projet insulaire dans un monde submergé par les flots d’une activité orageuse. PiNG, contre vents et marées, persiste et signe son succès. Car c’est bien de cela dont il s’agit. D’un succès. Venir à l’Atelier Partagé du Breil, c’est l’adopter. Les fidèles comme les visiteurs d’un jour l’ont bien compris.
Comme pour tout projet arrivant à une certaine forme de maturité, la volonté de partager son expérience a émergé. C’est la démarche de ces chroniques. À travers le prisme de l’explorateur associé, j’ai tenté de raconter cet endroit qui, vu de l’extérieur, s’apparenterait plus à un cabinet des curiosités qu’à un laboratoire sociétal. Avec le souhait que des porteurs de projets, des institutions aussi, puissent s’en emparer afin de multiplier les îlots d’une éducation populaire ouverte aux problématiques du XXIème siècle.

Se reproduire, c’est dans les gènes de l’humanité. Je vous épargnerai la méthodo. En revanche, pour ce qui nous concerne ici, embrayons. Vous souhaitez monter un atelier partagé dans votre quartier, votre hameau, votre village, ou encore sur une autre planète ? Vous avez déjà un clou et un bout de scotch et vous vous demandez si c’est un bon début ? Voici un aperçu de ce qui vous attend !

Un atelier partagé, c’est donc un lieu, animé par une équipe, accueillant des publics, avec pour objectifs de concevoir, réparer, apprendre et échanger autour des technologies.

### 1/ Étape 1 : déterminez ce que vous souhaitez faire.
Commencez par la structuration juridique. Vous pensiez faire évoluer votre club de philatélie ? Former un groupe informel ? Une association ? Une coopérative ? Les responsabilités et les financements varieront fortement selon que vous mettiez en place une équipe salariée ou que vous vous organisiez uniquement sur la base du bénévolat. Il faut donc fixer le cadre et l’ambition de fonctionnement.
Dans le même temps, réfléchissez aux sources de financements. Publiques et / ou privées, leurs provenances influeront sur votre activité.
Finalisez cette étape par le contenu : la programmation, l’équipement mis à disposition et la capacité d’accueil. À titre d’exemple, l’Atelier Partagé du Breil est un projet porté par l’association PiNG comprenant une équipe de sept salariés et deux volontaires en service civique qui consacre environ 10 % de leur temps au fonctionnement du lieu. Son financement est composé à 70 % d’argent public, 15 % de ressources propres, 9 % de fonds privés et 5 % de sources diverses et probablement douteuses ! Le matériel quant à lui provient de dons, récupérations, réparations et en dernier recours d’achats neufs, utilisé par environ 25 personnes chaque semaine.

### 2/ Étape 2 : trouvez le lieu.
C’est le nerf de la guerre. Or, la chasse au foncier, ce n’est pas forcément une partie de plaisir. Sur ce terrain, vous rentrez en concurrence avec des promoteurs aux calculettes plus persuasives que vos idéaux. Particulièrement dans certaines villes où le mètre carré est spéculé comme vous le feriez avec le spéculoos accompagnant le café de votre voisin de terrasse « vous ne le mangerez pas, n’est-ce-pas ? »
Malgré un marché du béton en tension, il existe beaucoup de bureaux vides, de baux aux bois dormants. D’ailleurs, chanceux de nature, vous vous trouvez au bon moment, au bon endroit. Un local correspondant à vos besoins se présente à vous. Sauf que… BiNG, il est situé dans un no man’s land*. C’est bien dommage car l’idée est plutôt d’avoir des men et des women pour faire vivre votre lieu.
Vous repartez alors à la chasse. Les moyens de se tenir aux aguets sont multiples, allant de l’attention donnée aux bruits de quartier, à l’épluchage des pages du Bon Coin, en passant par le squat des couloirs institutionnels. Sachez-le, les collectivités ont souvent du foncier qu’elles préféreraient ne plus chauffer aux frais d’un budget toujours plus fragile. C’est d’ailleurs cette piste que PiNG a choisie. L’Atelier Partagé du Breil évolue dans une pièce de 50m2, attenante à une seconde pièce au volume similaire qui accueille une bibliothèque, un espace détente, et permet à l’atelier de s’étendre en cas de forte affluence ou de fuite du toit. Il est implanté dans une ancienne école réhabilitée en pôle associatif, dans un quartier de ville accessible facilement à pied, en transport en commun ou, en tout dernier recours, à trottinette (pas électrique, restons sérieux).

### 3/ Étape 3 : obtenez le soutien de la collectivité.
Accordons-nous sur le fait que votre activité ne sera pas l’exemple même d’un équilibre budgétaire. Normal, on ne mène pas une action d’intérêt général comme on le ferait avec une entreprise à but lucratif. La collectivité, qu’elle soit à l’échelle de la commune, de la métropole, du département, de la région (je ne continue pas, vous avez compris), est là pour vous accompagner. Après, c’est au petit bonheur la chance, comme en amour. Vous pouvez espérer un peu de sous, beaucoup d’estime, du soutien moral passionnément, des démarches administratives à la folie, ou carrément rien de tout ça. Au delà du service concerné, c’est la personne qui vous accompagne qui fera la différence. Vos meilleurs atouts ? La patience et votre capacité à montrer ce que vous êtes capable de faire avant qu’on ne vous donne un coup de pouce. Attention toutefois, plus vous obtiendrez d’aides publiques, plus vous serez dépendant des aléas politiques

### 4/ Étape 4 : fédérez des partenaires.
Chaque jour, au réveil, entre le café et la tartine grillée, répétez 10 fois le dicton suivant : « Tout seul on va plus vite, ensemble on va plus loin ! ».
Vous devrez vous mettre en lien avec les réseaux de structures (sociétés civiles, éducatives et économiques) afin que votre projet trouve sa place dans le paysage. Ainsi, vous pourrez envisager toutes les formes de partenariats (ex : mise à disposition du lieu, programmation croisée, temps forts partagés) ce qui permettra de croiser les communautés d’intérêts et de personnes

### 5/ Étape 5 : communiquez. Arrivé à cette étape, vous avez communiqué sur votre intention pour formaliser vos besoins (local, finance, partenaires). Il vous reste à toucher les principaux concernés, ceux qui bénéficieront de l’atelier tout en lui donnant vie : les usagers.
Vous partez à la recherche de profils variés pouvant transmettre et recevoir. L’enjeu principal résidant dans le fait de toucher des publics hétérogènes. De les amener à s’approprier les équipements et les pratiques, dans ce lieu qui leur est encore étranger. Et ainsi tirer partie des technologies pour aborder des fondamentaux tel que l’autonomie, l’échange, l’apprentissage informel, la montée en compétence, ou encore la sobriété et l’écologie.

### 6/ Étape 6 : inaugurez !

Vous voilà initié à la démarche. Émoustillé ? Vous avez besoin d’aller plus loin ? Vous cherchez des compléments d’information, des points de clarification, du café chaud ? N’hésitez pas à venir à l’atelier et à interpeller l’équipe de PiNG !

C’est là-dessus que je vous laisse reprendre le cour de votre bricole. À tchao bon mardi à l’Atelier Partagé du Breil et ailleurs !

Édouard Bourré-Guilbert

* Endroit où l’on se promène par amour du tag, du tesson de bouteille ou plus généralement par erreur

---

Ces chroniques n’auraient pas été possibles sans l’existence de l’Atelier Partagé du Breil (CQFD) mais aussi et surtout de toutes les rencontres que j’y ai faites. Au premier rang desquelles, les adhérents, les bénévoles, l’équipe de PiNG dont les volontaires en service civique, et tout particulièrement l’instigateur de cette expérience d’atelier et d’écriture, Thomas Bernardi. Sans oublier mes relecteurs de la première et dernière heure. Merci pour ces temps partagés, passés et à venir. Ces chroniques vous sont dédiées.
