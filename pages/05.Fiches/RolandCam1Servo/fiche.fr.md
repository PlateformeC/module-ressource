---
title: RolandCam1Servo
media_order: ROland_Cam1servo.JPG
type_ressource: text
feature_image: ROland_Cam1servo.JPG
license: cc-by-nc-sa
date: '01-01-2019 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cette machine sert à la découpe vinyl.'
    image: ROland_Cam1servo.JPG
metadata:
    description: 'Cette machine sert &agrave; la d&eacute;coupe vinyl.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/rolandcam1servo'
    'og:type': website
    'og:title': 'RolandCam1Servo | Espace Ressources Num&eacute;riques'
    'og:description': 'Cette machine sert &agrave; la d&eacute;coupe vinyl.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/rolandcam1servo/ROland_Cam1servo.JPG'
    'og:image:type': image/jpeg
    'og:image:width': '1024'
    'og:image:height': '768'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'RolandCam1Servo | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine sert &agrave; la d&eacute;coupe vinyl.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/rolandcam1servo/ROland_Cam1servo.JPG'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T16:34:02+01:00'
    'article:author': Ping
---

*Cette machine sert à la découpe vinyl.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/RolandCam1Servo?classes=btn)
