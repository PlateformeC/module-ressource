---
title: 'Pour une réappropriation d''Internet'
media_order: about-img-2.png
type_ressource: text
feature_image: about-img-2.png
license: cc-by-sa
date: '23-06-2014 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - Internet
        - 'peer to peer'
        - communs
    author:
        - 'Mathieu Goessens'
aura:
    pagetype: website
    description: 'Vulgarisation sur Internet, centralisation et technos pair-à-pair : un article de Mathieu Goessens.'
    image: about-img-2.png
metadata:
    description: 'Vulgarisation sur Internet, centralisation et technos pair-&agrave;-pair : un article de Mathieu Goessens.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/pour_une_reappropriation_d_internet'
    'og:type': website
    'og:title': 'Pour une r&eacute;appropriation d''Internet | Espace Ressources Num&eacute;riques'
    'og:description': 'Vulgarisation sur Internet, centralisation et technos pair-&agrave;-pair : un article de Mathieu Goessens.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/pour_une_reappropriation_d_internet/about-img-2.png'
    'og:image:type': image/png
    'og:image:width': '620'
    'og:image:height': '165'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Pour une r&eacute;appropriation d''Internet | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Vulgarisation sur Internet, centralisation et technos pair-&agrave;-pair : un article de Mathieu Goessens.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/pour_une_reappropriation_d_internet/about-img-2.png'
    'article:published_time': '2014-06-23T00:00:00+02:00'
    'article:modified_time': '2020-11-26T16:11:03+01:00'
    'article:author': Ping
---

*Vulgarisation sur Internet, centralisation et technos pair-à-pair : un article de Mathieu Goessens.*

## De la naissance d'Internet...

Un vieil adage d'Internet veut que celui-ci ait été créé par les militaires américains avec comme objectif de pouvoir résister à une attaque nucléaire. Si cela n'est que partiellement vrai (Internet était avant tout un réseau de recherche reliant les différentes universités et centres de recherche), il n'en reste pas moins que sa conception a été dès le début fortement décentralisée. Ainsi, à l'inverse des réseaux téléphoniques, ou du modèle français, ayant centralisé tous les réseaux et les pouvoirs autour de Paris du fait de son héritage jacobin, Internet, le « réseau des réseaux » ne fut pas construit autour d'un centre, mais par l'interconnexion de multiples réseaux, jusqu'alors privés.

Cet élément peut paraître quelque peu anecdotique, mais il explique en grande partie le succès d'Internet : l'absence de réel centre, que ce soit au niveau du réseau physique, des entités administratives ou des services offerts, a ainsi créé un réseau des plus ouverts. C'est cette ouverture qui permet à tous de rejoindre facilement le réseau, mais aussi d'y proposer des services ou faire entrer de nouveaux utilisateurs qui, a pour une grande part mené Internet à son succès actuel.

Un parallèle intéressant peut également être fait avec le milieu du logiciel libre. En effet, le développement d'Internet a été très lié au développement et à l'essor du logiciel libre, au travers d'une synergie commune. Ainsi, si le logiciel libre a profondément profité de l'infrastructure offerte par Internet pour la distribution et le développement collaboratif, la mise à disposition de logiciels, de protocoles et de spécifications ouvertes a également fortement favorisé la démocratisation d'Internet vis-à-vis de ses concurrents de l'époque, au demeurant plus fermés.

## … À sa démocratisation.

Après avoir été cantonné pendant plusieurs années à des usages spécifiques au sein des milieux de recherche et universitaires, c'est durant les années 90 qu'Internet est sorti de l'ombre et qu'il est apparu au sein du grand public. Ces années ont vu l'arrivée de fournisseurs d'accès commerciaux qui ont rendu Internet accessible à une population de plus en plus grande. Elles ont aussi vu apparaître les premiers fournisseurs de contenus commerciaux, qui tiraient alors profit de la toute jeune naissance du .com au milieu des années 80.

Ces nouveaux utilisateurs et les nouveaux usages qu'ils ont amenés ont fortement influencé l'apparition de nouvelles technologies liées à Internet, de modèles physiques de connexions aux contenus eux-mêmes. C'est ainsi que petit à petit les connexions ADSL ont remplacé les connexions téléphoniques (RTC), rendant disponibles des débits plus importants et des connexions permanente (les connexions RTC monopolisaient les lignes téléphoniques). De la même manière, le Web (né au tout début des années 90) a progressivement évolué. S'il comportait au tout début essentiellement de simples pages de textes liées entre elles, il a vu peu à peu arriver de nouveaux types de contenus de plus en plus riches, à l'image des mises en page complexes et fortement stylisées, des vidéos et photos haute définition, en passant par des applications ou des jeux.

Ces évolutions, ainsi que celles qui ont suivi (comme la mise à disposition d'Internet sur les mobiles), et la diminution des coûts d'accès à Internet, ont amené à leur tour de nouveaux usages. C'est ainsi que vers la fin des années 2000, sont apparus les blogs et autres réseaux sociaux, où chaque utilisateur d'Internet est amené à contribuer et à s'exprimer.


## Un outil de plus en plus participatif …

À la fin des années 2000, la démocratisation des réseaux sociaux et des sites participatifs a profondément bouleversé la manière dont les utilisateurs appréhendent et utilisent Internet. En effet, à sa création, la publication de contenu sur Internet n'était accessible qu'à quelques techniciens chevronnés. Ces dernières années ont vu apparaître l'émergence de logiciels, de sites et de services qui permettent à chaque utilisateur du réseau de publier et de diffuser aisément ses propres contenus. C'est ainsi que sont apparus de plus en plus nombreux, les blogs, les forums et autres sites collaboratifs permettant à des individus et à des communautés d'échanger ensemble et de s'exprimer. De la même manière de nombreuses plate-formes collaboratives ont vu le jour, dont le meilleur exemple est sans doute l'encyclopédie libre Wikipédia.

Benjamin Bayart  a dit un jour « l'imprimerie a permis au peuple de lire, Internet va lui permettre d'écrire ». En effet, en offrant un nouveau moyen d'échange, ouvert, horizontal et participatif, Internet est en passe de modifier la société, à une échelle que l'on ne perçoit pas encore vraiment. Ainsi, la libéralisation de l'expression et de la prise de parole publique est en train de modifier progressivement le rapport des citoyens aux médias. Alors que les « vieux médias » comme la presse écrite, la télévision ou la radio offraient un modèle vertical de diffusion de l'information, où le lecteur/auditeur n'avait que quelques sources d'informations et devait les croire sur parole, Internet permet à tout le monde de s'exprimer, discuter, échanger. L'exemple de la presse, ce fameux quatrième pouvoir (que beaucoup considèrent comme le premier pouvoir du fait de son importance) n'est qu'un exemple parmi d'autres ; on pourrait également, trouver de nombreux autres exemples comme les outils permettant à tous d'ouvrir des pétitions (que certains états ou la commission européenne intègrent pleinement dans leurs processus législatifs), les outils de financement participatifs, les outils d'enseignement à distance (MOOCs) en passe d'être déployés en France, etc.  

L'un des exemples les plus connus du grand public, est sans doute Wikipédia, la fameuse encyclopédie libre et ses millions de pages traduites dans plusieurs dizaines de langues. L'exemple est intéressant en raison des nombreuses questions qu'il pose, en termes d'enseignement, de vérification de l'information et de ses sources, mais aussi simplement, car ces questions n'ont pas toutes trouvé de réponses et n'amènent d'ailleurs pas forcément de réponses uniques et universelles. L'exemple est également intéressant car Wikipédia à l'image de nombreux sites tend à centraliser l'information, et ainsi faciliter son contrôle.

## … Mais de plus en plus contrôlé

Comme beaucoup de sites Internet et d'initiatives fondées autour, Wikipédia est né grâce aux principes de décentralisation et d'ouverture chers à la communauté Internet. En effet, ce sont ces principes, qui ont permis du jour au lendemain, à ses créateurs d'ouvrir le service sans avoir à demander d'autorisation administrative, d'accès privilégié au réseau, ou quoi que ce soit. Il a suffi d'un budget de quelques centaines d'euros et d'un accord avec un fournisseur, parmi les milliers que compte Internet, pour ouvrir ce service qui est aujourd'hui l'un des plus utilisés au monde.

En revanche, Wikipédia, comme de nombreux autres services, tend à centraliser l'information sur une poignée de serveurs, hébergés à l'autre bout d'Internet, et sur lesquels le contrôle de l'utilisateur est faible. De la même manière, durant la fin des années 2000 de nombreux utilisateurs créaient et ouvraient leurs propres blogs, leurs propres forums ; aujourd'hui, ces mêmes utilisateurs utilisent facebook ou d'autres services similaires pour héberger leurs données. Dans bien des cas, facebook est utilisé en complément voire en remplacement d'un site Internet. Et il existe de nombreux autres exemples, à l'image de l'utilisation massive de Google et de ces services, en particulier gmail.

Cette « re-centralisation » d'Internet et de ses services autour de quelques grands fournisseurs pose de nombreux problèmes. Les problèmes de respect de la vie privée sont sans doute parmi les plus visibles et les plus connus du grand public. En effet, la centralisation des services que les internautes utilisent au quotidien fait que ces services et leurs fournisseurs accaparent d'immense volumes de données qui leur permettent de savoir qui cherche quoi, qui s'intéresse à quoi, qui communique avec qui et ce qu'ils se disent…

Ces problématiques de vie privée ne sont qu'un élément parmi d'autres. En effet, cette centralisation de l'information pose d'autres problèmes encore bien méconnus. Ainsi, il convient de se demander si cette centralisation de l'information n'est pas en mesure de favoriser son contrôle (Google filtre déjà les résultats des recherches en fonction de divers paramètres). Et, de la même manière, si Internet était à sa création un réseau ouvert, participatif, où chaque machine, que ce soit le serveur d'une université ou l'ordinateur d'un particulier, était traité de la même manière, aujourd'hui, de fait, certains fournisseurs ont un rôle bien plus central que d'autres et l'information est de plus en plus située dans le cœur du réseau – quelques gros serveurs – et de moins en moins dans ses extrémités – l'ordinateur de monsieur tout le monde. Il convient alors de se demander si l'ouverture d'Internet et les facilités de diffusion de l'information qu'il offre sont amenées à perdurer, à l'heure ou l'information, son stockage, sa diffusion et son contrôle ne sont plus gérés que par une poignée de fournisseurs commerciaux.

En outre, bien que les récentes révélations autour des écoutes à très large échelle de la NSA aient fait la une des grands journaux, elles ne semblent avoir modifié les habitudes des internautes que dans une bien faible mesure. En effet, comment se plaindre de la surveillance étatique lorsqu'on admet confier l'ensemble de ses données à des fournisseurs commerciaux ? Comment critiquer cette surveillance lorsqu'on utilise au quotidien pour publier et recevoir de l'information des services de fournisseurs ayant renié toute idée de vie privée, comme c'est le cas de Google ? Un autre adage d'Internet, veut que « si le service est gratuit, c'est que vous êtes le produit », en référence à la monétisation des données confiées au fournisseurs commerciaux. Cela pose d'évidentes questions d'éducation, mais peut-être que l'absence de solutions techniques connues du grand public et faciles à prendre en main ne permet pas d'apporter des réponses simples à s'approprier.

## Le pair-à-pair comme nouvel élément de décentralisation ?

Actuellement, Internet permet à quiconque le souhaite et en possède les compétences, d'héberger chez soi ou chez des fournisseurs spécifiques, ses propres données, ses propres services et ainsi participer pleinement au réseau et à la création d'une communauté internet, à la diffusion de savoirs communs comme d'opinions personnelles.

Nul besoin pour cela de matériel spécifique ou de logiciels coûteux. Un PC d'il y a quelques années, ou un mini-PC peu cher comme un raspberry pi remplira très bien ce rôle. De la même manière, les logiciels qui permettent d'héberger des sites web, des services mails, des blogs, des forums, des galeries photos ou encore de systèmes d'échanges de fichiers, sont installables en quelques clics sur Windows, et sur des versions grand public de Gnu/Linux comme Ubuntu. Il existe également de nombreuses initiatives visant à créer des matériels et des logiciels dédiés à ces usages, peu chers et faciles à prendre en main.

En revanche, au-delà de la difficulté de prise en main, ces solutions techniques (matérielles et logicielles), se heurtent à quelques problèmes : par exemple, s'il est aisé de monter un petit service pour une petite communauté ou pour de la diffusion d'information à petite échelle, il devient plus compliqué de passer au stade supérieur et d'offrir ces services au plus grand nombre sans multiplier les coûts.

De nombreuses initiatives visent à améliorer cet état de fait, en facilitant la mise en place de tels logiciels, mais se heurtent à des problèmes de passage à l'échelle et de contraintes de bandes passantes, de disponibilité et de tolérance aux pannes. Les technologies pair-à-pair comme eMule ou BitTorrent utilisées pour les échanges de fichiers permettent de répondre à ces problématiques. Depuis 2012,mes travaux concernent l'adaptation de ces modèles à la navigation web de tous les jours. Ces technologies permettent aux utilisateurs d'échanger des contenus entre eux, par la mutualisation de leurs lignes internet, et offrent une redondance en cas de panne ou de censure, l'automatisation et la simplification des étapes nécessaires  à la diffusion de contenus.

L'objectif de ces travaux consiste à offrir une alternative viable à la diffusion de contenus sur les gros réseaux sociaux commerciaux. Au-delà, en mettant les utilisateurs à contribution pour la création, le stockage et la diffusion d'informations, ces initiatives visent à offrir les moyens techniques de remettre l'utilisateur au centre d'Internet, et d'en faire un outil, accessible au plus grand nombre, pour pour consolider, développer et diffuser les biens communs de la connaissance.

---
Texte sous licence CC BY 2.0 FR
