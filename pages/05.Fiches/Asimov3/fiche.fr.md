---
title: 'ASIMOV3 (imprimante 3D)'
media_order: Asimov.JPG
type_ressource: text
feature_image: Asimov.JPG
license: cc-by-nc-sa
date: '01-03-2016 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
        - 'impression 3D'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cette machine à commande numérique sert à l''impression 3D. Elle est située au fablab Plateforme C.'
    image: Asimov.JPG
anchors:
    active: false
tagtitle: h2
hero_overlay: true
hero_showsearch: true
show_breadcrumbs: true
content:
    items:
        - '@self.children'
    limit: 12
    order:
        by: date
        dir: desc
    pagination: true
metadata:
    description: 'Cette machine &agrave; commande num&eacute;rique sert &agrave; l''impression 3D. Elle est situ&eacute;e au fablab Plateforme C.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/asimov3'
    'og:type': website
    'og:title': 'ASIMOV3 (imprimante 3D) | Espace Ressources Num&eacute;riques'
    'og:description': 'Cette machine &agrave; commande num&eacute;rique sert &agrave; l''impression 3D. Elle est situ&eacute;e au fablab Plateforme C.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/asimov3/Asimov.JPG'
    'og:image:type': image/jpeg
    'og:image:width': '1161'
    'og:image:height': '861'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'ASIMOV3 (imprimante 3D) | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine &agrave; commande num&eacute;rique sert &agrave; l''impression 3D. Elle est situ&eacute;e au fablab Plateforme C.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/asimov3/Asimov.JPG'
    'article:published_time': '2016-03-01T00:00:00+01:00'
    'article:modified_time': '2020-11-25T16:55:36+01:00'
    'article:author': Ping
---

*Cette machine à commande numérique sert à l'impression 3D. Elle est située au fablab Plateforme C.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Asimov3?classes=btn)
