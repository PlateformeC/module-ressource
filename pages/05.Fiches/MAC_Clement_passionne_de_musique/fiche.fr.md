---
title: '[MAKER À COEUR] Clément, passionné de musique et graveur de vinyles en devenir'
media_order: IMG_9739bis-870x550.jpg
type_ressource: video
feature_image: IMG_9739bis-870x550.jpg
license: cc-by-nc-sa
date: '01-03-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Clément travaille actuellement sur la fabrication de son propre graveur de vinyles et il souhaite, à terme, réussir à démocratiser le procédé complexe de la gravure.'
    image: IMG_9739bis-870x550.jpg
metadata:
    description: 'Cl&eacute;ment travaille actuellement sur la fabrication de son propre graveur de vinyles et il souhaite, &agrave; terme, r&eacute;ussir &agrave; d&eacute;mocratiser le proc&eacute;d&eacute; complexe de la gravure.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/mac_clement_passionne_de_musique'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Cl&eacute;ment, passionn&eacute; de musique et graveur de vinyles en devenir | Espace Ressources Num&eacute;riques'
    'og:description': 'Cl&eacute;ment travaille actuellement sur la fabrication de son propre graveur de vinyles et il souhaite, &agrave; terme, r&eacute;ussir &agrave; d&eacute;mocratiser le proc&eacute;d&eacute; complexe de la gravure.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_clement_passionne_de_musique/IMG_9739bis-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Cl&eacute;ment, passionn&eacute; de musique et graveur de vinyles en devenir | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cl&eacute;ment travaille actuellement sur la fabrication de son propre graveur de vinyles et il souhaite, &agrave; terme, r&eacute;ussir &agrave; d&eacute;mocratiser le proc&eacute;d&eacute; complexe de la gravure.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_clement_passionne_de_musique/IMG_9739bis-870x550.jpg'
    'article:published_time': '2017-03-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:49:17+01:00'
    'article:author': Ping
---

*Clément travaille actuellement sur la fabrication de son propre graveur de vinyles et il souhaite, à terme, réussir à démocratiser le procédé complexe de la gravure. Cette semaine, il nous a exposé son projet et aussi sa vision de Plateforme C.*

**Bonjour Clément, peux-tu te présenter ?**

Je m’appelle Clément et j’ai 26 ans. Je suis issu d’une formation dans les matériaux composites, (j’ai passé un master en éco-conception) et dans le cadre de mes expériences professionnelles j’ai travaillé dans le domaine des composites (dans les secteurs de l’aéronautique par exemple). Dernièrement j’avais envie de développer un projet plus personnel avec un objectif professionnel à la clé afin de me sentir plus impliqué dans ce que je fais au jour le jour. J’ai voulu réunir ma passion pour la musique (afrobeat, rockpsyché, techno…) et mes aspirations pour ma vie professionnelle, c’est-à-dire travailler sur les matériaux, un peu de mécanique, et aussi autour des outils numériques.

**Comment est né ton projet ?**

Mon projet est né il y a environ un an quand j’ai découvert la gravure, qui est une technologie qui existe depuis plusieurs dizaines d’années. Je me suis dit que ça avait l’air super bien mais aussi que c’était quelque chose de très technique. J’avais tout d’abord envie d’apprendre comment le procédé et les machines pour graver fonctionnaient mais aussi de rendre ça plus accessible. Je trouve que le paysage musical est en train de changer. Les gens qui produisent de la musique et qui veulent la distribuer ont aujourd’hui pas mal de problèmes au niveau de leurs délais, des quantités possibles qu’ils peuvent produire. On voit qu’il y a un regain d’intérêt pour la matière, pour le support physique de musique : c’est pour ça que je me suis plus intéressé notamment au vinyle. Avec la gravure je me suis dit que c’était possible de produire des disques en petite quantité. Mais je n’invente rien, la gravure existe déjà ! Ce que j’essaye de faire avec ce projet c’est personnellement d’apprendre cette technique et ensuite d’essayer de la démocratiser en faisant mon propre graveur.

**Qu’est-ce qui t’as conduit à mener ce projet à Plateforme C ?**

J’avais besoin de produire des pièces, de faire des choses simples comme couper, percer … et chez moi ce n’était pas possible. Je suis revenu frapper ici parce que je connaissais un peu le lieu et que je voulais, non seulement développer mon projet, mais aussi rencontrer des gens, partager mes idées et voir s’il y avait un retour dessus et si je pouvais rencontrer des gens qui puissent m’aider. La gravure c’est vraiment un domaine hyper technique et très difficile à maîtriser. Donc je voulais voir si je pouvais monter une équipe autour de ce projet-là. Après ici il se trouve que chacun a ses propres projets et aujourd’hui je n’ai pas trouvé quelqu’un à Nantes avec qui travailler. Mais j’ai des contacts sur Marseille, sur des forums… C’est vrai qu’au fablab je suis venu plutôt dans l’idée d’utiliser les outils et j’ai pu notamment apprendre à me servir des imprimantes 3D pour fabriquer mes pièces sur mesure pour le graveur, aussi d’outils plus simples (comme les perceuses, la fraiseuse…). J’ai pu avoir la chance de participer à un apéro|projets en novembre pour me faire connaître un peu plus.

**Comment imagines-tu la suite de ton projet ?**

Je ne sais pas encore où j’en suis, pour l’instant je me concentre sur l’apprentissage de la gravure. J’aimerais quand même essayer de protéger ce que j’ai fait, pas simplement dire « allez-y servez-vous », ce n’est pas si simple l’open source… Si j’arrive à obtenir des résultats intéressants avec cette machine, je pourrai par exemple vendre des machines en kit avec une protection au niveau de la vente, former des gens à la gravure ou bien, si je vois que les gens n’ont pas forcément envie d’avoir leur propre machine, je pourrai simplement faire de la gravure pour des artistes avec mon graveur. Mais de toute façon, tant que je n’ai pas de machine qui fonctionne, c’est compliqué. Les résultats que j’ai obtenus en décembre n’étaient pas de très bonne qualité, ça grésillait… J’ai vraiment essayé de faire avec ce que j’avais sur la main. Depuis le début de l’année, je suis en train d’essayer d’améliorer la machine après avoir vu quels problèmes j’avais. Je reviens ici depuis quelques semaines pour essayer de réimprimer de nouvelles pièces, modifier ma machine pour qu’elle fonctionne mieux et soit plus précise ! Au fur et à mesure que j’avance dans mon projet, je me rends compte que la gravure est très complexe et peu accessible. Je me demande si plus tard les gens voudront vraiment avoir leur propre machine, est-ce que ça sera vraiment faisable… Ça demande beaucoup de connaissances. Mais le but reste quand même de le rendre plus accessible de la même manière que les imprimantes 3D.

**Tu fréquentes Plateforme C depuis environ un an, qu’est-ce qui t’a marqué la ou les premières fois que tu es venu ici ?**

Les premières fois c’était pour faire des initiations aux imprimantes 3D justement. J’avais déjà dans l’idée que je voulais produire mes pièces et j’ai rencontré des gens qui m’ont permis de prendre ces machines en main. C’était l’été et il y avait peu de monde, du coup c’était plus facile de discuter, c’était sympa. Je pouvais prendre le temps de comprendre comment fonctionnait la machine. C’est ça que j’aime au fablab, c’est le simple fait de prendre le temps de discuter avec les gens, de pouvoir se réunir autour d’un projet, d’une machine…

*Si vous êtes intéressés par ce sujet ou bien vous-mêmes en train de fabriquer votre graveur, n’hésitez pas à visiter le site de [Clément](http://clementduret.fr/).*
