---
title: 'Les doigts dans la prise #13 : les aidants numériques'
type_ressource: audio
feature_image: men-laptop-computer-keyboard.jpg
license: cc-by-nc-sa
published: true
date: '30-09-2020 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Grégoire Barbot'
aura:
    pagetype: article
    description: '*Votre grand-père n’arrive pas à lire vos messages ou votre petite nièce souhaite que vous lui appreniez comment télécharger des musiques sur son téléphone ? Eh bien vous êtes, sans forcément le savoir, un aidant numérique !*'
    image: men-laptop-computer-keyboard.jpg
metadata:
    description: '*Votre grand-p&egrave;re n&rsquo;arrive pas &agrave; lire vos messages ou votre petite ni&egrave;ce souhaite que vous lui appreniez comment t&eacute;l&eacute;charger des musiques sur son t&eacute;l&eacute;phone ? Eh bien vous &ecirc;tes, sans forc&eacute;ment le savoir, un aidant num&eacute;rique !*'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/sun13_septembre20_aidants_numeriques'
    'og:type': article
    'og:title': 'Les doigts dans la prise #13 : les aidants num&eacute;riques | Espace Ressources Num&eacute;riques'
    'og:description': '*Votre grand-p&egrave;re n&rsquo;arrive pas &agrave; lire vos messages ou votre petite ni&egrave;ce souhaite que vous lui appreniez comment t&eacute;l&eacute;charger des musiques sur son t&eacute;l&eacute;phone ? Eh bien vous &ecirc;tes, sans forc&eacute;ment le savoir, un aidant num&eacute;rique !*'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/sun13_septembre20_aidants_numeriques/men-laptop-computer-keyboard.jpg'
    'og:image:type': image/webp
    'og:image:width': '910'
    'og:image:height': '607'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #13 : les aidants num&eacute;riques | Espace Ressources Num&eacute;riques'
    'twitter:description': '*Votre grand-p&egrave;re n&rsquo;arrive pas &agrave; lire vos messages ou votre petite ni&egrave;ce souhaite que vous lui appreniez comment t&eacute;l&eacute;charger des musiques sur son t&eacute;l&eacute;phone ? Eh bien vous &ecirc;tes, sans forc&eacute;ment le savoir, un aidant num&eacute;rique !*'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/sun13_septembre20_aidants_numeriques/men-laptop-computer-keyboard.jpg'
    'article:published_time': '2020-09-30T16:31:00+02:00'
    'article:modified_time': '2020-12-09T14:01:05+01:00'
    'article:author': Ping
---

*Votre grand-père n’arrive pas à lire vos messages ou votre petite nièce souhaite que vous lui appreniez comment télécharger des musiques sur son téléphone ? Eh bien vous êtes, sans forcément le savoir, un aidant numérique !*

Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire). Pour ce premier épisode de la saison, Grégoire Barbot nous parle des aidants numériques.

Pour écouter le podcast c’est par [ici](https://www.lesonunique.com/content/les-doigts-dans-la-prise-les-aidants-numeriques-0) !

![Les doigts dans la prise - les aidants numériques](les%20aidants%20numeriques.mp3)
