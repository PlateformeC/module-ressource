---
title: 'Culture libre et propriété industrielle : incompatibilité chronique?'
media_order: IMG_9378-870x550.jpg
type_ressource: text
feature_image: IMG_9378-870x550.jpg
license: cc-by-nc-sa
date: '09-09-2016 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'propriété intellectuelle'
        - 'culture libre'
        - opensource
        - 'c libre'
    author:
        - 'Vladimir Ritz'
aura:
    pagetype: website
    description: 'Comment les bricoleurs et inventeurs à l’œuvre dans les fablabs peuvent-ils, à la fois, partager leurs travaux tout en préservant leurs intérêts ? '
    image: IMG_9378-870x550.jpg
metadata:
    description: 'Comment les bricoleurs et inventeurs &agrave; l&rsquo;&oelig;uvre dans les fablabs peuvent-ils, &agrave; la fois, partager leurs travaux tout en pr&eacute;servant leurs int&eacute;r&ecirc;ts ? '
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/culture_libre_et_propriete_industrielle'
    'og:type': website
    'og:title': 'Culture libre et propri&eacute;t&eacute; industrielle : incompatibilit&eacute; chronique? | Espace Ressources Num&eacute;riques'
    'og:description': 'Comment les bricoleurs et inventeurs &agrave; l&rsquo;&oelig;uvre dans les fablabs peuvent-ils, &agrave; la fois, partager leurs travaux tout en pr&eacute;servant leurs int&eacute;r&ecirc;ts ? '
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/culture_libre_et_propriete_industrielle/IMG_9378-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Culture libre et propri&eacute;t&eacute; industrielle : incompatibilit&eacute; chronique? | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Comment les bricoleurs et inventeurs &agrave; l&rsquo;&oelig;uvre dans les fablabs peuvent-ils, &agrave; la fois, partager leurs travaux tout en pr&eacute;servant leurs int&eacute;r&ecirc;ts ? '
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/culture_libre_et_propriete_industrielle/IMG_9378-870x550.jpg'
    'article:published_time': '2016-09-09T00:00:00+02:00'
    'article:modified_time': '2020-11-26T11:16:52+01:00'
    'article:author': Ping
---

*Comment les bricoleurs et inventeurs à l’œuvre dans les fablabs peuvent-ils, à la fois, partager leurs travaux tout en préservant leurs intérêts ? En d’autres termes, la culture libre peut-elle s’adapter à tous les secteurs de notre société ? Si l’on se fie à ses nombreuses déclinaisons (Open Innovation,  Open Acces, Open Data, Open Education, Open source écologie, Open source seeds initiative, Open democracy …) il semblerait que oui. Et pourtant, le dernier né de la fratrie, l’Open Hardware questionne à bien des égards : s’agit-il toujours de Propriété Littéraire et Artistique ? Et sinon, dans quelle mesure la Culture Libre influence t-elle la Propriété Industrielle ?*

Le groupe de travail C LiBRE, constitué d’une vingtaine de membres (juristes, universitaires, designers, bricoleurs) a mis à plat ces questionnements lors d’une journée de travail collectif ayant pour postulat de départ « Comment réconcilier Propriété Industrielle et Culture Libre ?… et encore, est-ce bien raisonnable ? ».
Voici, condensé dans cet article, le résumé de nos pérégrinations en terre inconnue, quelque part entre partage de connaissances et production industrielle.

## Tout d’abord, nous nous sommes entendus sur les choses suivantes :

– **Par culture libre, on entend ce mouvement social et politique qui vise à plus de partage entre les individus.** L’idée globale est qu’avec de la collaboration et de l’entraide, la société ne se portera que mieux. Ainsi, ce mouvement tente de s’émanciper de l’individualisme post-révolutionnaire de 1789 dont l’une des incarnations les plus célèbres est le concept de propriété privée, notamment en révisant les modalités de rétribution.

– **La propriété industrielle quant à elle est un ensemble de règles juridiques qui encadre les créations industrielles et notamment les inventions.** Il faut bien comprendre que ce qui est protégé n’est pas l’objet mais la création. L’objet est le support et le droit de la propriété industrielle ne s’en occupe pas. Elle ne s’occupe que de la création d’un point de vue intellectuel, immatériel (c’est d’ailleurs pour cela que la notion de « matériel libre » prête à confusion).
De manière courante, nous voyons des objets (brevetables ou non, brevetés ou non) placés sous différentes licences (Creative Commons, GNU/GPL, Art Libre, TAPR OHL, CERN …) et ce n’est pas surprenant. En effet, les acteurs de la culture Libre, fervents défenseurs du partage, ne perçoivent pas toujours très clairement les frontières de la propriété intellectuelle. Ce qui est plus surprenant, en revanche, c’est que bien souvent ces actes de partage n’ont pas de validité juridique. Et c’est, selon nos recherches, une chose trop peu connue.

Le constat est le suivant : il est possible de placer une œuvre de l’esprit originale (logiciel, photographie, image, vidéo, musique, texte …) sous licence libre (terme entendu au sens large) car le droit prévoit que vous êtes titulaire du droit d’auteur sur votre création et ce sans qu’il n’y ait de formalité à accomplir. En revanche, lorsqu’il s’agit d’une création industrielle (inventions, designs, obtentions végétales, circuits imprimés, etc.) il faut, pour en être propriétaire, effectuer des formalités administratives auprès de l’INPI et généralement se faire entourer de personnes compétentes en matière de rédaction de brevet. Ces formalités sont par ailleurs relativement onéreuses, mais surtout, impératives pour avoir des droits sur l’invention que l’on souhaite protéger. Et tout comme il ne vous viendrait pas à l’idée de mettre, à disposition de tous, la voiture de votre voisin, vous ne pouvez pas faire de contrat sur une invention qui ne vous appartient pas. **Vous ne pouvez donc pas en principe mettre de licence libre sur une invention sur laquelle vous ne possédez pas de brevet. Est-ce à dire que la culture libre s’arrête aux frontières de la propriété industrielle ? Pas nécessairement.**

## Plusieurs hypothèses sont avancées, mais chacune apporte son lot de difficultés :

### Utiliser les licences dites « Open Hardware »
La première hypothèse serait de choisir les licences dites « Open Hardware » (exemple : TAPR Licence, CERN Licence, Fab’Lib, etc.). Ces licences se basent sur la documentation du « hardware », soit, en d’autres terme, du matériel. **L’idée est qu’en apposant une licence sur la documentation, on va pouvoir contrôler la fabrication du matériel documenté. L’idée est séduisante mais juridiquement elle est très instable.** Si la documentation peut-être protégée par le droit d’auteur, lorsqu’elle est originale, la fabrication, elle, ne peut être contrôlée que par l’intermédiaire d’un brevet. Et encore, un brevet ne portera pas vraiment sur la fabrication mais sur la commercialisation de cet objet. En somme, pour espérer qu’une licence « Open Hardware » fonctionne, il faut détenir un brevet sur le « hardware » en question. Ce qui veut dire payer pour obtenir un droit qu’on va offrir ensuite. C’est assez magnanime et gageons que rares sont ceux qui auront les moyens financiers d’une telle grandeur d’âme. Plusieurs pistes peuvent néanmoins êtres envisagées. On peut imaginer financer par crowdfunding certaines inventions pour ensuite les libérer. On pourrait également recourir au mécénat, mais la période n’y est pas nécessairement propice.

### Utiliser les licences libres dédiées aux œuvres de l’esprit
La seconde hypothèse, déjà mise en œuvre par de nombreux porteurs de projet comme une stratégie détournée, proche de la précédente, consiste à utiliser des licences plus classiques (Creative Commons, Art Libre, GFDL), normalement dédiées aux œuvres de l’esprit, sur la documentation de leurs projets. Les limites de cette démarche sont les mêmes que celles qui affectent les licences Open Hardware. **Les conditions posées par ces licences ne vaudront que pour des modifications apportées à la documentation (si elle est originale) et n’auront pas d’incidence sur la fabrication des objets en eux-mêmes.** Par ailleurs, même en cas de possession d’un brevet, ces licences resteraient invalides, car elles ne prennent pas en compte les droits de propriété industrielle.

### Jouer avec les conditions de la brevetabilité
Une troisième hypothèse consiste à jouer avec les conditions de la brevetabilité. En effet, pour qu’une invention soit brevetable, il faut qu’elle soit nouvelle. Cette condition de nouveauté en droit est entendue de manière très large puisque c’est une nouveauté absolue. Il suffit que le public ait eu accès à cette invention pour qu’elle soit comprise dans l’état de la connaissance. Cet état de la connaissance, sorte de domaine public, est un pot commun dans lequel chacun peut venir puiser. C’est peu ou proue ce qui se passe lorsqu’un bricoleur documente son travail dans un fablab. **L’idée est donc pour l’inventeur de révéler son invention au public pour en faire don à l’humanité. Cette démarche est également très séduisante mais comporte principalement trois failles.** La première est que si l’office chargé de délivrer les brevets ne voit pas que l’invention a été publiée, il pourra délivrer à tort un brevet à quelqu’un d’autre que l’inventeur. Bien entendu, l’inventeur pourra faire annuler le brevet mais il faudra alors passer devant un juge avec tous les inconvénients que cela représente (coût, temps, énergie…). Le deuxième problème est que si l’invention est déjà brevetée, celui qui se croyait le premier inventeur risque d’être assigné en contrefaçon, ce qui n’est jamais vraiment plaisant. Le troisième problème est que même si tout a été effectué correctement, l’inventeur ne peut pas empêcher une entreprise plus importante, ayant de plus gros moyens, de commercialiser son invention, y compris pour moins cher.

### Changer la loi
Une quatrième hypothèse, qui peut paraître certainement moins faisable, est de changer la loi pour qu’elle soit plus favorable au partage non marchand des créations industrielles. Dans ce cas, il va falloir sérieusement se retrousser les manches, car le droit français n’est pas seul au monde : il existe d’autres droits très similaires au droit français et la question relève largement du niveau européen et international. **Une très grande partie des états se sont entendus pour fixer les règles de bases de la propriété industrielle par le biais de traités et la France en fait partie.** Donc pour modifier le droit français, il faut que l’ensemble de ces pays soient d’accord avec la modification, ce qui est peu probable.

### Envisager un label « Open Hardware »
Enfin, une dernière piste à envisager et qui est un peu plus à la marge des propositions juridiques précédentes serait d’envisager une sorte de label « Open Hardware ». Cette stratégie viendrait s’ajouter à la révélation de l’invention. **L’idée serait alors de déterminer un certain nombre de conditions qui, si elles sont respectées, permettraient à une entreprise d’apposer ce label sur le produit vendu.** Sorte d’équivalent du « bio », mais en version invention. La question se pose de déterminer quelles pourraient être ces conditions, doivent-elles avoir un caractère rédhibitoire ? Etc. L’Open Source Hardware Association a d’ailleurs récemment lancé l’Open Source Hardware Certification V. 1. : affaire à suivre…

Il nous semble intéressant de préciser à cette étape que la culture libre et la propriété industrielle semblent bien loin l’une de l’autre. Pourtant, à certains égards, la propriété industrielle présente de sérieux avantages par rapport au droit d’auteur. En effet, la protection est drastiquement plus courte : entre 20 et 25 ans pour la propriété industrielle, et la vie de l’auteur (plus 70 ans) pour le droit d’auteur. Le domaine public se remplit donc plus vite d’un coté que de l’autre. De plus, en ce qui concerne la propriété industrielle, il est toujours possible de ne pas obtenir le droit, alors qu’en matière de droit d’auteur, l’auteur obtient automatiquement son droit et, s’il n’en veut pas, il doit ensuite l’abandonner ou l’ouvrir en optant pour une licence. En propriété industrielle, il est toujours possible de fabriquer une invention même brevetée. Ce qui est interdit c’est de la commercialiser, alors qu’en droit d’auteur l’acte de copie est plus ambigu.

À la lecture de ces quelques lignes, l’avenir commun de la culture libre et de la propriété industrielle semble peu évident. Certes, il existe un certain nombre de complications juridiques. Mais les choses ne sont jamais figées, elles évoluent en fonction de ce que les gens en font et le droit ne fait pas exception. De plus, en dépit de ces complications juridiques, il existe des sociétés qui font le choix de prendre le risque de ne pas avoir l’exclusivité sur une technologie. Elles estiment en général que l’apport des utilisateurs compensera la possibilité d’attaquer les concurrents (c’est le cas d’Arduino, certaines imprimantes 3D, et dans une certaine mesure le réseau Internet par exemple). **Dès lors, il importe d’accompagner ce type de démarche sur la Propriété Industrielle et la Culture Libre pour fournir un cadre plus formel pour les bricoleurs. C’est donc à un vaste chantier que les auteurs de cet article s’attellent avec pour ambition de modéliser des outils capables d’accompagner les inventeurs et bricoleurs qui souhaitent s’inscrire dans la culture Libre.** L’année qui vient sera sûrement l’occasion pour se retrouver sur cette thématique et la porte est grande ouverte à celles et ceux qui souhaitent apporter leur pierre à l’édifice.

Vladimir Ritz, explorateur associé de PiNG, doctorant en propriété intellectuelle et associé au groupe de réflexion C LiBRE.
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). </br>
<a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license"><img style="border-width: 0;" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" alt="Creative Commons License"></a>
