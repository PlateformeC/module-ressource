---
title: 'Les doigts dans la prise #1 : le métal'
type_ressource: audio
feature_image: DSC00690-1024x683.jpg
license: cc-by-nc-sa
date: '25-01-2019 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Thomas Bernardi'
aura:
    pagetype: website
    description: 'Magnésium, nickel, yttrium, cuivre, indium, lanthane, aluminium, tantale, cadmium, antimoine, lithium, praséodyme, cobalt, néodyme, or, argent, plomb : pour cette première chronique sur la radio nantaise SUN, Thomas Bernardi nous parle du métal dans les technologies.'
    image: DSC00690-1024x683.jpg
metadata:
    description: 'Magn&eacute;sium, nickel, yttrium, cuivre, indium, lanthane, aluminium, tantale, cadmium, antimoine, lithium, pras&eacute;odyme, cobalt, n&eacute;odyme, or, argent, plomb : pour cette premi&egrave;re chronique sur la radio nantaise SUN, Thomas Bernardi nous parle du m&eacute;tal dans les technologies.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/sun01_metal_et_technologies'
    'og:type': website
    'og:title': 'Les doigts dans la prise #1 : le m&eacute;tal | Espace Ressources Num&eacute;riques'
    'og:description': 'Magn&eacute;sium, nickel, yttrium, cuivre, indium, lanthane, aluminium, tantale, cadmium, antimoine, lithium, pras&eacute;odyme, cobalt, n&eacute;odyme, or, argent, plomb : pour cette premi&egrave;re chronique sur la radio nantaise SUN, Thomas Bernardi nous parle du m&eacute;tal dans les technologies.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/sun01_metal_et_technologies/DSC00690-1024x683.jpg'
    'og:image:type': image/webp
    'og:image:width': '1024'
    'og:image:height': '683'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #1 : le m&eacute;tal | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Magn&eacute;sium, nickel, yttrium, cuivre, indium, lanthane, aluminium, tantale, cadmium, antimoine, lithium, pras&eacute;odyme, cobalt, n&eacute;odyme, or, argent, plomb : pour cette premi&egrave;re chronique sur la radio nantaise SUN, Thomas Bernardi nous parle du m&eacute;tal dans les technologies.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/sun01_metal_et_technologies/DSC00690-1024x683.jpg'
    'article:published_time': '2019-01-25T16:31:00+01:00'
    'article:modified_time': '2020-12-09T12:39:25+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Pour écouter le podcast c’est par [ici](http://www.lesonunique.com/content/les-doigts-dans-la-prise-metal-technologies) !

Magnésium, nickel, yttrium, cuivre, indium, lanthane, aluminium, tantale, cadmium, antimoine, lithium, praséodyme, cobalt, néodyme, or, argent, plomb : pour cette première chronique sur la radio nantaise SUN, Thomas Bernardi nous parle du métal dans les technologies.

![Les doigts dans la prise - metal et technologies](1%20-%20Les%20doigts%20dans%20la%20prise%20-%20metal%20et%20technologies.mp3)

Bonjour, bienvenu sur la nouvelle chronique de PiNG sur SUN, une chronique pour décrypter le numérique et plus particulièrement ce que l’on nomme les technologies. Nous verrons un peu plus loin ce qui se cache derrière ce terme à la fois banal et sibyllin.
Aujourd’hui, une première chronique introductive pour planter le décor.
On joue le jeu du mot de la semaine sur SUN : le métal.
Ça tombe bien, car le métal est un élément constitutif des technologies.
Des technologies support des bricolages et enquêtes de ping depuis 15 ans dans le but de mieux comprendre ce qui se cache derrière ce concept.

Mais qu’est-ce c’est que cette technologie ?
Alors un peu d’histoire !
D’après l’historien des techniques Guillaume Carnino : « la définition étymologique de la technologie comme discours sur la technique (Tekne + Logos) prévaut jusqu’au 19ème siècle (…) à la fois « discours sur » et « science de » la technique ».
Par la suite historiens, sociologues et philosophes de la Technique s’emploient à mieux cerner la différence entre ces deux termes – technique et technologies – et l’actualisation de leur définition : la technique serait un ensemble de savoir-faire et d’outils, alors que la technologie correspondrait plutôt aux procédures industrielles et aux machines.
Encore Guillaume Carnino nous dit : « La technologie n’est donc plus un discours sur la technique mais une rationalisation scientifique de la technique devenue techno-science. »

Pour Jean-Pierre Séris, philosophe de la technique, la technologie « c’est le nom de la technique dont nous nous sentons dépossédés. Elle se fait hors de nous, sans nous. »

Avez-vous déjà été associé à la fabrication d’ un téléphone portable ?

Ces technologies donc sont belle et bien matérielles, elles ne sont pas hors-sol, éthérées, légères comme semble le raconter la fable du cloud, de la dématérialisation, du virtuel ou encore des technologies vertes.

Non, non, non, le métal qui les constitue en grande partie est bel et bien extrait du sol ! Tout un ensemble de métaux plus ou moins précieux, plus ou moins lourds mais jamais renouvelables malgré leur importante utilisation dans les énergies dites renouvelables.
Un rapport de la Banque Mondiale de 2015 sur l’économie décarbonée montre que « les technologies requises pour limiter le réchauffement du climat feront grimper la demande de ressources naturelles ». Les auteurs préviennent : « Si elle n’est pas correctement gérée, cette croissance de la demande de minerais et métaux pourrait aller à l’encontre des efforts et des politiques des pays riches en ressources pour atteindre les objectifs qu’ils se sont fixés en matière de changement climatique ».

Magnésium, nickel, yttrium, cuivre, indium, lanthane, aluminium, tantale, cadmium, antimoine, lithium, praséodyme, cobalt, néodyme, or, argent, plomb.

Tous ces métaux sont extraits du sol, dans des mines ou par le biais de techniques de top moutain removal, puis raffinés, transportés sur des kilomètres, transformés dans des usines voraces en énergie et en ressources humaines peu coûteuses pour ne pas dire exploitées.
Les technologies et Internet ne sont donc pas quelque part dans le ciel vaporeux de l’intelligence collective, elles sont extraites du sol, elles sont industries, elles sont mines, elles sont terrestres, elles sont métal.

Merci !

Thomas Bernardi