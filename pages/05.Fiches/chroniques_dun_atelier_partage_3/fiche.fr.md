---
title: 'Chroniques d’un Atelier Partagé #3'
media_order: IMG_1779_light-870x550.jpg
type_ressource: text
feature_image: IMG_1779_light-870x550.jpg
license: cc-by-nc-sa
serie:
    -
        page: /fiches/chroniques_dun_atelier_partage_1
    -
        page: /fiches/chroniques_dun_atelier_partage_2
    -
        page: /fiches/chroniques_dun_atelier_partage_4
    -
        page: /fiches/chroniques_dun_atelier_partage_5
date: '12-11-2018 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'atelier partagé'
        - low-tech
        - réparation
        - chroniques
    author:
        - 'Édouard Bourré-Guilbert'
aura:
    pagetype: website
    description: 'Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé.'
    image: IMG_1779_light-870x550.jpg
metadata:
    description: 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/chroniques_dun_atelier_partage_3'
    'og:type': website
    'og:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #3 | Espace Ressources Num&eacute;riques'
    'og:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/chroniques_dun_atelier_partage_3/IMG_1779_light-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #3 | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/chroniques_dun_atelier_partage_3/IMG_1779_light-870x550.jpg'
    'article:published_time': '2018-11-12T00:00:00+01:00'
    'article:modified_time': '2020-11-26T10:52:19+01:00'
    'article:author': Ping
---

*Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé. En tant qu’explorateur associé de PiNG, Édouard nous livre son regard sur l’aventure de l’Atelier Partagé du Breil, que l’association anime depuis maintenant 3 ans. Une façon de nous faire découvrir ce qui compose un tel lieu : ses figures, ses temps forts, ses zones de tensions et ses petits miracles.*

De la même manière qu’il n’y a pas de fumée sans feu, ou encore de marteau sans clou, on peut affirmer sans risque qu’il n’y a pas de bricole sans bricolo.

## ÉPISODE 3 : LES BRICOLOS DU MARDI

Pour certains, faire corps avec la trotteuse d’une Rolex donne le sentiment d’avancer dans la vie. Pour moi, c’est plutôt le moment où j’ai pu distinguer les mèches bois des mèches bétons. Ce jour là, je crois bien avoir laissé s’échapper une larme. J’étais à deux doigts d’appartenir à cette espèce, celle qu’on appelle communément le bricoleur.

La bricole n’a pas eu une histoire facile. Oh non. Il fut un temps lointain – que les moins de 20 ans ne peuvent pas connaître – où la bricole désignait un engin de guerre. Quelques siècles plus tard, elle devient le bruit que fait la balle au jeu de paume. On l’utilise également pour parler d’un chien qui aime zigzaguer. Parlez-en à votre copain, celui assez étrange pour aimer les mots croisés, vous verrez. Mais revenons à notre bricole. Plus récemment, on commence à l’associer à la fabrication, celle de l’amateur, du pas sérieux, de la dilettante ou pire encore, à celui qui n’a pas de métier. « Bouh ! » le chômeur qui bricole au lieu de chercher du travail ! Et puis, comme si le seigneur n’en avait pas suffisamment sur la conscience, voilà pas qu’on lui pollue sa belle journée en lui imposant le bricoleur du dimanche.
Et bien, à l’Atelier Partagé du Breil, on invente le bricolo du XXIème siècle. Il était temps, après presque vingt années passées dans ce nouveau millénaire, d’adjoindre autre chose que le vulgaire « e. » devant ce nom commun, et surtout de lui créer ses lettres de noblesse, pour lui et pour la postérité.

Tenez-vous bien, en 2018, il n’y a pas un genre de bricolo, il y en a pléthore. Tenez-vous encore mieux, chez PiNG, à chaque bricolo correspond son heure. Il y a le bricolo d’un jour, celui qui vient pour un événement en particulier et qui, rassasié, ne remettra probablement jamais les pieds à l’atelier. Celui qui vient avec assiduité pour mener un projet de plusieurs semaines. C’est le vagabond de la bricole, il disparaît une fois le travail réalisé, et réapparaît un beau jour, sans crier gare, avec un nouveau projet dans les bras.
Il y a également un genre tout particulier ayant élu domicile le mardi après-midi et qui participe, telle une cheville ouvrière, à faire tourner l‘atelier. Il apparaît généralement au moment d’allumer les fers à souder comme un génie malicieux qui aurait reniflé la bonne odeur d’étain. C’est le bricolo volontaire. Il aime ça et ce n’est pas récent. Expérimenté, il sait faire chauffer le multimètre aussi bien que la scie à métaux. Cerise sur le gâteau, il a envie de transmettre son savoir ancestral. Celui-là, on se l’arrache en le soudoyant avec un café frais dès qu’il met les pieds dans la pièce.
Mais continuons notre bricoloscope. Il ne faudrait pas oublier de parler du bricolo-exhibitionniste, celui qui dégaine son avis de derrière son grand manteau et décampe aussitôt. Ou encore le bricolo de 16h tapante, qui tourne autour du pâté de maison depuis une heure en attendant l’ouverture du lieu. Le bricolo des lilas, qui vient faire uniquement des petits trous avec la perceuse. Le bricolo-casseur, qui détruit volontairement ses objets pour avoir un prétexte de venir. J’en oublie, bien sûr. En vérité, le bricolo est à l’être humain ce que la météo est au printemps, variable. Et surtout changeant. Car une fois sa bricole réalisée, un bricolo peut très bien évoluer, muter vers d’autres horizons. Qu’on se le dise, le bricolo n’est pas genré !

Vous l’aurez probablement compris en arrivant à ces lignes, la recette pour passer de la simple pièce vierge à un lieu de vie dynamique nécessite plusieurs variétés de bricolos. Mais pas que. Parmi les personnes qui mettent l’ambiance, il y a les instigateurs du concept. Ensuite, tout est question de couches qui se superposent.

Pour réussir vos lasagnes « low-tech/éducation populaire » du premier coup, suivez bien les étapes suivantes :

1/ prenez un plat pouvant aller au four. Une salle non climatisée fera l’affaire ;

2/ tapissez le fond d’un corps gras. On s’accommodera d’une serpillière usagée ;

3/ pour la première couche de pâte, pensez à fonder une association de loi 1901, avec une bonne dose d’ardent défenseurs à la cause qui créeront les statuts adéquats à la sueur de leurs méninges ;

4/ pour la seconde couche, accueillez des personnes en Services Civiques. Une pour douze bricolos en moyenne. Au delà, on pourra envisager de recruter Mac Gyver ;

5/ pour la troisième couche, fédérez des bénévoles impliqués. En cas de pénurie, laisser des tracts auprès de Pôle Emploi et des Caisses de retraites, voir carrément de la SÉCU, une séance à l’atelier partagé soigne de tous les maux ;

6/ pour la sauce, vous ne serez pas maître de tous les ingrédients, c’est l’idée, l’important étant de laisser mijoter. Vous constaterez qu’elle s’améliore à chaque fois qu’elle est réchauffée ;

7/ pour lier l’ensemble, plutôt qu’une béchamel, prévoyez un outillage diversifié et une méthodologie évolutive, sans oublier une bonne machine à café et des gâteaux ;

8/ saupoudrez le tout d’une ambiance électrique pour créer le côté croustillant ;

9/ pour le service, pensez à une table collective, car c’est bien le dénominateur commun du lieu : faire ensemble. Échanger des savoirs, des techniques, des réussites et des échecs mais surtout des moments de vie.

Cette recette est libre de droit, elle peut et doit circuler pour être incrémentée, modifiée, adaptée aux usages de chaque projet. Et si jamais vous voulez goûter nos lasagnes avant de les reproduire, vous savez ce qu’il vous reste à faire. Calez votre montre sur le fuseau horaire de l’Atelier partagé du Breil, TOUSLESMARDI16h-21h. Passez à l’improviste, il y aura toujours du rab !
