---
title: '1.CAMP - Roscoff'
media_order: 597545226.jpg
type_ressource: video
feature_image: 597545226.jpg
license: cc-by-nc-sa
date: '01-01-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - anthropocène
    author:
        - 'Vincent Pouplard'
aura:
    pagetype: website
    description: 'Le réalisateur Vincent Pouplard filme 1.CAMP à Roscoff.'
    image: 597545226.jpg
metadata:
    description: 'Le r&eacute;alisateur Vincent Pouplard filme 1.CAMP &agrave; Roscoff.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/vincent_pouplard_sea_watch'
    'og:type': website
    'og:title': '1.CAMP - Roscoff | Espace Ressources Num&eacute;riques'
    'og:description': 'Le r&eacute;alisateur Vincent Pouplard filme 1.CAMP &agrave; Roscoff.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/vincent_pouplard_sea_watch/597545226.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1920'
    'og:image:height': '1080'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '1.CAMP - Roscoff | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Le r&eacute;alisateur Vincent Pouplard filme 1.CAMP &agrave; Roscoff.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/vincent_pouplard_sea_watch/597545226.jpg'
    'article:published_time': '2017-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T16:51:58+01:00'
    'article:author': Ping
---

*Vincent Pouplard est réalisateur. Son travail documentaire s’articule autour de récits de vie et d’expérience. Il a récemment signé PAS COMME DES LOUPS dont la sortie en salles est prévue pour le mois d’avril 2017. Pour 1 camp, il a été attentif à rendre sensible, à représenter par le geste et la parole un projet qui le plongeait dans un univers scientifique intrigant, fascinant, méconnu.*

<iframe src="https://player.vimeo.com/video/187682069" width="100%" height="600" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
