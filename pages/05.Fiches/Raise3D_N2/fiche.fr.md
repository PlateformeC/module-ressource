---
title: 'Raise3D N2 (Imprimante 3D)'
media_order: Raise1.jpg
type_ressource: text
feature_image: Raise1.jpg
license: cc-by-nc-sa
date: '01-01-2017 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
        - impression3D
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cette machine sert à l''impression 3D.'
    image: Raise1.jpg
metadata:
    description: 'Cette machine sert &agrave; l''impression 3D.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/raise3d_n2'
    'og:type': website
    'og:title': 'Raise3D N2 (Imprimante 3D) | Espace Ressources Num&eacute;riques'
    'og:description': 'Cette machine sert &agrave; l''impression 3D.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/raise3d_n2/Raise1.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1010'
    'og:image:height': '734'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Raise3D N2 (Imprimante 3D) | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine sert &agrave; l''impression 3D.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/raise3d_n2/Raise1.jpg'
    'article:published_time': '2017-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T16:32:00+01:00'
    'article:author': Ping
---

*Cette machine sert à l'impression 3D.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Raise3D_N2/Modedemploi?classes=btn)
