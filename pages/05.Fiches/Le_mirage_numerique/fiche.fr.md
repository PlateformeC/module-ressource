---
title: 'Morozov, Le mirage numérique, pour une politique du Big Data'
media_order: le_mirage_numerique.jpg
type_ressource: text
feature_image: le_mirage_numerique.jpg
license: cc-by-nc-sa
date: '01-01-2019 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - essai
        - données
        - culturenumérique
    author:
        - 'Meven Marchand Guidevay'
aura:
    pagetype: website
    description: 'Evgeny Morozov travaille sur les conséquences sociales et politiques des nouvelles technologies. Il est auteur de Pour tout résoudre, cliquez ici.'
    image: le_mirage_numerique.jpg
show_breadcrumbs: true
metadata:
    description: 'Evgeny Morozov travaille sur les cons&eacute;quences sociales et politiques des nouvelles technologies. Il est auteur de Pour tout r&eacute;soudre, cliquez ici.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/le_mirage_numerique'
    'og:type': website
    'og:title': 'Morozov, Le mirage num&eacute;rique, pour une politique du Big Data | Espace Ressources Num&eacute;riques'
    'og:description': 'Evgeny Morozov travaille sur les cons&eacute;quences sociales et politiques des nouvelles technologies. Il est auteur de Pour tout r&eacute;soudre, cliquez ici.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/le_mirage_numerique/le_mirage_numerique.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1000'
    'og:image:height': '518'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Morozov, Le mirage num&eacute;rique, pour une politique du Big Data | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Evgeny Morozov travaille sur les cons&eacute;quences sociales et politiques des nouvelles technologies. Il est auteur de Pour tout r&eacute;soudre, cliquez ici.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/le_mirage_numerique/le_mirage_numerique.jpg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:40:33+01:00'
    'article:author': Ping
---

## Résumé :

Evgeny Morozov travaille sur les conséquences sociales et politiques des nouvelles technologies. Auteur de *Pour tout résoudre, cliquez ici* (2014), il collabore à *The New Republic* et écrit régulièrement pour le *Guardian*.

Il résume lui même ainsi son ouvrage : « Ce livre défend une thèse simple : aujourd’hui, ceux qui participent aux débats sur la technologie soutiennent, souvent à leur insu, l’idéologie néolibérale dans ce qu’elle a de pire. La plupart des critiques de la Silicon Valley, de quelque bord qu’ils soient, sont alignés sur le néolibéralisme. Ce n’est pas parce qu’ils haïssent Google et Amazon qu’ils combattent la rapacité du capitalisme financiarisé.
Une critique technologique émancipatrice est-elle possible ? J’en suis convaincu. Mais il faut d’abord comprendre les insuffisances de la critique actuelle. Elle est inoffensive pour une raison précise, et n’a qu’un moyen de se rendre plus radicale et plus conséquente : elle doit étudier sérieusement l’économie politique de la Silicon Valley, mais aussi la place grandissante que celle-ci occupe au sein de l’architecture fluide, constamment en évolution du capitalisme mondial.
La Silicon Valley nous fait souvent de fausses promesses, mais le problème n’est pas là. Le problème, c’est surtout que ces promesses ont pour toile de fond la disparition de l’État social, remplacé par des modèles plus légers, plus rapides, plus cybernétiques ; le problème a trait au rôle que le libre flux des données est appelé à jouer dans un commerce mondial totalement dérégulé. […] La Silicon Valley n’a d’avenir que sous le régime du capitalisme contemporain, et le capitalisme contemporain n’a d’avenir que sous le régime de la Silicon Valley. »

## Notes de Meven :

L'idée centrale de l'ouvrage est que *"la raison pour laquelle le débat sur le numérique paraît aussi creux et inoffensif est simple : précisément parce qu'il porte sur le *"numérique"* plutôt que sur "la politique" et "l'économie""*.

Morozov constate que l'imagination technologique est aujourd'hui débridée, mais qu'à l'inverse *"notre imagination institutionnelle s'est figée, et, avec elle, le potentiel démocratisant des technologies radicales"* Il reproche à *"la gauche"* de s'être montrée incapable de *"proposer des politiques solides en matière de technologie, des politiques susceptibles de contrecarrer l'innovation, le "bouleversement", la privatisation promue par la Silicon Valley"*.

En conséquence, il enjoint à quitter le débat sur le numérique et remiser les arguments invoquant l'essence même des choses (technologie, information, Internet), pour déplacer la critique sur le régime politique et économique contemporain *"où le complexe militaro-industriel côtoie un système bancaire hors de contrôle et un secteur publicitaire qui l'est tout autant"* et où la régulation algorithmique voudrait dépasser l'arbitrage - politique - nécessaire entre *"des visions souvent mutuellement incompatibles, de la vie en commun"*.

## Citer l'ouvrage :
Morozov, E. (2015). Le Mirage numérique : Pour une politique des big data. Les Prairies Ordinaires.
