---
title: 'Les doigts dans la prise #3 : avec la langue'
type_ressource: audio
feature_image: IMG_0670.jpg
license: cc-by-nc-sa
published: true
date: '22-03-2019 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Charlotte Rautureau'
aura:
    pagetype: article
    description: 'Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne Le Fil de l’histoire.'
    image: IMG_0670.jpg
metadata:
    description: 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/sun03_mars19_aveclalangue'
    'og:type': article
    'og:title': 'Les doigts dans la prise #3 : avec la langue | Espace Ressources Num&eacute;riques'
    'og:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/sun03_mars19_aveclalangue/IMG_0670.jpg'
    'og:image:type': image/webp
    'og:image:width': '800'
    'og:image:height': '533'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #3 : avec la langue | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/sun03_mars19_aveclalangue/IMG_0670.jpg'
    'article:published_time': '2019-03-22T16:31:00+01:00'
    'article:modified_time': '2020-12-09T12:44:00+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Pour écouter le podcast c’est par [ici](http://www.lesonunique.com/content/les-doigts-dans-la-prise-avec-la-langue) !

![Les doigts dans la prise - avec la langue](3%20-%20Les%20doigts%20dans%20la%20prise%20-%20avec%20la%20langue.mp3)

Le thème de l’émission d’aujourd’hui s’intitule « avec la langue ». En digne (enfin je l’espère) représentante pour cette chronique de l’association PiNG, j’ai longtemps hésité à consacrer cette salve radiophonique à la technolangue propre à la culture numérique et vous inviter à découvrir ou redécouvrir tout ces termes qui composent le jargon de la start-up nation ou de la silicon Valley. J’aurais dès lors pu vous pitcher en 3 minutes un texte disruptif ponctué de buzzwords et autres hashtags que seuls les initiés connaissent. Toi même tu sais.

J’aurais pu… Mais le sujet que j’ai choisi est tout autre. Aujourd’hui, je voulais explorer avec vous une forme de langue ou de langage propulsé par les technologies numériques, et en premier lieu les smartphones, à savoir les smileys, émoticônes et autres émojis que nous utilisons pour communiquer. Je suis en effet assez fascinée je l’avoue de constater combien **ces dessins relativement naïfs ont pris une telle ampleur dans nos échanges.**

Tout d’abord, que les choses soient claires ou s’éclairent pour tous (moi la première) : smiley, émoticônes ou émojis ça n’est pas la même chose !! Soyons précis, haro sur les abus de langage. Le smiley est ce dessin assez simple d’un visage rond, souriant, coloré en jaune et souhaitant exprimer une émotion positive. Apparu courant des années 70, ce symbole largement diffusé dans la culture populaire, a été une source d’inspiration des actuels dessins que nous nous échangeons sur nos smartphones. Les émoticones, eux, sont les symboles que l’on écrit en utilisant les caractères du clavier comme par exemple 😉 qui renvoie au clin d’oeil. Enfin, les emojis, sont tous les symboles que nous retrouvons aujourd’hui sur les smartphones, tablettes, ordinateurs ou réseaux sociaux et dont nous ponctuons nos messages. **Si la terminologie diffère, tous (smiley, émoticônes ou émojis) ont en commun de chercher à transmettre à travers le symbole une émotion.**

La question qui me vient alors à l’esprit une fois ces vérités rétablies est la suivante : qui opère dans le plus grand secret pour nous abreuver de ces petits symboles dont nous sommes si friands ? Qui fait la pluie et le beau temps sur le monde des émojis ? Dans une enquête de la plus haute importance intitulée [« Pourquoi vous n’aurez jamais d’emoji raclette dans votre smartphone »](https://www.lemonde.fr/pixels/article/2019/03/02/pourquoi-vous-n-aurez-jamais-d-emoji-raclette-dans-votre-smartphone_5430435_4408996.html) publiée le 2 mars dernier, les journalistes du Monde nous révèlent les coulisses de la création des émojis.

Tout un chacun peut proposer la création d’un nouvel émoji. Pour ce faire, il faut envoyer un dossier étayé au consortium Unicode. Basée à la Silicon Valley, cette organisation regroupe sur adhésion entre autres les grands du numérique (dont les GAFA), des pays, des universités. Sa mission est de **définir les standards d’affichage des caractères entre appareils électroniques, dont les émojis, dans un soucis d’interopérabilité.**

Les journalistes du Monde ont eux-même fait l’expérience de proposer un nouvel émoji : celui de la raclette. Il leur a fallu défendre leur bout de fromage dans un dossier complet expliquant notamment en quoi leur symbole sera très utilisé, qu’il y a un véritable besoin, prouver qu’il peut avoir plusieurs significations, qu’il exprime un concept jusque là non présent dans le spectre des émojis, démontrer qu’il est facilement identifiable et ne prête pas à confusion etc. Malgré les efforts des journalistes et les arguments créatifs déployés, la commission a tranché dans la meule savoyarde et répondu, je cite «  Cela nous semble être un plat trop obscur, pas assez universel pour en faire une priorité à ajouter aux émojis nourriture. ». RIP l’émoji raclette !

Si l’on en vient à analyser les émojis comme un langage à part entière, on pourrait croire que le fait qu’il s’agisse d’un dessin, d’un symbole permette l’universalité défendue par Unicode et une fluidité dans la communication entre les individus. Il n’en est rien. **L’usage comme la compréhension de cet esperanto-symbolico-numérique varie fonction des communautés, des groupes et des cultures.** Le rapport SwiftKey sur les Emoji qui date de 2015 a traité plus d’un milliard d’emoji pour comprendre comment ils sont utilisés par des utilisateurs de 16 régions et langues différentes. Cette étude révèle que les choix et la fréquence d’utilisation des émojis varient d’un pays à l’autre et que l’on pourrait reconnaître la nationalité à partir des emoji utilisés. Ainsi, la France serait la championne dans l’usage des emojis de type « cœur » et de type « mariage » et ne dérogerait pas à sa réputation de pays de l’amour…

Par ailleurs, au sein d’une même nationalité, culture, communauté voire même d’un groupe d’ami similaire, l’universalité des symboles est mise à mal. Savez-vous toujours ce que cachent comme messages les émojis envoyés par vos amis ? Pourquoi votre copine Alice vous envoie t’elle de bon matin cet émoji panda ? A t’elle eu une nuit difficile ou au contraire très douce ? Vous envoie t’elle un message subliminal d’inspiration pour son futur cadeau d’anniversaire ? Est-elle inquiète, suite à sa lecture d’un article du Monde, de la disparition des espèces ? On constate ici que **les symboles ne se suffisent pas toujours à eux-mêmes et que quelques mots demeurent toujours utiles pour éclairer un propos.** Mais, à l’heure de l’accélération des flux et des échanges dans laquelle nous propulsent les technologies numériques, avons-nous seulement le temps de ces quelques mots ? Vous avez 4h !

Charlotte Rautureau
