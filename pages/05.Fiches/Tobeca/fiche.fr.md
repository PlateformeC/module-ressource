---
title: 'Tobeca (Imprimante 3D)'
media_order: TOBECACOMPLETE.png
type_ressource: text
feature_image: TOBECACOMPLETE.png
license: cc-by-nc-sa
date: '01-01-2019 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
        - impression3D
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cette machine sert à l''impression 3D.*'
    image: TOBECACOMPLETE.png
metadata:
    description: 'Cette machine sert &agrave; l''impression 3D.*'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/tobeca'
    'og:type': website
    'og:title': 'Tobeca (Imprimante 3D) | Espace Ressources Num&eacute;riques'
    'og:description': 'Cette machine sert &agrave; l''impression 3D.*'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/tobeca/TOBECACOMPLETE.png'
    'og:image:type': image/png
    'og:image:width': '2100'
    'og:image:height': '1800'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Tobeca (Imprimante 3D) | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine sert &agrave; l''impression 3D.*'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/tobeca/TOBECACOMPLETE.png'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T16:42:20+01:00'
    'article:author': Ping
---

*Cette machine sert à l'impression 3D.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Tobeca?classes=btn)
