---
title: 'Éclairage sur le rapport entre propriété intellectuelle et lieux de médiation'
media_order: img_2663.jpg
type_ressource: text
feature_image: img_2663.jpg
license: cc-by-sa
date: '15-05-2015 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'propriété intellectuelle'
        - 'culture libre'
        - opensource
        - médiation
    author:
        - 'Vladimir Ritz'
aura:
    pagetype: website
    description: 'Un article de Vladimir Ritz, doctorant en propriété intellectuelle et membre de la CEPI (Cellule d''Expérimentation sur les Propriétés Immatérielles).'
    image: img_2663.jpg
metadata:
    description: 'Un article de Vladimir Ritz, doctorant en propri&eacute;t&eacute; intellectuelle et membre de la CEPI (Cellule d''Exp&eacute;rimentation sur les Propri&eacute;t&eacute;s Immat&eacute;rielles).'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/eclairage_sur_le_rapport_entre_propriete_intellectuelle_et_lieux_de_mediation'
    'og:type': website
    'og:title': '&Eacute;clairage sur le rapport entre propri&eacute;t&eacute; intellectuelle et lieux de m&eacute;diation | Espace Ressources Num&eacute;riques'
    'og:description': 'Un article de Vladimir Ritz, doctorant en propri&eacute;t&eacute; intellectuelle et membre de la CEPI (Cellule d''Exp&eacute;rimentation sur les Propri&eacute;t&eacute;s Immat&eacute;rielles).'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/eclairage_sur_le_rapport_entre_propriete_intellectuelle_et_lieux_de_mediation/img_2663.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '620'
    'og:image:height': '165'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '&Eacute;clairage sur le rapport entre propri&eacute;t&eacute; intellectuelle et lieux de m&eacute;diation | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Un article de Vladimir Ritz, doctorant en propri&eacute;t&eacute; intellectuelle et membre de la CEPI (Cellule d''Exp&eacute;rimentation sur les Propri&eacute;t&eacute;s Immat&eacute;rielles).'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/eclairage_sur_le_rapport_entre_propriete_intellectuelle_et_lieux_de_mediation/img_2663.jpg'
    'article:published_time': '2015-05-15T00:00:00+02:00'
    'article:modified_time': '2020-11-26T13:51:30+01:00'
    'article:author': Ping
---

*Un article de Vladimir Ritz, doctorant en propriété intellectuelle et membre de la CEPI (Cellule d'Expérimentation sur les Propriétés Immatérielles).*

## Idées reçues et préjugés :

La propriété intellectuelle et les lieux de médiation. Qu’ont-ils de commun ? A priori pas grand chose et pourtant, tels deux compagnons de chambrée, ils partagent bien plus que quatre murs et un toit.

La propriété intellectuelle est partout, enfin, il paraît. Elle fait l’apologie du modèle propriétaire, enfin, il paraît. Il paraît surtout qu’il faut se méfier des apparences et des idées reçues. La propriété intellectuelle est bien plus permissive qu’elle n’y paraît et les lieux de médiations sont un parfait exemple de cette souplesse qui permet le partage des connaissances, la promotion des biens communs …

Les animateurs multimédia et les médiateurs numériques, parfois sans le savoir, sont confrontés au droit de la propriété intellectuelle. C’est le cas par exemple lorsqu’il réalisent une vidéo, écrivent un texte, font un reportage photo ou encore lorsqu’ils téléchargent un logiciel libre. Autant d’actes qui ont un rapport avec le droit ou, tout du moins, que le droit de la propriété intellectuelle a pour vocation de réglementer.

Or, ce qu’il faut garder à l’esprit c’est que la propriété intellectuelle c’est le droit d’interdire l’usage de l’œuvre mais aussi le droit de l’autoriser. Il y a, alors, deux façons de diffuser les œuvres de l’esprit, à savoir en faire un usage propriétaire ou en favoriser l’usage commun. Il semble que, de ce point de vue, les lieux de médiations s’inscrivent plus dans l'usage commun : partage, appropriation de la technique mais aussi diffusion des contenus culturels.

## Des licences de plus en plus compliquées :

Il existe depuis plus de trente ans des initiatives qui cherchent à exploiter ce « droit d’autoriser » garanti par la propriété intellectuelle pour développer le partage des contenus culturels. Les licences libres s’appuient donc sur une base légale : ce fameux droit de la propriété intellectuelle tant contesté. À l’origine, le fer de lance a été le logiciel libre. Quatre libertés sont alors consacrées : la liberté d’utiliser le logiciel, la liberté de l’étudier, la liberté de le modifier et enfin la liberté de le redistribuer. Rapidement cette démarche de libération de l’usage s’est étendue à d’autres domaines : textes, vidéos, musique, Art…
Apparaissent alors un certain nombre de licences dont les plus connues sont les Creative Commons. Ces licences qui n’ont pas vocation à s’appliquer aux logiciels ont du s’adapter aux domaines variés de la création. Dans le cas des Creative Commons, il est possible de choisir si l’utilisateur aura le droit de faire un usage commercial ou non de l’œuvre, s’il aura le droit ou non de la modifier. Le point commun de toutes étant qu’il est nécessaire de respecter la paternité de l’œuvre c’est-à-dire identifier l’auteur.

Par la suite, le mouvement des licences libres prend de l’ampleur et ne cesse de se perfectionner. De nouvelles licences libres apparaissent comme la licence « domaine public ». Mais également les licences s'affinent : la « peer production licence » implique une réciprocité dans le partage des biens culturels (ce que nous empruntons aux communs nous devons le rendre d’une manière ou d’une autre aux communs). Cela implique que le mouvement du libre se complexifie et qu’il n’est, dès lors, plus tout à fait à la portée de tous.

Il y a de plus en plus de licences libres (CeCILL, GNU/GPL, Art Libre, Apache, etc.), certaines plus lisibles que d’autres tant et si bien qu’il est parfois difficile de se repérer dans ces méandres. De plus la complexification des licences libres nécessite de comprendre à la fois les bases juridiques et les bases idéologiques du mouvement des licences libres. Le partage aussi bien que l’usage de contenu sous licence libre devient une affaire de spécialistes qui nécessite un effort d’appropriation (pour en comprendre les tenants et les aboutissants). Car ne nous voilons pas la face, bien souvent l’utilisateur n’aura pas la volonté d’aller à l’encontre de ce que l’auteur autorise. Il n’est pas conscient d’enfreindre ladite licence libre simplement parce qu’il ne la comprend pas.
Il est donc nécessaire de s’ouvrir à ce mode de diffusion et de le comprendre. Là est le lien entre la propriété intellectuelle et les lieux de médiations. Et cela concerne à la fois les animateurs multimédia et les usagers de lieux de médiation.

## Que retenir pour les lieux de médiations ?

Il y a deux volets aux licences libres. Le premier est la **« diffusion »** d’un contenu culturel protégé par le droit d’auteur sous licence libre. Il faut alors bien identifier quelle licence utiliser en fonction du bien culturel et de la volonté du ou des auteurs. Et il y a un volet **« utilisation »** de logiciels libres, de musique libre, d’art libre … Là, il faut comprendre la volonté exprimée par le ou les auteurs.
Les EPN peuvent être confrontés dans leurs pratiques à ces deux volets.

Prenons l’exemple d’un EPN qui lance un projet vidéo. Pour ce projet il fait appel à des personnes qui vont « jouer » dans la vidéo, il télécharge des musiques libres et les utilise pour le film. Pour monter ce film, il utilise un logiciel libre et le diffuse sur internet.

- *Volet « diffusion »*

Il y a eu création d’un contenu culturel qui a priori est protégé par le droit d’auteur (et cela indépendamment de la volonté de l’auteur et sans condition de dépôt : il est protégé c’est tout). Bien souvent, la volonté ne va pas être de commercialiser ce film. Il est là pour faire connaître une initiative locale. Si rien n’est précisé lors de la diffusion, alors par défaut, personne sans l’autorisation de l’auteur ne peut utiliser ce film.

Or peut-être que ce n’est pas la volonté du ou des auteurs. Peut-être souhaitent-ils que ce film soit utilisé dès lors qu’aucun usage commercial n’en est fait et à condition que les auteurs soient crédités. Le choix de la licence libre *Creative Commons* CC-BY-NC peut alors être un atout intéressant tant du point de vue d'une large diffusion pour le créateur, que du point de vue de la compréhension du cadre légal par l’utilisateur. Il peut donc être intéressant pour l’EPN de maîtriser le volet « diffusion ».

- *Volet « utilisation »*

Il doit également maîtriser le volet « Utilisation » car dans notre exemple, de la musique libre a été utilisée. Or si l’auteur de cette musique a opté pour la licence Creative Commons CC-BY-NC-SA alors le film réalisé par l’EPN s’il veut utiliser cette musique ne doit pas être « commercialisée ». De même, il doit être diffusé sous la même licence que la musique à savoir CC-BY-NC-SA sans quoi l’EPN ne respectera pas la licence de l’auteur du contenu musical et alors planera au dessus de lui cette épée de Damoclès appelée « action en contrefaçon ».

L’objectif, que ce soit pour l’un ou l’autre des volets, n’est pas d’aller plaider sa cause devant le juge mais bien entendu de clarifier la situation avant que le besoin du juge ne se fasse sentir. Il convient, pour un meilleur usage des licences libres, de bien identifier sa volonté quant à la diffusion et de l’exprimer clairement afin qu’elle soit respectée par les utilisateurs. Et bien entendu, en tant qu’utilisateur, il convient de respecter la volonté des auteurs des contenus utilisés.

Dès lors, une culture des communs intellectuels peut prospérer. Car ce n’est qu’un exemple et nous n’avons fait que gratter la partie émergée de l’iceberg. Des exemples il y en a d’autres et tous sont différents, c’est ce qui fait la diversité culturelle. Armés de courage et de détermination, nous pouvons tous participer à la diffusion des biens communs de la connaissance.

*Je remercie Bertrand et Patrick pour leurs précieux conseils.*

*Vladimir Ritz, doctorant en propriété intellectuelle et membre de la [CEPI](http://fablabo.net/wiki/CEPI)*
