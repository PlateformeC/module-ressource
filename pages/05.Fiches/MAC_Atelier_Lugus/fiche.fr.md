---
title: '[MAKER À COEUR] L''Atelier Lugus'
media_order: IMG_9574bis-1-870x550.jpg
type_ressource: text
feature_image: IMG_9574bis-1-870x550.jpg
license: cc-by-nc-sa
date: '01-03-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Nous avons rencontré Jérémie et David, qui fréquentent le fablab dans le cadre de leur projet de création mobilier, l’Atelier Lugus. Ils ont accepté de répondre à quelques questions.'
    image: IMG_9574bis-1-870x550.jpg
metadata:
    description: 'Nous avons rencontr&eacute; J&eacute;r&eacute;mie et David, qui fr&eacute;quentent le fablab dans le cadre de leur projet de cr&eacute;ation mobilier, l&rsquo;Atelier Lugus. Ils ont accept&eacute; de r&eacute;pondre &agrave; quelques questions.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/mac_atelier_lugus'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] L''Atelier Lugus | Espace Ressources Num&eacute;riques'
    'og:description': 'Nous avons rencontr&eacute; J&eacute;r&eacute;mie et David, qui fr&eacute;quentent le fablab dans le cadre de leur projet de cr&eacute;ation mobilier, l&rsquo;Atelier Lugus. Ils ont accept&eacute; de r&eacute;pondre &agrave; quelques questions.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_atelier_lugus/IMG_9574bis-1-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] L''Atelier Lugus | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Nous avons rencontr&eacute; J&eacute;r&eacute;mie et David, qui fr&eacute;quentent le fablab dans le cadre de leur projet de cr&eacute;ation mobilier, l&rsquo;Atelier Lugus. Ils ont accept&eacute; de r&eacute;pondre &agrave; quelques questions.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_atelier_lugus/IMG_9574bis-1-870x550.jpg'
    'article:published_time': '2017-03-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:46:23+01:00'
    'article:author': Ping
---

*Nous avons rencontré Jérémie et David, qui fréquentent le fablab dans le cadre de leur projet de création mobilier, l’Atelier Lugus. Ils ont accepté de répondre à quelques questions.*

**Bonjour ! Pouvez-vous vous présentez brièvement ?**

Jérémie : Alors, Jérémie Goriaux, j’ai 36 ans et je suis revenu sur Nantes depuis trois ans et demi et je suis charpentier, technicien cordiste et plasticien.

David : David Guyon, j’ai 30 ans, j’ai une formation d’architecte à la base et je suis à Nantes depuis deux ans environ.

**Comment s’est faite votre rencontre et la naissance de votre projet ?**

Jérémie : Alors nous nous sommes rencontrés en faisant des percussions brésiliennes, nous étions dans la même formation de batucada sur Nantes. Et puis en discutant, et bien il s’est trouvé qu’on avait besoin un peu chacun des compétences de l’autre. On a commencé à collaborer de façon informelle sur nos projets respectifs, l’un apportant à l’autre ce dont il avait besoin soit sur des savoir-faire numériques ou des savoir-faire manuels.

David : C’était il y a deux ans à peu près.

Jérémie : Voilà. Les choses se passant bien et puis étant chacun sans emploi, on s’est dit que ça pouvait coller de travailler ensemble. On partageait des objectifs de travail, une éthique…

**Est-ce que vous connaissiez déjà Plateforme C ?**

David : Moi, j’étais déjà venu faire une visite de présentation du site mais sans avoir de besoins donc je n’ai pas trouvé comment y rentrer. Je trouvais l’idée géniale mais je ne voyais pas en quoi ça me servirait à ce moment-là. Et puis on y est revenu naturellement parce que pour le coup on avait un projet concret et on savait qu’à Plateforme C on pouvait trouver les solutions techniques aux problèmes auxquels on était confrontés.

**Et en quoi est-ce Plateforme C vous a aidé ? Etait-ce par rapport  à l’espace, au matériel, aux personnes rencontrées ?**

Jérémie : Alors on avait une nécessité très concrète en termes de machine, en particulier sur la découpe laser.  Pour nous c’était un moyen de tester, de faire du prototypage.

David : On voit Plateforme C comme un espace potentiellement collaboratif. Après l’usage qu’on en a ne l’est pas parce qu’on travaille aussi à l’atelier de Jérémie (on a des machines qui sont là-bas). On vient rarement à Plateforme C et quand on vient c’est essentiellement pour un besoin technique. Après ça nous est comme même arrivé de solliciter l‘accompagnement juridique pour la protection de nos créations avec Vladimir et Thomas. On avait déjà rencontré Vladimir (doctorant en propriété intellectuelle) une première fois de manière informelle dans le centre de Nantes et on est revenu ici à une réunion collective pour se renseigner sur le type de licence sous laquelle on pouvait protéger et partager nos créations. Les valeurs de culture libre et de partage qui se retrouvent au sein du fablab nous séduisent énormément et on a découvert auprès de Vladimir et Thomas qu’on pouvait protéger ces idées tout en les diffusant.

![IMG_9602.jpg](IMG_9602.jpg)

**Et comment fonctionnez-vous aujourd’hui ?**

Jérémie : Alors nous avons intégré depuis un an une CAE (Coopérative d’Activité et d’Emploi) qui s’appelle [l‘Ouvre-Boîte 44](http://cooperer-paysdelaloire.coop/ouvre-boites/). L’objectif d’une CAE c’est d’accompagner les personnes qui ont un projet entrepreneurial mais qui ne veulent pas se lancer dans la création d’une nouvelle entité juridique. Le but étant de tester son activité de façon immédiate tout en ayant un statut, le statut visé étant le salariat. Il y a une période probatoire, dans laquelle nous sommes toujours, de test où toutes les recettes vont servir à financer le salariat.

**Sur votre site, vous mentionnez des valeurs qui marquent votre projet, pouvez-vous en parler ?**

David : Et bien, elles ne sont pas inhérentes au projet mais ce sont des choses auxquelles on est sensibles dans nos vies privées tous les deux et c’est aussi là-dessus qu’on s’est retrouvés et ça nous a paru cohérent de développer ce projet. On essaye de réduire au maximum notre impact environnemental dans nos créations. Ça devient cohérent avec cette approche de partage parce que dans un fablab on est dans une dimension d’échange. En s’approvisionnant en bois de manière locale, on réduit à la fois notre emprunte carbone et on fait travailler aussi des personnes qui travaillent à côté de chez nous. Le projet est toujours en développement, on garde ça dans un coin de nos têtes, c’est important pour nous et on essaye de tendre vers un idéal le plus sain possible. Je pense que ça nous permet aussi de justifier les prix. Il y a une prise de conscience collective actuellement de qualité. On ne se tourne plus forcément vers ce qui est le moins cher, parce qu’en général si c’est moins cher, c’est que ça n’a pas été produit en France. De voir d’où viennent les matériaux, c’est important pour les gens. Ce ne sont pas seulement nos propres considérations mais aussi ce que les gens commencent à attendre aujourd’hui dans ce qu’ils achètent.

**Et combien de temps prenez-vous pour la conception et la fabrication d’un objet ?**

Jérémie : Alors de conception, on n’a même pas compté…

David : C’est trop long !

Jérémie : Après de fabrication, je pense que sur une sculpture comme ça on est sur de l’ordre d’une semaine complète.

David : En fait l’idée c’est d’avoir un animal de base et de pouvoir le décliner. Pour l’instant il y a une déclinaison en sculpture et en maquette en modèle réduit à assembler soi-même. On est en train de travailler sur une marche intermédiaire qui sera un jouet de la taille de la sculpture mais à assembler soi-même aussi, ce qui va nous permettre à nous de ne plus avoir d’intervention manuelle ou très peu dessus et donc de réduire le coût total de prix de vente par rapport à une sculpture où on bosse une semaine dessus. Et puis ça permettra de toucher un public qui est sensible à notre univers mais qui n’a pas les moyens de se payer la sculpture, ce qui représente la plus grande partie des personnes qui voient ce qu’on fait. On a une silhouette globale qu’on va décliner, alors la silhouette de la maquette ne va pas être exactement la même que celle de la sculpture. Cette silhouette de base est adaptée à chaque fois pour chacune des échelles et il y a une nouvelle phase de travail pour dessiner spécifiquement la silhouette qui sera adaptée à la taille de l’objet. Par exemple, pour la girafe il y a trois tailles donc, même si globalement on retrouve la même silhouette, il y a trois phases de création pour chacune de ces tailles.

![IMG_9611.jpg](IMG_9611.jpg)

**Et pour finir, avez-vous une anecdote à propos de Plateforme C ?**

David : À l’expérience, il y a un truc qui me fait marrer. On se sert 90 % du temps uniquement de la découpe-laser. On commence à maîtriser le process de conception, à connaître la machine, mais à chaque fois qu’on vient ça ne se passe pas comme on a prévu ! C’est assez déstabilisant et en même temps ça permet de relativiser ce qu’on fait parce qu’à chaque fois y a un truc qui fait que c’est pas ce qu’on avait prévu. Malgré la familiarisation avec le logiciel, l’outil, y a toujours plein d’inconnues qui viennent interférer.
