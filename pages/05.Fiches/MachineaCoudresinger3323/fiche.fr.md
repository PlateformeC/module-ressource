---
title: 'Machine à coudre Singer3323'
media_order: Machiecoudre.JPG
type_ressource: text
feature_image: Machiecoudre.JPG
license: cc-by-nc-sa
date: '01-01-2019 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
        - couture
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cet outil sert à la couture.'
    image: Machiecoudre.JPG
metadata:
    description: 'Cet outil sert &agrave; la couture.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/machineacoudresinger3323'
    'og:type': website
    'og:title': 'Machine &agrave; coudre Singer3323 | Espace Ressources Num&eacute;riques'
    'og:description': 'Cet outil sert &agrave; la couture.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/machineacoudresinger3323/Machiecoudre.JPG'
    'og:image:type': image/jpeg
    'og:image:width': '800'
    'og:image:height': '600'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Machine &agrave; coudre Singer3323 | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cet outil sert &agrave; la couture.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/machineacoudresinger3323/Machiecoudre.JPG'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T15:07:56+01:00'
    'article:author': Ping
---

*Cet outil sert à la couture.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/MachineaCoudresinger3323?classes=btn)
