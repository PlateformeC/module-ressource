---
title: '[MAKER À COEUR] Julien, l''auto-entrepreneur sur longboard'
media_order: IMG_0139_light-870x550.jpg
type_ressource: text
feature_image: IMG_0139_light-870x550.jpg
license: cc-by-nc-sa
date: '01-04-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Julien, ingénieur en éco-conception, nous raconte sa passion pour le longboard, et la fabrication !'
    image: IMG_0139_light-870x550.jpg
metadata:
    description: 'Julien, ing&eacute;nieur en &eacute;co-conception, nous raconte sa passion pour le longboard, et la fabrication !'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/mac_julien_auto-entrepreneur_sur_longboard'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Julien, l''auto-entrepreneur sur longboard | Espace Ressources Num&eacute;riques'
    'og:description': 'Julien, ing&eacute;nieur en &eacute;co-conception, nous raconte sa passion pour le longboard, et la fabrication !'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_julien_auto-entrepreneur_sur_longboard/IMG_0139_light-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Julien, l''auto-entrepreneur sur longboard | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Julien, ing&eacute;nieur en &eacute;co-conception, nous raconte sa passion pour le longboard, et la fabrication !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_julien_auto-entrepreneur_sur_longboard/IMG_0139_light-870x550.jpg'
    'article:published_time': '2017-04-01T00:00:00+02:00'
    'article:modified_time': '2020-11-26T14:57:34+01:00'
    'article:author': Ping
---

**Bonjour Julien, peux-tu nous dire un peu qui tu es ?**

Moi c’est Julien Giboudeaux, je suis ingénieur en éco-conception à la base et j’ai fini mes études en septembre. J’ai 23 ans et puis je suis passionné par les sports de glisse, la nature, l’environnement : ce sont des choses qui me plaisent beaucoup.  Et j’ai décidé de monter mon projet [Naoned Longboards](https://www.facebook.com/naonedlongboards/) : ce sont des longboards made in France on peut dire, presque écolo !

**Peux-tu expliquer la démarche que tu as eu derrière ce projet ?**

Mon projet est né de ma passion pour les longboards. J’ai toujours voulu faire quelque chose de concret de mes mains. Depuis tout gamin, je travaille avec mon grand-père et mon père dans l’atelier de moto dans lequel on a aussi une machine à bois et j’ai toujours eu envie de travailler le bois. Pour moi, le fait de monter ma boîte, ça me permettait d’être un peu tout, c’est-à-dire un peu ingénieur, faire un peu de communication, être un peu vendeur et surtout faire quelque chose qui me plaît.

**Quelles sont les valeurs que tu attaches à ton projet ?**

Je suis dans une démarche visant à améliorer mon produit tout au long de mon entreprise. Là je sais qu’il y a pas mal de points que je trouve moyens sur mon projet. Par exemple j’ai du mal à trouver du bois en France qui me convienne pour les grandes boards. Pour les petites boards y a pas de soucis, je trouve du chêne labellisé. Pour les grandes boards c’est de l’érable, et j’ai un peu de mal à en trouver en France donc je prends de l’érable Canadien qui est quand même labellisé « durable ». Pour moi c’est le minimum, mais je ne suis pas satisfait. Le truc sur lequel je le suis plus, c’est la colle. Celle que j’utilise est à base d’eau, avec un autre matériau, mais la recette est secrète… Elle est agréée pour tout ce qui est matériel de cuisine et elle dégage très très peu de COV. Donc je me dis, si elle est bonne pour la nourriture, elle n’est pas mauvaise pour l’environnement ! Je suis également très attaché au Made in France, j’y fais attention dans ma vie de tous les jours en essayant d’acheter au maximum du local, même si ça coûte un peu plus cher. En plus de ça, j’aimerais travailler avec Reforestaction, qui propose aux entreprises de donner entre 1 et 5% de leur chiffre d’affaires. Pour l’instant je suis parti sur 1% parce que j’ai toujours des investissements à faire. Mais dès que je pourrai, j’essayerai de donner le maximum que je peux.

**Si tu devais résumer en quelques phrases les étapes de fabrication de tes longboards, que dirais-tu ?**

Et bien d’abord, il s’agit de donner une forme à la board, puis on va la découper, la poncer et appliquer des finitions (la décoration…). Ça reste assez basique.

**Quelles sont les machines que tu utilises le plus ?**

Pour l’instant, ce sont plutôt des machines basiques : scie-sauteuse, la ponceuse… En ce moment, je tend beaucoup à utiliser la machine laser. Pourquoi pas la CNC le jour où ça fonctionnera.

**As-tu eu quelques difficultés pendant tes réalisations ?**

Ça m’est arrivé, mais bon sur internet on trouve tout en fait. Au départ on tâtonne forcément alors on y va doucement, on essaye de faire au plus propre. Mais si on s’applique et qu’on est dans une démarche réflexive alors en général tout se passe bien.

**Et pourquoi travailler à Plateforme C ?**

Le lien avec Plateforme C c’est la possibilité de partager et monter une expérience. Ce que je trouve génial, c’est l’esprit collaboratif. Rien que l‘apéro|projets ! On peut présenter son projet et en écouter d’autres. Il y a un truc super convivial qui se passe ici. Je n’avais jamais fait partie d’un fablab, bah je suis pas déçu ! Au-delà du partage des outils, il y a un partage de l’humain qui est essentiel.
En même temps j’ai besoin d’un atelier pour pouvoir faire mes boards. Et puis, une partie des gains de la campagne Ulule va me permettre de m’abonner à Plateforme C afin que je puisse venir plus souvent. Et l’utilisation des machines, c’est super cool.

**Quels liens as-tu créé avec les autres adhérents ?**

Et bien de mon cursus d’ingénieur, j’ai aussi une licence en design et un master en éco-conception et j’ai travaillé en micro-technique. Tout ça m’a appris plein de connaissances, que ce soit en électronique, en petite et grosse mécanique, du coup si jamais quelqu’un a besoin d’un coup de main ou s’il y a un projet qui m’intéresse, moi je suis là pour aider aussi.  Et puis si quelqu’un a quelque chose à m’apporter, et bien je suis ouvert aussi ! Certaines personnes se sont intéressées à mon travail, et les interactions c’est super sympa, ça permet de se détendre pendant son travail.

**L’idée à terme, c’est donc de travailler ici ?**

Oui, je viendrai travailler ici, le plus souvent possible ou dès que j’ai des commandes. J’avais un autre travail, mais du coup j’ai démissionné ce soir ! Ça ne me plaisait pas finalement. J’étais recruteur de donateurs, donc je travaillais pour une ONG. Je veux travailler pour et sensibiliser à l’environnement mais ce n’est pas de cette façon-là que je le voyais. Là je suis en attente d’acceptation, j’envoie des papiers pour obtenir mon statut d’auto-entrepreneur, c’est en train de se faire (c’est un peu le combat des fois…).

**Et alors, ça fait quoi de devenir auto-entrepreneur à 23 ans ?**

Ah c’est une joie et une super expérience ! Je suis mon propre patron et je bosse pour moi, c’est super !
