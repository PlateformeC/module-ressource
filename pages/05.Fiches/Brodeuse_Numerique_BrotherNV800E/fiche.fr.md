---
title: 'Brodeuse Numérique BrotherNV800E'
media_order: Brother_nv800.png
type_ressource: text
feature_image: Brother_nv800.png
license: cc-by-nc-sa
date: '01-01-2018 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cette machine permet de broder des motifs sur du tissu, mais elle est commandée de manière numérique.'
    image: Brother_nv800.png
metadata:
    description: 'Cette machine permet de broder des motifs sur du tissu, mais elle est command&eacute;e de mani&egrave;re num&eacute;rique.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/brodeuse_numerique_brothernv800e'
    'og:type': website
    'og:title': 'Brodeuse Num&eacute;rique BrotherNV800E | Espace Ressources Num&eacute;riques'
    'og:description': 'Cette machine permet de broder des motifs sur du tissu, mais elle est command&eacute;e de mani&egrave;re num&eacute;rique.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/brodeuse_numerique_brothernv800e/Brother_nv800.png'
    'og:image:type': image/png
    'og:image:width': '1293'
    'og:image:height': '729'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Brodeuse Num&eacute;rique BrotherNV800E | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine permet de broder des motifs sur du tissu, mais elle est command&eacute;e de mani&egrave;re num&eacute;rique.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/brodeuse_numerique_brothernv800e/Brother_nv800.png'
    'article:published_time': '2018-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-25T16:56:35+01:00'
    'article:author': Ping
---

*Cette machine permet de broder des motifs sur du tissu, mais elle est commandée de manière numérique.*

Avec la brodeuse numérique, on peut donc broder des dessins préparés avec Inkscape, des images,... Elle possède une seule aiguille, on ne peut donc broder qu'un seul fil à la fois. Il est néanmoins possible de broder en plusieurs couleurs, en segmentant le fichier pour que la machine s'arrête et nous laisse le temps de changer de bobine.

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Brodeuse_Numérique_BrotherNV800E?classes=btn)
