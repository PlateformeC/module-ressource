---
title: 'Kit d''organisation Summerlab'
media_order: 'agenda_semaine141.png,budgetsummerlab.png,IMG_6310.jpg'
type_ressource: text
feature_image: IMG_6310.jpg
license: cc-by-nc-sa
taxonomy:
    category:
        - pratiques
    tag:
        - 'open source'
        - summerlab
        - summercamp
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Malgré l’apparente facilité de mise en œuvre de ce type d’événement, porté par l’auto-gestion de l’agenda par les participants, un summerlab n’est possible que s’il y a un acteur-organisateur pour porter le projet au long d’une indispensable phase de production.'
    image: IMG_6310.jpg
metadata:
    description: 'Malgr&eacute; l&rsquo;apparente facilit&eacute; de mise en &oelig;uvre de ce type d&rsquo;&eacute;v&eacute;nement, port&eacute; par l&rsquo;auto-gestion de l&rsquo;agenda par les participants, un summerlab n&rsquo;est possible que s&rsquo;il y a un acteur-organisateur pour porter le projet au long d&rsquo;une indispensable phase de production.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/kit_organisation_summerlab'
    'og:type': website
    'og:title': 'Kit d''organisation Summerlab | Espace Ressources Num&eacute;riques'
    'og:description': 'Malgr&eacute; l&rsquo;apparente facilit&eacute; de mise en &oelig;uvre de ce type d&rsquo;&eacute;v&eacute;nement, port&eacute; par l&rsquo;auto-gestion de l&rsquo;agenda par les participants, un summerlab n&rsquo;est possible que s&rsquo;il y a un acteur-organisateur pour porter le projet au long d&rsquo;une indispensable phase de production.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/kit_organisation_summerlab/IMG_6310.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1000'
    'og:image:height': '667'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Kit d''organisation Summerlab | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Malgr&eacute; l&rsquo;apparente facilit&eacute; de mise en &oelig;uvre de ce type d&rsquo;&eacute;v&eacute;nement, port&eacute; par l&rsquo;auto-gestion de l&rsquo;agenda par les participants, un summerlab n&rsquo;est possible que s&rsquo;il y a un acteur-organisateur pour porter le projet au long d&rsquo;une indispensable phase de production.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/kit_organisation_summerlab/IMG_6310.jpg'
    'article:published_time': '2020-11-26T14:13:42+01:00'
    'article:modified_time': '2020-11-26T14:17:53+01:00'
    'article:author': Ping
---

**Organisation : quelques conseils…**
Malgré l’apparente facilité de mise en œuvre de ce type d’événement, porté par l’auto-gestion de l’agenda par les participants, un summerlab n’est possible que s’il y a un acteur-organisateur pour porter le projet au long d’une indispensable phase de production.

Historiquement, les summerlabs ont été portés par différentes structures : ils peuvent être organisés par des personnes individuelles, des centres d’art, une ou plusieurs associations, des collectifs… Si l’on compare ces différents cas, le portage par une association rend les choses plus simples car l’organisation et l’événement sont gérés par une équipe déjà constituée et habituée à travailler ensemble, qui bénéficie d’un réseau d’acteurs investis et d’un large réseau de participants potentiels. Cela facilite également la recherche et la levée des fonds (subventions données par la Région, par exemple) et les questions d’assurances. Cependant, il existe de multiples manières d’organiser un summerlab, et chaque cas de figure correspond à une configuration particulière en termes de lieux, d’effectifs, et d’implication des participants dans l’organisation même de la rencontre.

L’organisation porteuse est aussi garante de la qualité et de l’intégrité du projet. S’il n’y a pas de but particulier à atteindre à la fin de l’événement, ces deux caractéristiques sont cruciales dans un agenda auto-géré. Dans le cas des summerlabs de Nantes organisés par PiNG, il est intéressant de noter que la notoriété de l’association donne une coloration à la rencontre : les gens qui connaissent les activités de l’association ont une confiance par rapport au contenu et à la forme de l’événement même en l’absence d’informations précises, comme c’était le cas pour la première édition.


## PRÉ-PRODUCTION

### Définition de la rencontre
Il est un temps nécessaire avant la mise en place de ce type de rencontre: celui de la définition des objectifs. Pourquoi faisons nous cette rencontre? Pour et avec qui? Quels sujets souhaitons nous aborder? De quelle manière?

Dans le cadre des summerlabs nantais, ils ont été imaginés comme des temps « de pause » permettant de mettre en perspective les sujets sur lesquels l’équipe salarié de PiNG travaille le reste de l’année. Il s’agissait de mettre en place un moment fort, un rendez-vous annuel, au sein d’un cycle de recherche-action intitulé « court-circuit/circuit court ». L’idée était d’inviter le temps d’une semaine une centaine de participants aux profils et parcours différents à faire leur ces sujets et à les « malmener ». Dans ce cas, le choix s’est fait de mettre les participants sur un même piédestal, aucun n’est plus invité ou intervenant que l’autre. Tout le monde arrive avec ses compétences, ses savoirs et l’envie de faire ensemble et d’échanger.  Ainsi, chaque participant est potentiellement intervenant et peut se sentir libre de proposer des temps de discussion, de pratique et de partage autour d’un projet, d’une idée…

### Organisation logistique
Fonction de la taille de la structure porteuse et de la jauge des participants accueillis, la phase de pré-production est plus ou moins conséquente.

Dans le cas d’une organisation annuelle d’un summerlab, la clôture d’une édition donne lieu à un bilan et une discussion sur la reconduction du projet l’année suivante. En cas de reconduction, la production s’enclenche rapidement, notamment dans la mise en place du planning annuel de la structure et du budget prévisionnel.
Le rétro planning d’organisation du summerlab de Nantes est présenté ici à titre d’exemple :
-> Juillet : bilan pour décider si renouvellement, définition de la date du prochain summerlab;
-> Décembre : envoie d’un mail aux réseaux pour bloquer la date (save the date);
-> Janvier : recherche du lieu et réservation, réservation du prestataire pour les repas;
-> Mars : travail sur le fond – sujets, nœuds;
-> Avril : lancement des inscriptions, début de la communication;
-> Avril-juin: programmation des soirées, animation du réseau, gestion des inscriptions (closes mi-juin) pour raisons logistiques (nombre de repas), réunion d’organisation de l’équipe (salariés + bénévoles);
-> Juin : travail sur les méthodes d’animations, la scénographie, animation de la communauté d’inscrits, mise en avant des participants sur le site, diffusion des supports de communication pour les moments ouverts, organisation de l’accueil des participants (couchsurfing);
-> Juillet:  derniers réglages logistiques, mise en place du lieu, dernière réunion des bénévoles avant l’événement.

Quelle que soit l’ambition de la rencontre, il semble essentiel de s’appuyer sur une équipe conséquente afin de répartir les efforts. Au-delà de cette équipe, gardons pour principe le système de démultiplication des efforts: dans un événement basé sur l’auto-gestion, il semblerait opportun et pertinent que plus les participants sont nombreux, plus les tâches sont réparties.


### Budget et financements
La mise en place de ce type de rencontre nécessite des soutiens qu’ils soient financiers, matériels, humains ou en nature. C’est l’obtention, en amont, de ces soutiens qui ouvre ou ferme un éventail d’options qui définiront les formes logistiques de la rencontre : gratuité, prise en charge totale ou partielle des repas, des soirées, qualité et volume du matériel à disposition.

L’organisation du summerlab à Nantes, dans sa forme actuelle, est possible grâce au soutien de la Région dans le cadre de conventions aux portées plus large que cet événement ainsi qu’aux partenariats développés avec des acteurs locaux tant sur l’accueil de la rencontre (ENSA Nantes) que sur la programmation des soirées (montées en collaboration avec les acteurs culturels locaux au fil des éditions).

À titre d’exemple, vous trouverez ci-après un budget moyen d’un summerlab à Nantes. La lecture de ce budget ne fait sens que s’il est analysé dans un contexte particulier à savoir…
… un événement organisé par une équipe de 8 salariés,
… accueillant une centaine de personnes sur 5 jours,
… dans un lieu mis à disposition gratuitement,
… avec une programmation artistique de la soirée de lancement rémunérée,
… des déjeuners facturés en partie aux participants, une partie étant prise en charge par les organisateurs,
… des apéros offerts aux soirées de lancement et de clôture,
… du matériel mis à disposition pour la semaine.

![budgetsummerlab](budgetsummerlab.png)

### Communication
La stratégie de communication à mettre en place dépend de la portée souhaitée de l’événement: s’agit-il d’une rencontre avec un nombre large ou restreint de participants? Est-elle ouverte ou fermée? Dans quelle mesure s’adresse t-elle à un large public? Fonction des réponses à ces questions, les moyens et supports alloués à la communication varieront.

Pour le summerlab nantais, la communication auprès des potentiels participants s’effectue à travers les réseaux de PiNG via des listings de mails, des listes de diffusion, la newsletter de l’association. Pour ce qui est de l’ouverture au grand public, la communication se fait alors plus « physique » avec la diffusion d’affiches, de flyers et un relais dans les médias locaux. Des événements publics sont également crées sur les réseaux sociaux.

### Assurance
La question de l’assurance doit se poser pour des questions de responsabilité et de sécurité. Elle dépend de bien des choses: la capacité du porteur à organiser ce type de rencontres, une demande express du lieu d’accueil si elle n’est pas inclue dans la convention d’accueil…

## PENDANT LA RENCONTRE

### Semaine type
Un summerlab peut s’envisager sur 3, 4 ou 5 jours.

Ci-dessous, pour illustration, le planning de la semaine du summerlab de Nantes du 7 au 11 juillet 2014. Cette semaine s’est organisée autour de temps forts avec :
– une semaine qui s’ouvre sur une matinée dédiée à l’accueil des participants via notamment un temps convivial (ice-breaking);
– des assemblées obligatoires chaque matin pour échanger autour de l’agenda, faire connaissance avec les nouveaux arrivants, régler des éventuels problèmes, partager ses remarques;
– des moments de labo-atelier où les participants pratiquent, échangent, discutent;
– des déjeuners en général partagés le midi;
– et des soirées conviviales et festives pour clore en beauté les journées

![agenda_semaine14](agenda_semaine141.png)

### Déroulé d’une journée
Dans le récit suivant, un participant du summerlab nantais nous raconte le déroulé d’une journée pour l’équipe organisatrice.

« Le début de la journée consiste déjà à prendre « possession » du lieu: l’équipe commence par se réunir, une heure avant l’accueil des participants, afin de se répartir les tâches de la journée.
Il s’agit alors de mettre en place la logistique technique (réseau …), matérielle (gestion du stockage/destockage des affaires des participants), moins technique (café), et humaine: accueillir les participants, vérifier les mails des participants ayant des difficultés ou impossibilités à venir. Durant le temps de convivialité qui précède l’assemblée, les participants arrivent au compte-goutte: on discute de tout, on fait connaissance, on trace les grandes lignes des activités de la journée. Du côté des organisateurs, on fait encore le point individuellement entre responsables, concernant la documentation, le matériel, puis on survole une première fois le planning des activités pour le mettre à jour et se familiariser avec.
Puis c’est le moment de l’assemblée. Il faut parvenir à diriger tout le monde vers le lieu de rassemblement, et animer/structurer ce moment de partage.
Le rôle du facilitateur est multiple: il doit encourager les participants prenant la parole à restituer un certain nombre de choses, dont un compte rendu des activités menées les jours précédents, afin que chacun puisse se tenir au courant des avancées. Ensuite, il doit faire en sorte que le programme de ce même atelier soit esquissé dans les grandes lignes, puis, éventuellement, le facilitateur devra s’atteler à la résolution de problèmes, qu’il s’agisse de manque de matériel ou d’un besoin d’une compétence spécifique qui fait défaut par exemple ou autre. Une fois les restitutions terminées de la part des participants, le facilitateur doit encore communiquer des informations relatives au déroulement général de la journée et évoquer d’éventuels problèmes relatifs au déroulement de l’événement lui-même pour les résoudre. Là encore, il ne s’agit pas de tout prendre en charge, mais de distribuer la parole aux responsables des différents pôles, afin qu’ils puissent eux-mêmes partager et résoudre les problèmes. Enfin, un temps particulier pour présenter les nouveaux arrivants et les mettre en lien avec les participants est mis en place.
Une fois l’assemblée terminée, l’activité du labo-atelier peut reprendre, jusqu’à l’heure du repas. Le facilitateur circule d’un groupe à l’autre et se met à disposition des participants pour répondre à leurs requêtes ou à leurs demandes d’information. Il profite également de ce temps de circulation pour se rapprocher des nouveaux arrivants pour mieux les aiguiller. Pendant l’assemblée, le but était d’animer et de structurer; pendant les ateliers, il s’agit de fluidifier l’activité en éliminant les obstacles et en permettant les rapprochements, les échanges: prise en charge de demandes imprévues qui rallongent la liste des tâches à accomplir, gestion des tâches en latence (l’équipe organisatrice ne gère pas seulement le programme du jour, mais également les problèmes irrésolus de la veille: acheminement de matériel, restitution d’emprunts, etc. ….).
L’heure du repas arrivant, il faut à nouveau communiquer sur ce point, encourager les participants à s’acheminer vers le pôle de restauration, après avoir vérifié lors d’une courte réunion avec l’équipe de restauration si tout se passe correctement.
La suite de la journée suit le même rythme de circulation et de fluidification, jusqu’à la fermeture du lieu, où il s’agit à nouveau de communiquer pour que les participants puissent vider les lieux, et où symétriquement, les tâches de la matinée se retrouvent: stockage des affaires, gestion du matériel, et fermeture du lieu proprement dit, chaque jour dévolue à deux personnes par roulement, bénévoles ou membres de l’équipe d’organisation. »

Ajoutons à ce récit vu d’un regard extérieur que, pendant la journée, une partie de l’équipe organisatrice s’attelle à mettre en place les soirées. Ce travail de l’ombre est conséquent et nécessite beaucoup d’implication en terme d’organisation logistique.

### Les rôles des uns et des autres
L’organisation d’un summerlab repose sur plusieurs fonctions supports, une personne pouvant en assumer plusieurs et plusieurs personnes pouvant s’en partager une. C’est selon.
– coordinateur(trice) de l’événement
– facilitateur(trice)
– référent(e) des soirées
– régisseur(se) matériel
– référent(e) accueil des participants
– référent(e) accueil grand public
– référent(e) catering et hébergement
– référent(e) budget
– référent(e) documentation
– référent(e) communication

Chaque participant peut être organisateur et référent d’une activité qu’il souhaite mener. Il l’expose aux autres, lors des assemblées. Il définit le lieu et l’heure à laquelle celle-ci aura lieu et chacun est libre de le rejoindre. Les participants peuvent donc revêtir tour à tour différents rôles:
– initiateur d’atelier
– contributeur
– observateur
– agitateur
– référent

À noter la perméabilité entre les rôles de l’équipe organisatrice et les participants. Certains organisateurs participent à des ateliers tandis qu’un participant peut devenir référent technique ou logistique spontanément.

### Organisation des espaces
La configuration de l’espace/des espaces joue un rôle clé dans un événement de ce type. En effet, l’espace doit être au service des valeurs de ces rencontres cad faciliter auto-gestion et collaboration. Il doit donc être (ré)appropriable par les participants qui doivent se sentir libres de pouvoir faire évoluer la configuration du lieu et être au service des croisements entre les pratiques (ateliers, discussions, performances….). Ceci étant, certaines de ces activités peuvent engager des pratiques différentes qui peuvent entrer en conflit (bruit vs calme, poussière…). Là encore un espace flexible permettra de remédier à ces problèmes. De même, une scénographie mêlant espaces semi clos (type tentes) et espaces ouverts permettra de créer des espaces aux usages différents dans une certaine proximité, propice aux croisements.

### Signalétique et autres affichages
Là encore, l’idée est de mettre en place des outils facilitant l’expérience du participant et au service de l’auto-gestion. Si des activités ont déjà été proposées en amont, en ligne par ex via un wiki ou lors de l’inscription, il semble néanmoins nécessaire de les partager à travers un affichage sur le lieu de la rencontre, que la réflexion virtuelle se partage dans l’espace physique. Cela peut se faire sous la forme d’un agenda partagé et contributif par ex.

Fonction de l’espace et de sa configuration, ne pas hésitez à nommer certains espaces pour fluidifier l’organisation (ex telle activité se déroule dans l’espace machin ou dans l’espace chose).

Enfin, la signalétique peut permettre de partager des informations logistiques clés, utiles à tout moment, comme les identifiants de connexion, un organigramme des membres de l’organisation, le déroulé de la semaine…

## POST PRODUCTION

### Rangement
Et voilà, tout le monde est content (et épuisé) de cette semaine. Au petit matin, les participants rentrent chez eux tout imprégnés qu’ils sont par cette dernière soirée pensant que tout est terminé. Mais non, tout n’est pas terminé pour vous cher organisateur. Il vous faut encore ranger les lieux de la rencontre. Afin de ne pas vous retrouver seul, las et fatigué, dans cette étape rébarbative mais nécessaire, nous vous conseillons d’intégrer cette activité dans le déroulé de votre semaine. Ce qui vous prendrait une matinée sera rondement mené en 1h30 par le collectif. Le mot d’ordre: collaboration jusqu’au bout!

### Clôture du budget
Cette étape peut prendre quelques semaines, la clôture du budget étant liée au règlement et à l’encaissement des factures. Elle est néanmoins nécessaire pour voir si votre budget réalisé est proche ou non de votre budget projeté. C’est aussi une bonne base sur laquelle repartir si vous souhaitez organiser à nouveau une rencontre de ce type.

### Valorisation des contenus
De nombreuses ressources sont produites dans la semaine, encouragées notamment par le référent(e) documentation et la personne en charge de la communication.

Côté documentation, ce sont parfois des contenus un peu bruts qu’il faut retravailler. D’autres fois, les participants n’ont pas eu le temps de documenter leur travail. Il s’agit dès lors de les relancer/accompagner pour qu’il le fasse à posteriori. La richesse de ces contenus doit ensuite être valorisée ici ou là fonction des outils mis en place.

Côté communication, de nombreuses photos et vidéos ont pu être prises. Idem, il s’agit de prendre le temps de les trier et de partager celles qui font le plus sens sur les outils mis en place.