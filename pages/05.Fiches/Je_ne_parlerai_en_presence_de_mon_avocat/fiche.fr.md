---
title: 'Je ne parlerais qu''en présence de mon avocat !'
media_order: AccompagnementCLibre_12mai.jpg
type_ressource: text
feature_image: AccompagnementCLibre_12mai.jpg
license: cc-by-sa
date: '23-05-2017 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'propriété intellectuelle'
        - 'culture libre'
        - opensource
        - 'c libre'
        - fablab
    author:
        - 'Vladimir Ritz'
aura:
    pagetype: website
    description: 'Retours sur trois années d’accompagnements juridiques C LiBRE : ou comment nous sommes venus à la rescousse des makers, perdus dans les méandres des licences libres !'
    image: AccompagnementCLibre_12mai.jpg
metadata:
    description: 'Retours sur trois ann&eacute;es d&rsquo;accompagnements juridiques C LiBRE : ou comment nous sommes venus &agrave; la rescousse des makers, perdus dans les m&eacute;andres des licences libres !'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/je_ne_parlerai_en_presence_de_mon_avocat'
    'og:type': website
    'og:title': 'Je ne parlerais qu''en pr&eacute;sence de mon avocat ! | Espace Ressources Num&eacute;riques'
    'og:description': 'Retours sur trois ann&eacute;es d&rsquo;accompagnements juridiques C LiBRE : ou comment nous sommes venus &agrave; la rescousse des makers, perdus dans les m&eacute;andres des licences libres !'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/je_ne_parlerai_en_presence_de_mon_avocat/AccompagnementCLibre_12mai.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '800'
    'og:image:height': '450'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Je ne parlerais qu''en pr&eacute;sence de mon avocat ! | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Retours sur trois ann&eacute;es d&rsquo;accompagnements juridiques C LiBRE : ou comment nous sommes venus &agrave; la rescousse des makers, perdus dans les m&eacute;andres des licences libres !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/je_ne_parlerai_en_presence_de_mon_avocat/AccompagnementCLibre_12mai.jpg'
    'article:published_time': '2017-05-23T00:00:00+02:00'
    'article:modified_time': '2020-11-26T14:08:14+01:00'
    'article:author': Ping
---

*Retours sur trois années d’accompagnements juridiques C LiBRE : ou comment nous sommes venus à la rescousse des makers, perdus dans les méandres des licences libres !*

À l’issue de ces trois années d’existence, le projet C LiBRE  porté par PiNG, a su trouver sa place auprès des adhérents, s’enrichir de nombreuses découvertes et s’entourer d’acteurs impliqués dans la Culture Libre. Son objectif premier était de défricher le terrain de la culture libre et de la propriété intellectuelle notamment dans le cadre des fablabs ; celui animé par PiNG, Plateforme C, étant notre terrain d’expérimentation privilégié. Cette volonté de mieux cerner les rudiments de la Propriété Intellectuelle et de la Culture Libre est sous-tendu par la démarche d’appropriation des TICs défendue par PiNG : pour comprendre les machines, les logiciels… il faut pouvoir avoir accès à leur code source, à leur recette de fabrication.

Toutefois avant de proposer des solutions clés en main aux adhérents nous avons réalisé des recherches et mis en place des temps de discussions pour faire un état des lieux autour du thème « Culture libre et fablab ». Il y a deux ans nous avons commencé à proposer des temps d’accompagnements juridiques aux adhérents pour leur faire découvrir davantage les licences libres mais aussi pour les épauler dans le développement de leur projet. Chaque projet possède ses propres caractéristiques, il faut donc bien comprendre les licences libres pour imaginer les solutions les plus adaptées. Durant ces temps de discussion collectifs nous nous demandons comment adapter la Culture Libre aux projets des adhérents et à leurs objectifs.

Ces temps sont tout aussi importants pour les adhérents que pour le réseau C LiBRE car, comme expliqué plus haut, ils sont organisés sous forme de discussion collective afin que chacun puisse partager ses expériences et ses connaissances et ainsi de s’enrichir mutuellement. Pour les aspects les plus complexes nous sommes bien évidemment accompagnés par notre « explorateur » associé, Vladimir Ritz, doctorant en propriété intellectuelle à l’Université de Nantes. Mais aussi par le Kit C Libre, kit de médiation sur les licences libres et la propriété intellectuelle.

C’est dans le cadre de ces accompagnements collectifs que Jérémy et David de l’Atelier Lugus sont venus nous voir avec l’envie de créer du mobilier et des jouets en bois au fablab. Ils envisageaient de documenter leur projet en ligne sur le wiki [Fablabo](http://www.fablabo.net/) avec les plans de conception (pour les personnes qui se sentiraient capables de les reproduire). Nous avons trouvé cela très encourageant. Leur but était de fabriquer une petite série, haut de gamme et de la commercialiser, malgré la mise en ligne du processus de conception des objets. S’est alors posé la question, comment faire pour qu’ils commercialisent leur projet tout en libérant les plans de conception ? Ne risquent-ils pas de se faire « piquer l’idée » par un tiers (rappelons que l’idée n’est pas en elle-même protégeable mais seule son individualisation particulière au créateur l’est) ? Après échange nous avons opté pour la licence CC-BY-NC-SA. C’est-à-dire ? Cette licence permet à l’auteur d’autoriser l’exploitation des plans ou dessins originaux par un tiers mais à des fins non commerciales uniquement ! Elle autorise également la création de plans ou dessins dérivés (donc modifiés) à condition qu’ils soient redistribués sous une licence identique (cette licence est dite virale). Une solution qui semblait correspondre tout à fait aux objectifs de Jérémy et David. Par la suite, l’Atelier Lugus a pu réaliser un partenariat avec une entreprise capable de produire leur mobilier et ce malgré la présence de la licence Creative Commons : un bel exemple qui montre que libre ne veut pas dire gratuit ! Cela ne veut pas non plus dire abandonner ses droits.

En novembre dernier, Clément est venu avec l’envie de fabriquer de manière collaborative un graveur de disque vinyle open-source, qu’il souhaite également vendre en petite série. Nous aurions pu lui proposer d’utiliser la même licence que l’Atelier Lugus mais son projet nous amène à nous poser plusieurs questions intéressantes dont la première est lancée par lui-même : « Je fabrique une machine mais la technologie existe déjà depuis les années 60 donc j’imagine qu’elle doit être brevetée ? » En effet, mais ce brevet n’est valable que 20 ans et ensuite il s’élève dans le domaine public et si l’on souhaite poser un nouveau brevet il faut pouvoir apporter une modification substantielle à l’invention. En faire une nouvelle à partir de l’ancienne en quelque sorte. Toutefois il ne faut pas oublier d’aller vérifier sur le site de l’INPI si le brevet existe avant de commencer un projet. Mieux vaut s’en rendre compte avant d’avoir trop avancé dans son projet… Cette problématique des brevets et de la Culture Libre a été saisie par C LiBRE et exposée lors de plusieurs articles accessibles sur la page C LiBRE.

Désormais, et pour notre plus grand plaisir, certains participants reviennent à des accompagnements pour approfondir leur connaissance de la Culture Libre ou poser des questions différentes en fonction de l’évolution de leur projet. C’est notamment le cas de Dorothée Bégué, artiste plasticienne. Dorothée est venue nous voir car elle avait de nombreuses questions autour des droits d’auteur. Puis elle a découvert la logique des licences libres et y a adhéré pour plusieurs raisons : tout d’abord pour la dimension éthique du partage, mais aussi pour l’aspect pédagogique qui lui permet d’expliquer ce qu’induit clairement le droit d’auteur et enfin pour véhiculer son nom sur ses œuvres grâce à la clause BY des licences Creative Commons. La deuxième fois elle est venue pour mieux appréhender la question d’œuvre collective (œuvre faite par plusieurs auteurs) et pour comprendre le rôle de chaque acteur dans une création commune ! Cette découverte des licences libres a changé sa façon d’envisager un travail de création. Depuis elle diffuse le plus possible à son entourage les informations que nous lui avons transmises. Le partage et la médiation sont les leviers d’action de C Libre, nous sommes donc toujours ravis d’apprendre que cela devient contagieux chez nos adhérents.

Actuellement, les licences libres sont adaptées aux créations qui sont soumises au droit de la propriété intellectuelle et artistique (photographies, dessins, sculptures, plans, logiciels, etc) et non aux inventions techniques et à la propriété Industrielle. Certaines initiatives émergent telles que l’Open Source Hardware Certification qui est apparu aux États-Unis, mais il reste encore des sentiers à explorer pour la propriété industrielle et même si des pistes ont aussi été proposées ou mises en valeur au sein du réseau C LiBRE, nous continuons d’explorer, de rencontrer, d’échanger pour tenter de répondre au mieux aux adhérents qui cherchent un nouvel équilibre entre partage et protection !
