---
title: 'Laser rouge'
media_order: 1ere_decoupe.jpg
type_ressource: text
license: none
date: '01-01-2019 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
        - 'découpe laser'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cette machine sert à la découpe laser.'
    image: 1ere_decoupe.jpg
metadata:
    description: 'Cette machine sert &agrave; la d&eacute;coupe laser.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/laser_rouge'
    'og:type': website
    'og:title': 'Laser rouge | Espace Ressources Num&eacute;riques'
    'og:description': 'Cette machine sert &agrave; la d&eacute;coupe laser.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/laser_rouge/1ere_decoupe.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '800'
    'og:image:height': '450'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Laser rouge | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine sert &agrave; la d&eacute;coupe laser.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/laser_rouge/1ere_decoupe.jpg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:25:24+01:00'
    'article:author': Ping
---

*Cette machine sert à la découpe laser.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Laser_rouge?classes=btn)
