---
title: 'Tous Terriens'
media_order: 614930339.jpg
type_ressource: video
feature_image: 614930339.jpg
license: cc-by-nc-sa
date: '01-10-2016 00:00'
taxonomy:
    category:
        - recits
    tag:
        - summerlab
    author:
        - 'Vincent Pouplard'
aura:
    pagetype: website
    description: 'Le réalisateur Vincent Pouplard présente Tous Terriens.'
    image: 614930339.jpg
metadata:
    description: 'Le r&eacute;alisateur Vincent Pouplard pr&eacute;sente Tous Terriens.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/vincent_pouplard_tous_terriens'
    'og:type': website
    'og:title': 'Tous Terriens | Espace Ressources Num&eacute;riques'
    'og:description': 'Le r&eacute;alisateur Vincent Pouplard pr&eacute;sente Tous Terriens.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/vincent_pouplard_tous_terriens/614930339.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1280'
    'og:image:height': '720'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Tous Terriens | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Le r&eacute;alisateur Vincent Pouplard pr&eacute;sente Tous Terriens.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/vincent_pouplard_tous_terriens/614930339.jpg'
    'article:published_time': '2016-10-01T00:00:00+02:00'
    'article:modified_time': '2020-11-26T16:54:58+01:00'
    'article:author': Ping
---

*Vincent Pouplard est réalisateur. Son travail documentaire s’articule autour de récits de vie et d’expérience. Il a récemment signé PAS COMME DES LOUPS dont la sortie en salles est prévue pour le mois d’avril 2017.*

<iframe src="https://player.vimeo.com/video/186186458" width="100%" height="600" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
