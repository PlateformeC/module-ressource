---
title: 'Les doigts dans la prise #5 : la captologie'
type_ressource: audio
feature_image: DSCF3652.jpg
license: cc-by-nc-sa
date: '17-06-2019 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Mona Jamois'
aura:
    pagetype: article
    description: 'Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne Le Fil de l’histoire.'
    image: DSCF3652.jpg
metadata:
    description: 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/sun05_mai19_captologie'
    'og:type': article
    'og:title': 'Les doigts dans la prise #5 : la captologie | Espace Ressources Num&eacute;riques'
    'og:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/sun05_mai19_captologie/DSCF3652.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1000'
    'og:image:height': '699'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #5 : la captologie | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/sun05_mai19_captologie/DSCF3652.jpg'
    'article:published_time': '2019-06-17T16:31:00+02:00'
    'article:modified_time': '2020-12-09T13:44:28+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Pour écouter le podcast c’est par [ici](https://www.lesonunique.com/content/les-doigts-dans-la-prise-attends-jai-une-notif) !

![Les doigts dans la prise - la captologie](5%20-%20Les%20doigts%20dans%20la%20prise%20-%20Attends%20jai%20une%20notif.mp3)

Bonjour à toutes et à tous et bienvenue pour cette nouvelle chronique “Les doigts dans la prise” de l’association PiNG ! Le thème de la semaine nous branche sur les réseaux et, pour l’occasion, je vous propose de nous pencher sur une question qui me turlupine un peu : **les réseaux sociaux sont-ils en train de nous transformer en zombies ?**

Liste non-exhaustive des symptômes :

Vous regardez votre téléphone compulsivement 72 fois par jour,
Vous vous retrouvez à scroller sans raison votre fil Instagram à minuit passé,
Vous ouvrez Whatsapp ou votre boite mail juste au cas où alors que vous êtes en train de dîner avec des amis…

Et bien tout à cela est malheureusement totalement normal, ou plus exactement logique : c’est le résultat du travail des designers qui ont conçu ces plateformes dans le but de vous faire faire tout ça, très précisément.

Comment en sommes-nous arrivés là ?

Chaque jour nous sommes bombardés d’une **quantité considérable d’informations** : vidéos, posts, tweets, mails, actualités. Du coup, c’est la loi de l’offre et de la demande : les journées ne comptant que 24h notre temps d’attention devient quelque chose de rare. Tous les services proposant des contenus sur internet se livrent donc une lutte sans merci afin que nous passions notre temps chez eux et pas sur le site ou l’application d’à côté.

En 2017, Sean Parker, ancien président de Facebook, avouait que l’objectif initial du réseau social avait été le suivant : “Comment pouvons-nous consommer un maximum de votre temps et de votre attention ?”.

Au moins, les choses sont claires.

Cette guerre pour notre temps porte un nom que vous avez peut-être déjà entendu : **l’économie de l’attention.**

Et capter notre attention est une science très sérieuse : **la captologie** est enseignée dans certaines écoles comme au Persuasive Tech Lab de l’université de Stanford. Cet endroit charmant forme les designers aux neurosciences dans le but d’exploiter nos biais cognitifs avec la technologie. Autrement dit, **utiliser nos faiblesses psychologiques pour nous influencer dans l’utilisation des plateformes qu’ils développent.**

Prenons l’exemple de nos vérifications compulsives de téléphone.

Il s’avère que pour nos cerveaux, **plus un taux de récompense est variable, plus l’action devient addictive.** Pour nous rendre bien accros, les designers de Facebook, Instagram et consorts lient donc nos clics à des gratifications aléatoires : comme des utilisateurs fous de machines à sous, nous jouons alors en permanence pour savoir si nous avons gagné quelque chose (une notification, un like, un message etc).

De la même manière, nos cerveaux sont conçus pour savoir quand s’arrêter (de manger par exemple) grâce à des indices visuels. Si ces derniers sont occultés, nous ne savons plus quand nous arrêter et nous ne sommes jamais rassasiés. Voilà comment vous pouvez vous retrouvez à scroller sans fin votre mur Facebook alors que vous avez autre chose à faire : vous êtes immergés dans une continuité narrative qui a été fabriqué exactement dans ce but.

Ajoutez à cela **le fameux FOMO (notre peur de rater quelque chose d’important)**, notre besoin d’approbation sociale (chaque like stimulant notre circuit de récompense), et des stimulis d’alerte incessant qui activent les zones liées au stress dans notre cerveau et vous commencez à avoir une idée du séduisant cocktail dans lequel nous sommes plongés.

Toutes ces sympathiques astuces des designers ne sont pas sans conséquences sur notre comportement. En dehors de nous faire perdre du temps, les sollicitations permanentes de nos téléphones finissent à la longue par **altérer notre concentration**. N’activant que les zones de la réflexion rapide dans nos cerveaux, **elles nous habituent à toujours plus de rapidité et érodent peu à peu nos capacités d’attention ciblée et d’analyse critique : aie, le zombie hagard n’est plus si loin.**

Alors on est tous foutus ? Pas complètement, ou peut-être pas encore.

Prendre conscience des ficelles qui se cachent derrière les réseaux sociaux a le mérite de nous rappeler que la technologie n’est pas seulement un outil, mais un acteur social à part entière qui influe sur les rythmes de nos vies, nos relations et certains de nos choix.

La solution pour concevoir des plateformes plus éthiques réside alors indéniablement du côté des logiciels libres pour permettre à toutes et tous de se réapproprier nos outils numériques.

En attendant, vous pouvez prendre votre courage à deux mains, affronter votre FOMO et désactiver le maximum de notifications possibles.

Allez, si vous le faites, je le fais !
