---
title: 'Un label pour le matériel libre ?'
media_order: IMG_0049.jpg
type_ressource: text
feature_image: IMG_0049.jpg
license: cc-by-sa
date: '17-11-2016 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'propriété intellectuelle'
        - 'culture libre'
        - opensource
        - 'c libre'
    author:
        - 'Vladimir Ritz'
aura:
    pagetype: website
    description: 'Vladimir Ritz, doctorant en propriété intellectuelle et explorateur associé de PiNG impliqué dans le groupe de réflexion C LiBRE, nous apporte un éclairage sur les avancées juridiques en matière de matériel libre.'
    image: IMG_0049.jpg
metadata:
    description: 'Vladimir Ritz, doctorant en propri&eacute;t&eacute; intellectuelle et explorateur associ&eacute; de PiNG impliqu&eacute; dans le groupe de r&eacute;flexion C LiBRE, nous apporte un &eacute;clairage sur les avanc&eacute;es juridiques en mati&egrave;re de mat&eacute;riel libre.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/un_label_pour_le_materiel_libre'
    'og:type': website
    'og:title': 'Un label pour le mat&eacute;riel libre ? | Espace Ressources Num&eacute;riques'
    'og:description': 'Vladimir Ritz, doctorant en propri&eacute;t&eacute; intellectuelle et explorateur associ&eacute; de PiNG impliqu&eacute; dans le groupe de r&eacute;flexion C LiBRE, nous apporte un &eacute;clairage sur les avanc&eacute;es juridiques en mati&egrave;re de mat&eacute;riel libre.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/un_label_pour_le_materiel_libre/IMG_0049.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '800'
    'og:image:height': '533'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Un label pour le mat&eacute;riel libre ? | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Vladimir Ritz, doctorant en propri&eacute;t&eacute; intellectuelle et explorateur associ&eacute; de PiNG impliqu&eacute; dans le groupe de r&eacute;flexion C LiBRE, nous apporte un &eacute;clairage sur les avanc&eacute;es juridiques en mati&egrave;re de mat&eacute;riel libre.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/un_label_pour_le_materiel_libre/IMG_0049.jpg'
    'article:published_time': '2016-11-17T00:00:00+01:00'
    'article:modified_time': '2020-11-26T16:48:04+01:00'
    'article:author': Ping
---

*Vladimir Ritz, doctorant en propriété intellectuelle et explorateur associé de PiNG impliqué dans le groupe de réflexion C LiBRE, nous apporte un éclairage sur les avancées juridiques en matière de matériel libre.*

Le métronome qui oscille entre le matériel et l’immatériel ne s’arrêtera donc jamais. Il y a un mois, le 7 octobre 2016, se déroulait l’Open Hardware Summit 20161 à Portland dans l’Oregon (USA), c'est lors de cet événement qu'a été annoncé le lancement de l’Open Source Hardware Certification (OSHW). Ça semble être peu de chose mais il se pourrait bien que ce soit une nouvelle pierre non négligeable à l’édifice de la Culture Libre.

Avant d’entrer dans le vif du sujet, prenons quelques instants pour savoir comment nous en sommes arrivés là. D’abord, M. Stallman libère les logiciels, puis M. Lessig libère les œuvres culturelles. Dans la foulée, pléthore de personnalités plus ou moins connues ont œuvré pour la création de nouvelles licences libres ou l’amélioration des licences existantes (des exemples parmi tant d’autres : ArtLibre, CeCill, Apache V 2 .0). Toutes ces licences portent sur des choses immatérielles : des photographies, des logiciels, des œuvres d’art, des vidéos, des musiques… Alors bien sûr qu’il est possible de toucher une photographie ou un roman ou encore une sculpture, Il est question ici du droit d’auteur qui porte sur la photographie, le roman, le film et non de l'objet lui-même... Il faut en quelque sorte oublier l’objet au profit du droit d’auteur. Le droit d’auteur est un droit de propriété qui s’exerce, non pas sur l’objet physique mais sur « l’œuvre » qui a été matérialisé dans l’objet. Il s’agira alors de la musique gravée sur un CD, l’image peinte sur un tableau, l’histoire écrite dans un livre. À vrai dire, cela se conçoit assez bien : lorsqu’on place son œuvre sous une licence libre, on ne « donne » pas l’objet mais bien la possibilité de reproduire, de modifier, de faire commerce … de l’œuvre.
Pourtant il existe des choses pour lesquelles ce degré d’abstraction est moins évident : il s’agit des inventions. C’est en quelque sorte « plus matériel ». Juridiquement il n’en est rien. Qu’il s’agisse d’une invention ou d’un tableau de maître, l’objet matériel n’aura que peu d’importance face au droit de propriété intellectuelle. Mais en pratique, lorsqu’il s’agit d’une invention, les bricoleurs parlent souvent de matériel.

Ainsi, lorsqu’ils ont la volonté de partager leurs travaux, par mimétisme avec les licences libres, ils parlent de matériel libre. Pourtant, ce qu’ils libèrent n’est pas l’objet physique mais bien l’invention (la solution technique qu’ils ont trouvé au problème). Se pose alors la question fatidique : « quelle licence choisir ? ». C’est là que le bât blesse : il n’existe à l’heure actuelle aucune licence qui puisse présenter des garanties suffisantes à l’acte de partage d'inventions. Il faut donc emprunter des chemins détournés et c’est là qu’intervient l’[OSHWA](https://2016.oshwa.org/) (Open Source HardWare Association).

L’idée de ce collectif de joyeux partageurs a été de créer un label pour le « matériel libre ». C’est en quelque sorte l’équivalent d’un label « bio » mais  pour le matériel informatique et électronique, en attendant que ce label ou un autre fasse le lien avec tout type d'objet. Qu’est-ce que c’est ? Comment ça marche ? Est-ce que c’est valable en France ? Ça coûte cher ?
L’initiative est encore jeune, il plane donc encore quelques inconnues mais essayons de voir ce qu’il en est pour le moment.

## Qu’est-ce que c’est ?

Comme son nom l’indique, l’Open Source Hardware Certification (OSHW) est gérée par l’OSHWA. C’est une sorte de Label qu’ils ont décidé de tester au niveau mondial. Le concept est que si une certification est obtenue, il est alors possible d’apposer sur l’objet un logo.

Ce logo renseigne le pays d’origine suivi du numéro d’enregistrement. En pratique, cela sert dès lors qu’une personne veut reproduire cet objet. Il cherche alors [sur le site](certificate.oshwa.org/certification-directory/) et peut trouver tous les éléments nécessaires à la reproduction de l’objet. Simple, rapide et efficace. Ainsi, il est possible de trouver toutes sortes d’objets : un [casque audio que l’on peut imprimer en 3D](https://www.thingiverse.com/thing:674179), une [LED programmable pour avoir des notifications d’un CPU](https://blink1.thingm.com/about/), un [détecteur de moisissure pour plantes](https://www.sparkfun.com/products/13322), un [violon électrique DIY](https://openfabpdx.com/fffiddle/) et bien d’autres. Des français se sont prêtés au jeu avec la [smoothieboard](http://smoothieware.org/) qui est d’ailleurs la carte électronique utilisée dans la découpe laser de Plateforme C.

## Comment ça marche ?

C’est en réalité assez simple et c’est là tout l’avantage de ce système. Il « suffit » de s’enregistrer auprès de la OSHWA [ici](http://certificate.oshwa.org/get-certified/) et de compléter le formulaire. C’est apparemment gratuit et la certification est annuelle, renouvelable autant de fois que nécessaire.
Il y a 4 conditions :
- Il faut adhérer à la définition de l’open hardware de la OSHWA. C’est-à-dire :
« Open source hardware regroupe « les conceptions “Hardware” réalisées publiquement et disponibles de manière à ce que n’importe qui puisse étudier, modifier, distribuer, créer et vendre un “design” ou un produit basé sur ce design. La source du produit hardware, le design duquel le produit est issu, est disponible sous un format choisi pour permettre de faire des modifications. Idéalement, open source hardware utilisera des composants et matériaux facilement approvisionnables, des procédés de fabrication standard, des infrastructures libres, des contenus libres de droit et des outils de design “Open-source” pour maximiser la possibilité donnée à d’autres de concevoir ou utiliser un produit hardware. Open source hardware permet à quiconque d’avoir le contrôle sur leur technologie du moment qu’elles partagent leur savoir et encourage le commerce au travers de l’échange de design libre. »
- Le déposant doit assurer que toutes les parties qu’il a créé sont accessibles en Open source.
- Le déposant doit assurer que toutes les parties qu’il a utilisé sont accessibles en Open source. Il doit faire de son mieux pour trouver s’il y a des parties « propriétaires ». Il doit n’utiliser que des parties Open source.
- Il doit remplir le formulaire et le renouveler s’il le souhaite tous les ans.
- Enregistrer chaque nouveau matériel libre.

## Quelles sont les garanties ?

L’OSHWA garantit qu’il n’y aura pas d’usage de ce label par une personne qui n’en respecterait pas les codes. C’est assez peu mais c’est un début.

## Et enfin, la question fatidique, est-ce valable en France ?

C’est sur ce point précis qu’il existe encore quelques inconnues. Pour autant, il est possible de livrer un début de raisonnement. Il faut comprendre ces certifications comme des preuves du fait que le « produit » enregistré est dans une sorte de domaine public étendu. En effet, le « produit » sera librement utilisable par quiconque, pour n’importe quel usage, à n’importe quel moment et en n’importe quel lieu. De plus, le déposant livre avec le produit la documentation nécessaire à sa reproduction. Ainsi, il n’est plus possible d’empêcher quoi que ce soit. Autrement dit c’est un acte de pur partage.

En France, il est possible de faire ce genre de chose. Sans entrer dans les détails juridiques, il est possible si on fait une **invention nouvelle** de la placer dans le domaine public en la rendant accessible au public sans avoir déposé de brevet au préalable. Normalement, cela bloque n’importe quelle demande de brevet ultérieure sur la même invention. Il y a principalement deux inconvénients. Le premier est qu’il n’est plus alors question pour quiconque d’avoir un monopole économique sur l’invention. Le second est qu’il est possible que l’invention soit brevetée par quelqu’un d’autre. Dans ce cas il faut contester ce brevet en justice. Le brevet aura toutes les chances d’être annulé mais cela implique de passer devant le juge. Il peut exister des solutions à ce problème et cela fera l’objet d’un futur article.

Ainsi, s’il est possible de placer une invention nouvelle dans le domaine public en France, il est également possible de le faire par une certification qui se veut de renommée mondiale. D’ailleurs, cette certification va plus loin qu’un simple domaine public puisqu’elle assure qu’il sera toujours possible de trouver associée à l’invention la documentation nécessaire à sa production.

Il n’est pas question ici de faire preuve de démagogie et cet article n’est d’ailleurs qu’une courte introduction à la « labellisation » de matériel libre. Il y a encore à ce sujet beaucoup d’interrogations que nous allons devoir résoudre. Elles ne nous semblent pas insurmontables. Au début des licences Creative Commons dont on connaît le rayonnement aujourd’hui régnait également beaucoup d’inconnues.

Il n’était pas certain que les contrats soient valables, alors que c'est bel et bien le cas aujourd’hui. Il n’était pas certain que ces licences soient utilisées. Il y a aujourd’hui plus d’un milliard de projets recensés qui utilisent ces licences et bien d’autres encore non recensés. Le parallèle avec les Creative Commons n’est pas anodin. Il y a en effet pour l’OSHW cette même volonté de simplicité et d’efficacité. L’OHSW connaîtra-t-elle le même destin ? L’avenir le dira.

Notons simplement qu’il est aujourd’hui possible d’apporter sa pierre à l’édifice de la Culture Libre dans le domaine du matériel libre. Emparons-nous en !

Vladimir Ritz, explorateur associé de PiNG, doctorant en propriété intellectuelle et associé au groupe de réflexion C LiBRE.