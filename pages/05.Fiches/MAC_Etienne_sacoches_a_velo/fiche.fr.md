---
title: '[MAKER À COEUR] Etienne, sacoches vélo et vie à l''asso !'
media_order: etienne.png
type_ressource: video
feature_image: etienne.png
license: cc-by-nc-sa
date: '01-02-2019 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Nous vous présentons Étienne : il nous dévoile la sacoche de vélo étanche qu''il vient de terminer à Plateforme C et raconte la dynamique associative et l''entraide au fablab !'
    image: etienne.png
metadata:
    description: 'Nous vous pr&eacute;sentons &Eacute;tienne : il nous d&eacute;voile la sacoche de v&eacute;lo &eacute;tanche qu''il vient de terminer &agrave; Plateforme C et raconte la dynamique associative et l''entraide au fablab !'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/mac_etienne_sacoches_a_velo'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Etienne, sacoches v&eacute;lo et vie &agrave; l''asso ! | Espace Ressources Num&eacute;riques'
    'og:description': 'Nous vous pr&eacute;sentons &Eacute;tienne : il nous d&eacute;voile la sacoche de v&eacute;lo &eacute;tanche qu''il vient de terminer &agrave; Plateforme C et raconte la dynamique associative et l''entraide au fablab !'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_etienne_sacoches_a_velo/etienne.png'
    'og:image:type': image/png
    'og:image:width': '956'
    'og:image:height': '470'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Etienne, sacoches v&eacute;lo et vie &agrave; l''asso ! | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Nous vous pr&eacute;sentons &Eacute;tienne : il nous d&eacute;voile la sacoche de v&eacute;lo &eacute;tanche qu''il vient de terminer &agrave; Plateforme C et raconte la dynamique associative et l''entraide au fablab !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_etienne_sacoches_a_velo/etienne.png'
    'article:published_time': '2019-02-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:52:28+01:00'
    'article:author': Ping
---

*Nous vous présentons Étienne : il nous dévoile la sacoche de vélo étanche qu'il vient de terminer à Plateforme C et raconte la dynamique associative et l'entraide au fablab !*

<iframe width="100%" height="450px" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/e6309207-9bee-471a-aa43-ad4ccb2664b7" frameborder="0" allowfullscreen></iframe>
