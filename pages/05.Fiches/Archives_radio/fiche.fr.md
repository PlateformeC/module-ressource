---
title: 'Archives Radio Contre Temps 17/06/20'
media_order: preciado.jpg
type_ressource: audio
feature_image: preciado.jpg
license: cc-by-nc-sa
date: '01-08-2020 00:00'
taxonomy:
    category:
        - pratiques
        - recherches
        - récits
    tag:
        - radio
    author:
        - 'Meven Marchand Guidevay'
aura:
    pagetype: website
    description: 'Le 17 juin 2020, PiNG animait une radio éphémère d''une journée autour du thème «Faire autrement». Vous pouvez retrouver ci-dessous les archives de cette journée d''échanges riches et passionnants.'
metadata:
    description: 'Le 17 juin 2020, PiNG animait une radio &eacute;ph&eacute;m&egrave;re d''une journ&eacute;e autour du th&egrave;me &laquo;Faire autrement&raquo;. Vous pouvez retrouver ci-dessous les archives de cette journ&eacute;e d''&eacute;changes riches et passionnants.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/archives_radio'
    'og:type': website
    'og:title': 'Archives Radio Contre Temps 17/06/20 | Espace Ressources Num&eacute;riques'
    'og:description': 'Le 17 juin 2020, PiNG animait une radio &eacute;ph&eacute;m&egrave;re d''une journ&eacute;e autour du th&egrave;me &laquo;Faire autrement&raquo;. Vous pouvez retrouver ci-dessous les archives de cette journ&eacute;e d''&eacute;changes riches et passionnants.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/archives_radio/preciado.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1080'
    'og:image:height': '1080'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Archives Radio Contre Temps 17/06/20 | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Le 17 juin 2020, PiNG animait une radio &eacute;ph&eacute;m&egrave;re d''une journ&eacute;e autour du th&egrave;me &laquo;Faire autrement&raquo;. Vous pouvez retrouver ci-dessous les archives de cette journ&eacute;e d''&eacute;changes riches et passionnants.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/archives_radio/preciado.jpg'
    'article:published_time': '2020-08-01T00:00:00+02:00'
    'article:modified_time': '2020-11-25T13:50:22+01:00'
    'article:author': Ping
---

*Le 17 juin 2020, PiNG animait une radio éphémère d'une journée autour du thème «Faire autrement». Interviews, lectures, flash-info, compilations de podcasts, interventions artistiques: accompagnés de nos invité·es, nous poursuivions les réflexions entamées dans le numéro de notre newsletter ressource [Faire Autrement](http://thy1.mjt.lu/nl2/thy1/mhjij.html?hl=fr ).Vous pouvez retrouver ci-dessous les archives de cette journée d'échanges riches et passionnants.*

## Programme de la journée

### 08h - 09h

* Rediffusion OUïDire [Goto80, Dear Data](https://www.ouiedire.net/emission/ailleurs-59)
* [LIVE] À 8h30 Réveil du corps décalé par Mona **5min**
* [LIVE] À 8h50 Edito : faire ensemble / contretemps par Charlotte **10  min**

### 09h - 10h30 | DÉ-CONSTRUIRE

* [PODCAST] Présentation de [Radio Informal du collectif Rybn](https://p-node.org/actions/antivirus) avec rediffusion de l'interview de [Françoise Vergès]
* [ENREGISTREMENTS PiNG] Lecture de l'article [«Nous étions sur le point de faire la révolution féministe… et puis le virus est arrivé»](https://bulb.liberation.fr/edition/numero-2/nous-etions-sur-le-point-de-faire-la-revolution-feministe/) par Maëlle
![Paul B. Preciado](01-02-preciado.mp3)

### 10h30 - 12h | NATURE

* [ENREGISTREMENTS PiNG] Cartes postales sonores "Souffle Terre" [d'Antoine Bellanger](https://antoinebellanger.hotglue.me/) **30 min**
* ~~Dominique A "Rendez-nous la lumière" **3 min**~~
* [PODCAST] Rediffusion de l'émission Cultures monde "[Humains/Nature, vers la réconciliation?](https://www.franceculture.fr/emissions/cultures-monde/culturesmonde-emission-du-jeudi-04-juin-2020)" **58min**

### 12h00 - 13h00 | CONFINEMENT CRITIQUE
* [ENREGISTREMENTS PiNG] ***"Du confinement à stop covid, de la biopolitique aux objets techniques"*** Entretien avec [Alain Giffard](https://alaingiffardblog.wordpress.com/) **1h**

### 13h00 - 14h00 | REPAS DIY

* [LIVE] À 13h00 le JT | Charlotte et Mona + chronique Tom `Mona`
* ~~Antenne libre : questions ouvertes~~
* [ENREGISTREMENTS PiNG] Open playlist : [machine sonore infernale du Florida](https://github.com/le-studio-5/MachineInfernale), répondeur, appel en direct, ...  **?**

### 14h00 - 15h30 | TRANSMISSION PAR RADIO-S

* [ENREGISTREMENTS PiNG] 3 rencontres avec des initiatives de radio (web et FM) par Julien :
    - L'[Acentrale Nantes](http://www.utopiesonore.com/lacentrale-nantaise/), *Un collectif de radio d'info directe, retour sur les plateaux directs* **30min**
    - Pascal Massiot de [JetFM](http://jetfm.fr/site/) *Retour sur la radio locale FM dans la période du confinement* **15min**
    - Guillaume Mézières pour la [Radio des confins](https://radio-des-confins.online/), *À Douarnenez, une radio locale éphémère et mobile* **15min**
* [ENREGISTREMENTS PiNG] Lecture de ["Vers une radio polymorphe" de Testuo Kogawa](http://syntone.fr/wp-content/uploads/Kogawa_RadioPolymorphe.pdf) | par Meven **25min**
* [ENREGISTREMENTS PiNG] Lecture Automatique ["Décoloniser les médias et les technologies numériques"](https://www.ritimo.org/Decoloniser-les-medias-et-les-technologies-numeriques) lu par Pocket - **9min**

### 15h30 - 18h30 | CHRONOTOPIES: SE RÉAPPROPRIER LE TEMPS ET L'ESPACE

* [PODCAST] Rediffusion de l'émission Le temps d'un débat [Vivre avec le virus: ralentir ou accélerer? ](https://www.franceculture.fr/emissions/le-temps-du-debat/le-temps-du-debat-emission-du-mercredi-20-mai-2020), **42min**
* [ENREGISTREMENTS PiNG] [“Tout quitter”](https://editions.flammarion.com/Catalogue/hors-collection/litterature-francaise/tout-quitter) d’Anaïs Vanel - Lecture d'extraits par Sophie **15 min**
* [LIVE] À 17h Flash info de terrain : Voyage indigène de détourisme avec [À la criée](https://www.alacriee.org/):  [contre la réintoxication du monde](https://17juin.noblogs.org/) ! **45min** 
* [LIVE] Débat ouvert sur les [friches culturelles](pad.numerique-en-commun.fr/le-temps-des-friches), par Charlotte et Julien **45min**
* [PODCAST] Rediffusion JetFM : [Plongée dans l’ilot Lamour les Forges à Rezé, un laboratoire urbain tiraillé entre densification et préservation du patrimoine.](http://jetfm.fr/site/Podcast-La-quotidienne-du-10-juin.html) **1h**


### 18h30-19h30 | DÉ-CONSTRUIRE, SUITE

* [LIVE] Comment grandir en étant un homme ? Comment s'extraire des masculinités dîtes "toxiques" pour soi et pour les autres ? Discussion avec Milan, créateur du podcast Phallodécentré, et diffusion de l'épisode "[Les privilèges](https://www.youtube.com/watch?v=E7ST2atsTt4)" **40 min**
* [PODCAST] Diffusion du podcast de Marc Jahjah, enseignant-chercheur à l'Université de Nantes en Sciences de l'Information et de la Communication "Conversation avec... un article" ["Des dessins et des enfants : la mémoire vivante de la colonisation"](https://www.youtube.com/watch?v=6heDGlscjl8) **20 min**
* [LIVE] Interview Rachel 

### 19h30 - 19h50 | La PAUSA del SOLSTICIO
* [LIVE] Depuis Quito : *"petit rituel de solsticio d'été dans le sens d'une dé-ré-construction de nos rélaciones avec le cosmos, la nature - savoirs ancestrals, phytocentrisme, devenir común"* par [Pedro Soler](http://word.root.ps/)  + guests

### 20h - 21h | CHERCHER ENSEMBLE
* ***Biohacking et espaces de vie social à défendre*** Interview de [Xavier Coadic](https://xavcc.frama.io/about/) par Victoria **1h** 

### 21h - 22h30 | DÉLIER LES LANGUES
* ~~Rediffusion de l'atelier d'écriture "Allô j’écris – Radi’haut en couleurs" d'[Amélie Charcosset](http://www.ameliecharcosset.com/bienvenue/) **1h**~~
* [PODCAST] Carte blanche à l'artiste [Olivier Baudu](cocovidalocacaducul.net/pianoagogo.php)

### 11. 22h30 | COMPOSER

* [ENREGISTREMENTS PiNG] “Mots sur maux” | Par Inès
* [LIVE]Improvisations fortes pour liens faibles, Maëlle et Julien
* ~~[LIVE]Le duo [-h-](https://www.h-artlab.com/) présente Faire FEU depuis la zad ou ailleurs, **30 min**~~
