---
title: '[MAKER À COEUR] Vincent et Matthieu, en chantier sur l''imprimante 3D !'
media_order: IMG_1030-1-768x512-1.jpg
type_ressource: text
feature_image: IMG_1030-1-768x512-1.jpg
license: cc-by-nc-sa
date: '01-06-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Matthieu et vincent se présentent, et partagent leurs avancées sur l''imprimante 3D !'
    image: IMG_1030-1-768x512-1.jpg
metadata:
    description: 'Matthieu et vincent se pr&eacute;sentent, et partagent leurs avanc&eacute;es sur l''imprimante 3D !'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/mac_vincent_et_matthieu_en_chantier_sur_l_imprimante_3d'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Vincent et Matthieu, en chantier sur l''imprimante 3D ! | Espace Ressources Num&eacute;riques'
    'og:description': 'Matthieu et vincent se pr&eacute;sentent, et partagent leurs avanc&eacute;es sur l''imprimante 3D !'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_vincent_et_matthieu_en_chantier_sur_l_imprimante_3d/IMG_1030-1-768x512-1.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '768'
    'og:image:height': '512'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Vincent et Matthieu, en chantier sur l''imprimante 3D ! | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Matthieu et vincent se pr&eacute;sentent, et partagent leurs avanc&eacute;es sur l''imprimante 3D !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_vincent_et_matthieu_en_chantier_sur_l_imprimante_3d/IMG_1030-1-768x512-1.jpg'
    'article:published_time': '2017-06-01T00:00:00+02:00'
    'article:modified_time': '2020-11-26T15:01:54+01:00'
    'article:author': Ping
---

**Pouvez-vous nous dire un peu qui vous êtes ?**

*Vincent :* Donc moi, Vincent Méryl, je fréquente Plateforme C depuis environ 3 ans maintenant. À la base je suis Breton, mais j'habite à Nantes depuis 4/5 ans. Je travaille dans l'informatique, et je viens à Plateforme C parce que c'est pas si loin de l'informatique que ça, et puis j'ai pas un appart' qui permet de bricoler à fond. J'ai pu du coup toucher un petit peu à toutes les machines, je m'amuse un peu à Plateforme C.

*Matthieu :* Moi c'est Matthieu Duval, je suis arrivé à Plateforme C il y a trois ans, un peu après Vincent. À la base je suis Breton, mais je viens pas du même coin de la Bretagne, je viens de la Bretagne Nord ! Je suis venu ici parce que j'avais pour projet de faire mon imprimante 3D, enfin d'en reproduire une dont les plans étaient disponibles sur internet. Au final, ça a prit beaucoup plus de temps que j'imaginais parce que j'avais pas pris la plus simple et que c'est pas simple dans tous les cas. Et donc, quand je suis arrivé à Plateforme C il y a trois ans, j'ai commencé à apprendre comment l'imprimante D fonctionnait, parce que je n'avais que des connaissances très théoriques sur le sujet obtenues via Youtube, mais sur la pratique j'avais rien. J'ai donc appris à travailler avec les imprimantes d'ici.

**Pouvez-vous présenter les autres adhérents qui font également partie du projet ?**

*Vincent :*  Au total nous sommes à peu près une dizaine, mais officiellement dans le noyau dur nous sommes quatre.
Il y a Jérémy Sauvrais, qui lui travaille dans tout ce qui est modélisation 3D, côté éolienne. Lui c'est un fou des imprimantes 3D, si on lui file une imprimante 3D qui est cool il l'achète ! Haha !

*Matthieu :* Et il y a Arthur Le Poivre aussi, qui est en études post-bac sur les matériaux composites. Lui il est très intéressé par tout le côté théorique, il aime bien comprendre comment ça marche.

**Comment avez-vous découvert Plateforme C ?**

*Vincent :*  Moi sur internet, il y avait un MOOC sur la modélisation numérique et c'est comme ça que j'ai découvert pour la première fois le mot « fablab ». Et en cherchant sur internet je suis tombé sur un fablab nantais. Donc je suis venu un jeudi, et me voilà !

*Matthieu :* Moi j'avais un fablab dans le lycée dans lequel j'étais, que j'ai découvert en fin de BTS. Ensuite j'ai déménagé à Nantes et comme j'avais pour projet de faire mon imprimante 3D et que je savais que dans mon appart', ça allait être compliqué d'avoir tout ce qui fallait le faire - notamment une scie circulaire et ce genre de choses - j'ai cherché un fablab sur internet à Nantes. La première fois que je suis venu c'était fermé parce que c'était férié ! Ha ha ! Mais du coup je suis revenu un peu après.

**Vous avez pour projet de fabriquer une imprimante 3D, pouvez-vous expliquer aux novices comment ça fonctionne ?**

*Matthieu :* Pour prendre une analogie assez simple, on peut la comparer à une imprimante lambda, une imprimante 2D. Imagine qu'on veuille passer plein de fois la même feuille qui sort de cette imprimante, et bien à force on aurait presque l'impression que les lettres sortiraient en relief parce que chaque passage déposerait de plus en plus d'encre. Et bah une imprimante 3D c'est exactement pareil sauf que c'est vraiment destiné à faire du relief. Donc en gros on va dessiner une tranche de l'objet, avec du plastique on va chauffer à 230° à peu près, on va faire fondre. On va dessiner notre objet et après on va monter un peu, moins d'un millimètre et on va redessiner cette forme avec la couche d'au-dessus. Donc ça va peut être varier un petit peu, c'est pour ça qu'on a cet effet de couche sur les objets sortis en imprimante 3D parce qu'on imprime plusieurs fois un plan, on imprime pas directement en 3D.

*Vincent :*  C'est toujours couche par couche. Une autre analogie que moi j'aime bien : c'est un peu comme si on prenait une pile de post-its. On prend un post-it, on découpe une forme dedans, on le pose sur la table, puis on prend un autre post-it, on découpe une autre forme légèrement différente et à force de poser des post-its on va avoir un volume qui va se faire. Donc ça fait beaucoup de post-its mais là c'est du plastique. L'imprimante 3D, avec son petit fil, elle va faire dessiner sa première couche comme avec un crayon, puis la deuxième, la troisième, et à force de superposer ça va donner du volume. Et toutes les imprimantes, grossièrement fonctionnent sur ce type-là, après c'est juste qu'il y en a où ce n'est pas du plastique mais du métal, il y en a où ça peut être du verre, de la céramique... Ça peut être différent dans la manière de fonctionner mais on a toujours cette logique de strates.

*Matthieu :* C'est pour ça qu'on aura toujours des effets de couche sur les imprimantes, peu importe le détail de telle ou telle imprimante.

*Vincent :*  Ça sera plus ou moins visible suivant le prix de l'imprimante.

**Comment vous est venue l'envie de mettre en place ce projet d'imprimante 3D ?**

*Matthieu :* Ça s'est passé durant Festival D, en 2015. Je tenais un stand pour présenter mon imprimante 3D et je n'avais pas envie d'être tout seul alors j'avais dit à Vincent de venir avec moi. Et puis on a commencé à parler un peu des défauts des imprimantes 3D qu'on connaissait, notamment celle du fablab puisque moi j'ai beaucoup travaillé avec celle-ci. On savait qu'il y avait quelques problèmes, surtout quand les températures commençaient à varier. Du coup on a commencé à se dire qu'on pouvait faire une imprimante qui se régule en terme de température. On savait que Laurent y pensait, du coup on lui en a parlé juste après le festival pour lui exposer notre projet. C'était le début des projets collectifs.

**Vous pouvez nous parler un peu de vos avancements jusqu'ici ?**

*Vincent :*  On avance petit à petit en ce moment car il y a eu une petite "période de gel" on va dire. Mais on a déjà le volume qui est assez complet car on a déjà fait l'imprimante 3D … en 3D, sur ordinateur. On a quelques pièces à imprimer, deux trois détails à finaliser et puis après bah on mélange le tout, on monte et on n'a plus qu'à la régler (ça c'est important). Ça prend du temps. Là on travaille la mécanisation du Z, donc ce qui consiste à faire en sorte que le plateau se déplace de haut en bas.

*Matthieu :* C'est ce qui permet de faire les couches.

*Vincent :* Sachant qu'on a aussi les plateaux intermédiaires qui seront découpés dans la découpe-laser. Dans les étapes imminentes. Après il y a toute la partie électronique, avec notamment la tête en X/Y, donc la tête haute qui va se déplacer sur le plateau. Tout est modélisé, il y a plein de pièces à imprimer.

*Matthieu :* Par contre, on peut pas construire toutes les pièces en 3D. Tout ce qui est visserie, les tiges qui ont besoin d'assez de précisions, l'armature, l'électronique etc… On peut pas imprimer tout ce qu'on veut. En plus on a des imprimantes plastique principalement, après on pourrait imprimer des dérivés de plastique mais ça reste du plastique. Et ça surtout ça nous permet d'avoir des éléments qui vont donner la forme de la structure et permettre l'assemblage des différentes pièces.

*Vincent :*  Il y a aussi des éléments que l'on ne va pas fabriquer au fablab. Toute la partie électronique, on pourrait la faire au fablab sauf que là c'est économiquement plus intéressant (et aussi en termes de temps) d'acheter les éléments. Ce ne sont pas des choses qui coûtent cher et c'est assez facile à trouver. En revanche il y a des pièces qu'on va vraiment fabriquer, ce sont des pièces imprimées en 3D pour l'imprimante 3D. Parce que le modèle que l'on va réaliser qui rentre dans le principe des RepRap : une imprimante 3D peut en imprimer les pièces d'une autre imprimante 3D pour participer à sa construction.

*Matthieu :* RepRap c'est un projet apparu en 2006 par un américain. C'est là où l'imprimante 3D a commencé à se démocratiser pour le grand public. Parce que pour l'industrie c'est pas du tout un procédé nouveau, c'était assez vieux mais c'était soumis à beaucoup de brevets. Et puis techniquement, toute la partie électronique a été conçue par le projet RepRap, donc ça a standardisé un peu les choses et les matériaux à utiliser. Ils ont vraiment documenté tout le truc pour le rendre accessible à tout le monde. Le projet RepRap a popularisé l'imprimante 3D au grand public.

**Arrivez-vous à estimer le temps nécessaire pour finir votre imprimante 3D ?**

*Vincent :*  On a arrêté d'estimer je crois … Haha !

*Matthieu :* Au départ on se disait que ça allait nous prendre un an, et en fait ça fait déjà deux ans qu'on est dessus. Il y a beaucoup d'aléas qui entrent en jeu, surtout la motivation. Ça varie énormément.

*Vincent :*  En plus on ne se retrouve qu'une fois par semaine, maximum, quand chacun a le temps de son côté.

*Matthieu :* On a tous une vie personnelle autour. Là on a tous essayé de se recentrer pour la rendre fonctionnelle donc on avance plutôt bien ces derniers temps. On a eu une période où on se perdait énormément dans les détails et on s'est rendu compte que cela nous faisait perdre de la motivation parce qu'on ne voyait que la montagne de ce qu'il y restait à faire plutôt que ce qu'on avait déjà fait. Donc maintenant on se recentre un peu sur ce qui a déjà été réalisé pour avancer vers la suite. Mais après, si on devait donner une estimation réaliste...

*Vincent :* Le fablab change de lieux en 2019... Il faudrait la finir avant quand même, haha !

*Matthieu :* On pourra avoir un truc qui commencera à être fonctionnel je pense dans six mois. Ensuite, ça nous prendra bien un an pour avoir quelque chose qui sera complètement conforme à ce qu'on avait spécifié au départ. Le temps de faire toute la partie régulation thermique interne à l'imprimante, de corriger tous les petits détails dont on se rend compte après l'assemblage... Parce que là typiquement je viens de finir mon imprimante et je l'ai faite marcher pendant quelques heures et je me suis rendu compte de tous les petits trucs que j'avais mal fait, donc je l'ai remise en panne pour tout réparer et les petits détails à régler cela prend énormément de temps.

**Et à quoi elle sera destiné cette imprimante 3D ?**

*Matthieu :* Et bien ici au fablab on a trois imprimantes 3D à l'heure actuelle, des asimov. Elles n'ont pas été conçues pour la charge qu'elles ont et pour l'environnement dans lequel elles évoluent. Parce qu'ici en hiver on a trois degrés, ça frôle le zéro, et en été dans le hangar on monte pas loin de 50 degrés des fois. Une imprimante 3D ça n'aime pas du tout les variations de températures parce que du coup le plastique ne fond pas de la même manière. Donc l'objectif serait que les usagers puissent l'utiliser pour avoir des impressions vraiment fiables et aussi qu'elle puisse supporter d'être un peu "martyrisée". Ce sont un peu des imprimantes de test ici, tout le monde apprend dessus, fait ses premières heures dessus et à force les machines elles n'aiment pas trop. C'est comme si on apprenait tous à conduire sur la même voiture, au bout d'un moment l'embrayage il est mort. C'est pareil pour une imprimante 3D.

*Vincent :* C'est ça, donc il y a de la casse, il y a des réparations et Laurent s'amuse pas mal avec ça !

*Matthieu :* Et du coup ça aussi ça faisait partie de nos objectifs : c'est-à-dire de la rendre facilement réparable parce que pour un fablab c'est extrêmement handicapant d'avoir une machine en panne. Et les animateurs sont justement des animateurs, pas des mécaniciens, donc ils n'ont pas forcément le temps de s'en occuper.

*Vincent :* Le but c'est aussi d'avoir des parties modulaires où au final on ne réfléchit pas à la panne, on retire le module, on le change et puis ça repart. Cela permet de dégager du temps plus tard pour régler le problème du modèle défaillant.

*Matthieu :* L'objectif c'est d'avoir une machine qui soit utilisable par tout le monde, fonctionnelle, qui pose le moins de soucis possible et qui soit disponible quand on en a besoin.

**Vous participez à nouveau à Festival D cette année, présenterez-vous votre projet ?**

*Vincent :* Sur Festival D 2017 on va présenter l'impression 3D en général déjà, parce que c'est intéressant de présenter ces technologies, de faire comprendre à Monsieur/Madame tout le monde que ce n'est plus une technique inabordable mais que ce n'est pas non plus une technique magique qui permet d'imprimer tout et n'importe quoi. On montrera les possibilités et les limites de l'imprimante 3D. Et au passage on présentera notre imprimante !

*Matthieu :* L'imprimante 3D ce n'est pas une technologie magique contrairement à ce qu'on a tendance à voir dans les journaux. Il y a eu une période où on entendait tout le temps parler de l'impression 3D : "Oui bientôt tout le monde aura une imprimante 3D chez lui !". Au vu des contraintes et de l'entretien que ça demande, ce n'est plus réaliste. Ce sont encore des machines qui sont à 1000€, ça reste très cher pour le grand public. Et pour avoir un plein usage de l'impression 3D aujourd'hui, il faut avoir énormément de connaissances, notamment en informatique ou en modélisation : des connaissances que tout le monde n'a pas le temps, l'envie ou l'intérêt d'apprendre. Je pense qu'il y aura plutôt des boutiques qui imprimeront les objets des gens.

*Vincent :* Ce qui existe déjà en partie dans certaines villes.

*Matthieu :* Il y a aussi tout un cadre juridique à mettre en place pour ce genre de choses, notamment pour les problèmes de droit d'auteur sur les objets. Ça ne pose pas encore problème parce que ça n'est pas assez démocratisé mais ça le fera quand on commencera à imprimer des pièces très complexes sur lesquelles des ingénieurs ont réfléchis durant plusieurs années. Par exemple, le ventilateur Dyson est déjà reproduit en 3D (même si c'est une version réduite qui a ses défauts), on a déjà un début et on peut imaginer que dans quelques années ce genre d'objet sera facile à réimprimer et recopier pour un moindre coût, là où justement des entreprises comme Dyson ont investi énormément en recherche et développement pour fabriquer ce genre d'objet.

*Vincent :* Pour notre imprimante, tout a été re-modélisé pour ne pas avoir des problèmes de droit justement.

*Matthieu :* Oui, et puis on a refait les pièces aussi parce qu'en fait, même s'il y en a énormément qui auraient pu être trouvées sous licence libre, tous ces objets n'ont pas forcément la même licence. Le problème aujourd'hui dans le libre, c'est aussi que toutes les licences qui sont libres ne vont pas forcément entre elles et ne fonctionnent pas pareil. Certaines seront en contradictions et donc c'est assez complexe d'utiliser une pièce d'une licence, et une pièce d'une autre licence.

**Cela fait un certain temps que vous fréquentez Plateforme C et que vous êtes impliqués dans la communauté : vous avez sûrement une petite anecdote à nous raconter ?**

*Matthieu :* Je me rappelle d'une fois où on était restés assez tard pour bosser sur l'imprimante 3D, Laurent était là. Et juste au moment de fermer la porte, la porte casse et impossible de fermer le fablab à clé... Du coup on était resté jusqu'à 22h30 (alors qu'on fermait à 21h30) pour aider Laurent. Au final j'ai pu le ramener chez lui en mettant son gros vélo dans le coffre de ma voiture. C'était assez sympa, moins rigolo pour Laurent mais c'était sympa haha !

*Vincent :* Moi j'ai coupé le courant une fois ! J'ai pas mal d'exemple dans ce style-là que j'ai pu faire en bidouillant de l'électronique (j'ai notamment fait cramer des composants...) ! Une fois, en branchant incorrectement un appareil, ça a fait disjoncter tout le fablab, j'ai donc du débrancher toutes les machines... il y a eu un grand blanc dans le fablab...

*Matthieu :* Tout le monde était fier de toi !

*Vincent :* Moi je faisais moins le fier ... haha !
