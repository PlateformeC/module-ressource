---
title: 'Les doigts dans la prise #14 : Instagram et censure des corps'
type_ressource: audio
feature_image: B9722508133Z.1_20200207151213_000+G6TFFI1E9.1-0.jpg
license: cc-by-nc-sa
published: true
date: '20-11-2020 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Chloé Gourgues'
aura:
    pagetype: website
    description: 'Cette semaine Chloé Gourgues nous parle de la censure des corps sur Instagram.'
    image: B9722508133Z.1_20200207151213_000+G6TFFI1E9.1-0.jpg
metadata:
    description: 'Cette semaine Chlo&eacute; Gourgues nous parle de la censure des corps sur Instagram.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/sun14_octobre20_instagram%20et%20censure%20des%20corps'
    'og:type': website
    'og:title': 'Les doigts dans la prise #14 : Instagram et censure des corps | Espace Ressources Num&eacute;riques'
    'og:description': 'Cette semaine Chlo&eacute; Gourgues nous parle de la censure des corps sur Instagram.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/sun14_octobre20_instagram%20et%20censure%20des%20corps/B9722508133Z.1_20200207151213_000+G6TFFI1E9.1-0.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1350'
    'og:image:height': '759'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #14 : Instagram et censure des corps | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cette semaine Chlo&eacute; Gourgues nous parle de la censure des corps sur Instagram.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/sun14_octobre20_instagram%20et%20censure%20des%20corps/B9722508133Z.1_20200207151213_000+G6TFFI1E9.1-0.jpg'
    'article:published_time': '2020-11-20T16:31:00+01:00'
    'article:modified_time': '2020-12-09T14:06:20+01:00'
    'article:author': Ping
---

**Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire). Cette semaine Chloé Gourgues nous parle de la censure des corps sur Instagram.**

Pour écouter le podcast c’est par [ici](https://www.lesonunique.com/content/les-doigts-dans-la-prise-instagram-censure-des-corps) !

![Les doigts dans la prise - Instagram et censure des corps](Instagram%20et%20censure%20des%20corps.mp3)

Bonne écoute !

*Photo : couverture de Télérama sur la grossophobie, par Jérôme Bonnet. Modèle : Leslie Barbara Butch.*
