---
title: 'Les détournements de la découpe vinyle'
media_order: papiermachinecouverture.png
date: '01-01-2016 00:00'
taxonomy:
    category:
    - pratiques
    tag:
    - 'Papiermachine'
    - 'Papier/machine'
    - 'decoupeusevinyle'
    - 'decoupevinyle'
    - 'plotter'
    - 'papier'
    - 'cartepostale'
    - 'gravuresurpapier'
    - 'gravure'
    - 'decoupe'
    - 'detournement'
    - 'Expérimentationmatériaux'
    - 'machineadessiner'
    - 'cutter'
    - 'workshop ESBAN'
    - 'typographie'
    - 'ecrire'
    - 'texte'
    author:
    - 'PiNG'
    aura:
      pagetype: article
      description: ‘Des détournements de la découpe vinyle au Fablab Plateforme C’
      image: ecrire a la decoupe vynil 8.jpg

---
## Produire du texte à la découpe vinyle

![ecrire a la decoupe vynil 3.jpg](ecrire a la decoupe vynil 3.jpg?forceresize=100%)

Le projet d'exploration de PiNG Papier/ Machine a accueilli 22 étudiant·es de l’École des Beaux-Arts de Nantes pour expérimenter les potentiels de création graphique du fablab, autour de la série et de la carte postale.
Voici quelques unes des expérimentations réalisées.

Nous avions déjà pu voir la découpe vinyle utilisée comme machine à dessiner en remplaçant la petite lame ( plotter* de découpe) par un stylo bille par exemple, produisant de magnifiques volutes parfaitement géométriques, faisant concurrence aux meilleurs spyrographes.
On a pu aussi trouver des ressources ici sur le dessin avec une machine de ce type.
[Fablab descartes - dessiner avec une decoupe vinyle](https://www.fablab-descartes.com/projects/comment-dessiner-avec-la-decoupe-vinyle/)


Au fablab Plateforme C, nous avons une contrainte importante sur un projet comme papier machine : pas de photocopieur ou de dispositif dédiés à la reproduction de texte. Il en découle de nombreuses expériences pour contourner ce handicap.
Voici l'une d'entre-elles :

Des étudiant·es ont utilisé la découpeuse vinyle pour la transformer en machine à dessiner en remplaçant le plotter de découpe par un stylo à bille usagé, afin de ne pas laisser de trace colorée, et d'éviter de «blesser» le papier.
On produit ainsi une forme d'embossage*.
Il est  intéressant de se pencher sur les support utilisés.
Le papier aluminium réagit très bien.
![ecrire a la decoupe vynil 12.jpg](ecrire a la decoupe vynil 12.jpg?forceresize=100%)
Avec le plotter de découpe, l'utilisation de papiers épais légèrement plastifiés et scintillants est également intéressant, amenant une typographie subtilement identifiable.
![ecrire a la decoupe vynil 1.jpg](ecrire a la decoupe vynil 1.jpg?forceresize=100%)


*plotter : A la base, un plotter est une machine qui trace des lignes ("to plot" en anglais veut dire tracer), à la différence d'une imprimante, dont le travail va être de couvrir toute une surface avec de l'encre. A ce titre, les plotter sont typiquement utilisés pour imprimer les patrons de couture, les schémas techniques en ingénierie ou en architecture. 
Un plotter fonctionne avec une tête qui vient se promener sur le matériau, tête qui dispensera de l'encre, mais qui pourra également être dotée d'une lame afin de découper le matériau choisi.
[Plotter de découpe - markerist](https://www.makerist.fr/topics/Tout-savoir-sur-le-plotter-de-decoupe)
*L’embossage est une technique qui a pour objectif de créer des formes en relief dans du papier ou un autre matériau déformable. L’embossage est la version artisanale de l’emboutissage, qui s’applique le plus souvent à de la tôle.
[Embossage - wikipedia](https:/fr.wikipedia.org/wiki/Embossage)

![ecrire a la decoupe vynil 2.jpg](ecrire a la decoupe vynil 2.jpg?forceresize=100%)
![ecrire a la decoupe vynil 4.jpg](ecrire a la decoupe vynil 4.jpg?forceresize=100%)
![ecrire a la decoupe vynil 6.jpg](ecrire a la decoupe vynil 6.jpg?forceresize=100%)
![ecrire a la decoupe vynil 7.jpg](ecrire a la decoupe vynil 7.jpg?forceresize=100%)
![ecrire a la decoupe vynil 8.jpg](ecrire a la decoupe vynil 8.jpg?forceresize=100%)
![ecrire a la decoupe vynil 9.jpg](ecrire a la decoupe vynil 9.jpg?forceresize=100%)
![ecrire a la decoupe vynil 11.jpg](ecrire a la decoupe vynil 11.jpg?forceresize=100%)
![ecrire a la decoupe vynil 13.jpg](ecrire a la decoupe vynil 13.jpg?forceresize=100%)

## Dessiner/graver à la découpe vinyle

![decoup-vynil-dessin-web.jpg](decoup-vynil-dessin-web.jpg?forceresize=100%)

Dans la même idée, un autre étudiant a expérimenté autour du dessin
Normalement cette machine est conçue pour découper dans des rouleaux de matière vinylique ( en gros, dans de l'autocollant en plastique) , mais ici son utilisation sur papier épais permet de faire apparaître un autre usage de la machine.
Au fur et à mesure des passages, la lame grave et déchire la surface sans toutefois la découper.
L'usage du papier noir permet d'amener un moirage subtil, la lumière qui se reflète sur le support contrastant avec les surfaces gravées, plus sombres.

Vous trouverez ici ses tests utilisant la lame du «plotter» comme outil de dessin, sur du papier couché*  noir très épais.

![decoupe-vynil-dessin-web.jpg](decoupe-vynil-dessin-web.jpg?forceresize=100%)

*Papier couché:
Le papier ou carton couché est un papier ou carton dont la surface est recouverte d'une ou plusieurs couches généralement constituées de produits minéraux (pigments) en mélange avec des liants et des produits d'addition divers.(...)Le but de cette opération est de permettre une meilleure reproduction des impressions en transformant la surface rugueuse et macroporeuse du papier en une face unie et microporeuse et d'améliorer la blancheur du papier ou du carton, son aspect (brillant, par exemple), son toucher.
[Papier couché - wikipedia](https://fr.wikipedia.org/wiki/Papier_couché)




### Autres ressources
Matériaux utilisés :
papier couché noir
papier aluminium
papier à grain blanc
**▸ Documentation technique**

Retrouvez l’ensemble de la documentation sur le workshop ici:
xxxxxx
[www.fablabo.net/wiki/Papier/Machine](http://fablabo.net/wiki/Papier/Machine)
xxxxxxx
