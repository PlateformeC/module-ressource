---
title: '[MAKER À COEUR] Mustafa, un parcours guidé par les rencontres'
media_order: Mustafa_R-870x550.jpg
type_ressource: text
feature_image: Mustafa_R-870x550.jpg
license: cc-by-nc-sa
date: '01-02-2018 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Nous vous présentons Mustafa : stagiaire chez PiNG dont l’objectif est de croiser les makers évoluant à Plateforme C !'
    image: Mustafa_R-870x550.jpg
metadata:
    description: 'Nous vous pr&eacute;sentons Mustafa : stagiaire chez PiNG dont l&rsquo;objectif est de croiser les makers &eacute;voluant &agrave; Plateforme C !'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/mac_mustafa_un_parcours_guide_par_les_rencontres'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Mustafa, un parcours guid&eacute; par les rencontres | Espace Ressources Num&eacute;riques'
    'og:description': 'Nous vous pr&eacute;sentons Mustafa : stagiaire chez PiNG dont l&rsquo;objectif est de croiser les makers &eacute;voluant &agrave; Plateforme C !'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_mustafa_un_parcours_guide_par_les_rencontres/Mustafa_R-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Mustafa, un parcours guid&eacute; par les rencontres | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Nous vous pr&eacute;sentons Mustafa : stagiaire chez PiNG dont l&rsquo;objectif est de croiser les makers &eacute;voluant &agrave; Plateforme C !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/mac_mustafa_un_parcours_guide_par_les_rencontres/Mustafa_R-870x550.jpg'
    'article:published_time': '2018-02-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T15:04:21+01:00'
    'article:author': Ping
---

*Nous vous présentons Mustafa : stagiaire chez PiNG dont l’objectif est de croiser les makers évoluant à Plateforme C !*

**Bonjour Mustafa ! Peux-tu nous dire qui tu es ?**

Je m’appelle Mustafa Shalan, j’ai 29 ans. Je suis en France depuis 2 ans, avant j’étais prothésiste dentaire en Géorgie. Depuis septembre 2017 je suis en formation à l’École de la deuxième chance et mon projet est de travailler dans la maintenance informatique. Cette école est là pour aider les jeunes sans diplôme et sans le bac à trouver une formation ou un job. Nous avons 5 stages à faire durant notre parcours à l’école : j’en ai déjà fait 3, dont un en maintenance informatique et un en réparation de vélos. Le 4ème stage je le fais en ce moment au fablab Plateforme C !

**Comment es-tu arrivé à Plateforme C ?**

J’aime beaucoup réparer des choses (comme par exemple mon ordinateur, une lampe, …), un jour ma femme m’a proposé d’aller à Plateforme C. Mais c’était à une période où je ne parlais pas encore bien français, alors l’idée de rencontrer d’autres personnes me faisait un peu peur.

Quelques temps plus tard, après avoir appris un peu mieux le français, un ami m’a reparlé du fablab en me disant que c’est un bon endroit pour rencontrer de nouvelles têtes. Nous avons alors commencé à nous renseigner sur les différents projets menés là-bas. Et, juste après, mon école nous a proposé de participer à un projet durant deux semaines au fablab afin de fabriquer un objet à présenter à la semaine culturelle : c’était l’opportunité parfaite pour moi de découvrir Plateforme C !

Ce projet m’a permis de rencontrer des adhérents et les membres de l’équipe : Adrien, Maëlle, Laurent. Nous avons passé 10 jours en tout à Plateforme C dans le cadre de ce projet et j’ai vraiment aimé l’environnement. Cela m’a mis en confiance pour demander un stage chez PiNG ! Pour moi, ce qui est le plus important au fablab c’est de faire des rencontres, de vivre différentes expériences et différents projets… Même s’il fait trop froid là-bas ! (rire)

**Sur quoi travailles-tu dans le cadre de ton stage à PiNG ?**

L’idée c’était de commencer avec les machines à coudre. Je trouve ces machines très importantes dans la vie de tous les jours : nous avons toujours besoin d’acheter tout et n’importe quoi et souvent c’est trop cher.

La première machine sur laquelle j’ai commencé à travailler au fablab ne fonctionnait pas. Plusieurs personnes travaillent avec et chacun a ses propres réglages, ce qui peut expliquer ce genre de problèmes. J’ai alors demandé de l’aide à un adhérent : nous avons repéré qu’il y avait un souci pour passer le fil dans la machine et, durant deux heures, nous avons essayé de comprendre ensemble pourquoi la machine ne marchait pas.

Pierre-Guillaume, un autre adhérent, est ensuite arrivé. Il était motivé pour nous aider. Nous avons à nouveau passé deux heures à tenter de résoudre le problème mais la machine ne fonctionnait toujours pas. Nous avons cherché sur internet, sur youtube, mais elle refusait de marcher !

Je me suis alors rendu compte que c’est vraiment intéressant de voir comment les gens viennent vers toi et te demandent « Tu as besoin d’aide ? Qu’est ce que tu fais ? Tu connais cette machine ? ». C’est un partage d’expérience, je trouve ça magnifique.

Après tout ça, Pierre-Guillaume a dit qu’il avait une machine chez lui et qu’il la ramènerait le lendemain. C’est ce qu’il a fait et grâce à ça j’ai pu commencer un projet de manteau de pluie. J’ai eu des soucis au niveau du premier matériau que j’ai utilisé, qui était fragile, mais c’est intéressant aussi de faire différents essais.

Ensuite, le plan de mon stage a un peu changé parce que le froid est arrivé et Plateforme C a été fermé pour les deux dernières semaines. J’ai donc été à l’Atelier Partagé du Breil où j’ai continué à peu près sur les mêmes projets mais en changeant certaines choses. Par exemple, j’ai entamé une formation en ligne pour apprendre le langage Python. L’Atelier Partagé (plus calme que la fablab) me permet de me concentrer un peu plus là-dessus. Je travaille également sur un jeu HTML, j’ai commencé à le fabriquer et dessiner une boite sur le logiciel Inkscape. J’attends la réouverture de Plateforme C pour pouvoir la faire avec la découpe laser.

**As-tu une idée de projet que tu souhaites réaliser au fablab, après ton stage ?**

Je n’ai pas vraiment réfléchi à cela, je veux surtout continuer d’apprendre à Plateforme C. J’ai envie d’observer comment le fablab fonctionne : cela m’intéresse plus que d’avoir un projet précis. Je n’ai pas grand-chose de prévu après le stage, je vais donc revenir à Plateforme C pour continuer à apprendre et, après, je choisirai quelque chose de précis. Je souhaite continuer à croiser du monde, créer et mettre en œuvre un projet ça sera pour plus tard !
