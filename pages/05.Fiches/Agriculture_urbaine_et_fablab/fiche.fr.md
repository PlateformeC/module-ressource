---
title: 'Agriculture urbaine et fablab'
media_order: agrifablab.jpg
type_ressource: text
feature_image: agrifablab.jpg
license: cc-by-nc-sa
date: '19-09-2016 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - fablab
        - 'agriculture urbaine'
        - biolab
        - jardin
    author:
        - 'Michka Mélo'
aura:
    pagetype: article
    description: 'PiNG lance ce mois-ci la deuxième saison de son programme de recherche-action « Agriculture urbaine et fablabs '
    image: agrifablab.jpg
metadata:
    description: 'PiNG lance ce mois-ci la deuxi&egrave;me saison de son programme de recherche-action &laquo; Agriculture urbaine et fablabs '
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/agriculture_urbaine_et_fablab'
    'og:type': article
    'og:title': 'Agriculture urbaine et fablab | Espace Ressources Num&eacute;riques'
    'og:description': 'PiNG lance ce mois-ci la deuxi&egrave;me saison de son programme de recherche-action &laquo; Agriculture urbaine et fablabs '
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/agriculture_urbaine_et_fablab/agrifablab.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Agriculture urbaine et fablab | Espace Ressources Num&eacute;riques'
    'twitter:description': 'PiNG lance ce mois-ci la deuxi&egrave;me saison de son programme de recherche-action &laquo; Agriculture urbaine et fablabs '
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/agriculture_urbaine_et_fablab/agrifablab.jpg'
    'article:published_time': '2016-09-19T00:00:00+02:00'
    'article:modified_time': '2020-11-25T13:43:00+01:00'
    'article:author': Ping
---

*PiNG lance ce mois-ci la deuxième saison de son programme de recherche-action « Agriculture urbaine et fablabs »
Dans le cadre de ce programme, l’association propose de soumettre à la communauté des personnes qui travaillent la terre et entretiennent une relation privilégiée avec le vivant et la nature (jardiniers, maraîchers, paysans, agriculteurs, botanistes, etc.) la pertinence des fablabs comme lieux de bricolage, de fabrication et de recherche susceptibles d’être utiles dans leurs pratiques.*

Après une saison consacrée à l’identification d’acteurs locaux et à la conception du programme, cette seconde année sera consacrée à l’exploration de deux premiers axes de recherche, et à leur mise en pratique .
La première partie présente la réflexion qui nous a amenés à lancer ce programme, et à choisir les deux premiers axes de recherche. La seconde partie nous projette dans une fiction après cinq ans de recherche, pour donner une idée de ce à quoi ce programme pourrait mener.

### L’AGRICULTURE URBAINE

L’agriculture urbaine est un terme aux contours flous, qui regroupe plusieurs types de pratiques. On y retrouve les jardiniers urbains, qui auto-produisent une partie de leur nourriture dans les jardins familiaux et les jardins partagés. On y retrouve également les maraîchers péri-urbains, professionnels de l’agriculture, et sur lesquels reposait principalement l’autonomie alimentaire des villes avant sa destruction progressive par l’étalement urbain et la spécialisation des territoires. On y retrouve enfin les fermes urbaines industrielles, gigantesques et automatisées, qui restent de l’ordre du fantasme malgré quelques expériences pionnières en cours.
Il a été démontré que la plupart des grandes villes européennes ne disposent que de quelques jours d’autonomie alimentaire. Ceci questionne de façon relativement alarmante leur résilience aux ruptures potentielles des chaînes d’approvisionnement, par exemple suite à des événements climatiques extrêmes, dont la fréquence est très probablement amenée à augmenter dans les années à venir.
L’agriculture urbaine est intéressante, car elle permet de répondre à cet enjeu de façon relativement simple et concrète : chacun peut commencer à cultiver autant que possible dans la ville.

Bien que prometteuse, l’agriculture des villes occidentales est aujourd’hui très loin de leur permettre d’atteindre une autonomie alimentaire. L’étalement urbain chassant les maraîchers de la périphérie est de loin la cause la plus menaçante pour l’autonomie alimentaire des villes. De plus, les centres-villes manquent d’espaces pouvant être dédiés à la production alimentaires dans de bonnes conditions : le sol et les eaux y sont souvent pollués par les autres activités urbaines présentes et passées, et la minéralisation extrême de la ville induit des micro-climats défavorables à la production alimentaire. Par ailleurs, les fermes urbaines industrielles, souvent érigées comme le futur de l’autonomie alimentaire, ne peuvent pas être considérées comme une solution crédible. Leur consommation énergétique est démesurée par rapport à l’énergie nutritionnelle qu’elles apportent, et elles sont dépendantes d’une infrastructure d’automatisation numérique largement basée sur des ressources non-renouvelables en raréfaction.

### LES FABLABS

La dernière décennie a vu l’émergence, dans les villes occidentales, de laboratoires de fabrication, ou fablabs. Ce sont des lieux ouverts et collectifs, où des bricoleurs se retrouvent autour d’outils (notamment à commande numérique) pour “fabriquer à peu près n’importe quoi”, pour reprendre la formulation de leurs inventeurs, les chercheurs du Center for Bits and Atoms du Massachussets Institute of Technology (MIT).
Comme l’ont remarqué plusieurs chercheurs, le fablab porte les germes d’une souveraineté productive et technologique accrue. Les usagers du fablab se posent la question de la fabrication et de la réparation des objets du quotidien, et démontrent certaines velléités d’auto-production. Les chercheurs les plus enthousiastes vont jusqu’à parler de ré-appropriation des moyens de production, grâce :
– à la diffusion aisée des fichiers de fabrication numérique créés dans un fablab vers le monde entier via internet :
– aux bonnes capacités de production en petite série des machines à commande numérique, ce qui était moins le cas avec le bricolage manuel.

Toutefois, la réalité du fablab n’a pas encore atteint son idéal. On y trouve plus d’activités de l’ordre du hobby que de la fabrication de produits de première nécessité. On remarque également que le prototype et la pièce unique y occupent encore une place plus importante que la petite série. Enfin, le fablab est encore trop dépendant de l’industrie pour la fabrication de ses machines et de ses consommables : un fablab n’est pas encore capable de produire complètement l’équipement du prochain fablab avec des ressources locales, ce qui serait un bon indicateur de souveraineté productive.

### AXES DE RECHERCHE

Le programme de recherche-action “Agriculture urbaine et fablab” propose de voir comment ces deux tendances peuvent converger, et ainsi dépasser leurs limites actuelles. De nombreux axes de recherche pourraient être proposés, en voici deux qui nous semblent pertinents :

**Un Atelier Paysan Urbain**, inspiré de l’Atelier Paysan, proposant aux acteurs de l’agriculture urbaine de concevoir, prototyper et produire les outils dont ils ont besoin en s’appuyant sur la communauté et les outils du fablab.
L’agriculture urbaine et péri-urbaine nécessite des outils adaptés à la production sur petites surfaces et surfaces verticales, à l’utilisation des déchets organiques urbains abondants et sous-utilisés, ainsi qu’à la protection contre la pollution urbaine et contre les micro-climats hostiles.
Le fablab pourra ainsi être utile à l’auto-production d’outils en petite série répondant à un besoin essentiel (l’alimentation).

**Un biolab low-tech « Biodikê »** proposant aux agriculteurs urbains et aux bricoleurs du fablab de travailler ensemble au développement de biomatériaux et de bio-procédés répondant à leurs besoins.
Le fablab pourrait ainsi s’appuyer sur les déchets organiques urbains, en particulier les déchets non comestibles des jardins, pour produire certain des consommables et outils nécessaires à leur fonctionnement, et ainsi s’affranchir de sa dépendance actuelle à une industrie socialement et écologiquement contestable.
L’agriculture urbaine trouverait ainsi un débouché pour une partie de sa production qu’elle peine à valoriser, des consommables nécessaires à travers ces biomatériaux, par exemple pour la production de ses outils.

Pour donner une image plus concrète des résultats que pourrait donner ce programme de recherche-action, suivez-moi pour un petit tour à vélo dans le futur.

### LE FUTUR GOÛT FRAISE

Nantes, jeudi 15 juillet 2021, fin d’après-midi. Je quitte l’ordinateur et ma documentation sur le wiki [fablabo.net](http://fablabo.net/wiki/Accueil) pour aller à l’OPENatelier. En sortant de mon appartement au rez-de-chaussée, je jette un œil à ma tour à patates, qui trône à côté de la porte : il va bientôt être temps de rajouter un quatrième étage. Les plants ont l’air en bonne santé. Je devrais pouvoir récolter les 25 kg de pommes de terre d’ici trois semaines, déjà la deuxième récolte de l’année ! J’enfourche joyeusement mon vélo à cette perspective. Je remercie intérieurement l’atelier mené en collaboration par PiNG et [ECOS](http://www.ecosnantes.org/), qui m’a permis de construire la structure de ma tour à Plateforme C, tout en apprenant tous les secrets de la culture des pommes de terre.

Sur la route, je passe à coté du jardin partagé du Tillay. Je vois que la haie comestible est couverte de framboises et de groseilles. Je salue les jardiniers en plein cueillette, et je remarque que Mathieu, très cher ami de Plateforme C, ramasse précautionneusement un peu de terre de la zone du jardin encore en friche. Il va probablement faire un test de pollution du sol avec le nouveau kit DIY qu’il a contribué à développer en collaboration avec d’autres membres de PiNG, des chercheurs de l’université, et avec le soutien de [Nantes Villes Comestible](https://www.nantesvillecomestible.org/).
En passant sur le pont Anne de Bretagne, je pense à la petite lampe qui m’attendra chez moi, devant la porte, en rentrant. Alimentée par une pile à bactéries nourrie à la vase de Loire !

J’arrive à l’OPENatelier. Sur le routeur CNC “Sentier Battu”, Frédéric, Dominique et un autre membre d’Ecos veillent sur la découpe d’un panneau à particules un peu spécial. Ces panneaux sont faits de bois raméal fragmenté (BRF), issu de la taille hivernale de la haie comestible du jardin partagé du Tillay, et aggloméré par du [mycélium](https://fr.wikipedia.org/wiki/Myc%C3%A9lium).
Comment l’avons-nous fabriqué ? Par une belle journée d’hiver, nous avons broyé les branches issus de la taille de la haie et de quelques arbres avec le broyeur solaire, conçu et fabriqué par les collègues de l’Atelier du Paysan Urbain. Ensuite, à Plateforme C, nous avons fait pousser du mycélium de champignon dans le BRF pour l’agglomérer.
Seront découpés dans le panneau les éléments d’une structure verticale, qui sera montée contre la façade de Plateforme C, pour faire pousser quelques kilos de haricots en grains tardifs, et créer un lieu ombragé à l’extérieur du fablab pendant le mois d’août.

Sur la grande table centrale, des enfants en stage Fablab Junior assemblent un circuit avec des fils électriques de mycélium. Ces fils sont cultivés par Juliette dans l’eau d’un étang pollué aux métaux lourds, étang découvert par le collectif Fertile dans une des friches industrielles de l’île de Nantes.

Pendant ce temps, quelques membres de [Mire](http://www.mire-exp.org/) modifient une de leur caméras. Deux d’entre elles changent un composant sur un des circuits électroniques régulant la capture d’images. Un des nouveau composants est un peu particulier : c’est une diode dont le semi-conducteur est un colorant bleu extrait de l’indigo, plante fraîchement cultivée dans la micro-serre d’un des jardins d’Ecos. L’extraction de l’indigo a été effectuée par les étudiants en chimie de l’[IUT](http://www.iutnantes.univ-nantes.fr/), partenaire de Plateforme C, dans le cadre de leurs travaux pratiques.
Deux autres membres de Mire finissent d’enlever le support et de polir une pièce de plastique pour la caméra, pièce qui sort tout juste de l’imprimante 3D. Mais de quel plastique est-elle faite ?
Du plastique produit par quelques membres de PiNG, à partir d’algues. Ces algues sont cultivées dans le bio-réacteur (fait de matériel récupéré et de déchets électroniques) qui ronronne vers le mur du fond. La biomasse de ces algues est ensuite traitée par des extraits de plantes phyto-épuratrices cultivées à jardin C, pour être transformée en plastique.
Grâce à ce nouveau procédé, développé à Plateforme C, et au bon vieux Filabot, voilà bien longtemps que n’ont plus été commandées des bobines de filament pour les imprimantes 3D !

Attendez une seconde, vous avez dit juillet ? Il fait beaucoup trop chaud à Plateforme C, personne ne peut y travailler… Eh bien si, maintenant oui !
Lors du grand chantier participatif du printemps, ont été placés, sur les parois intérieures du fablab, de grands panneaux de mycélium, isolant et ignifuge, produits à partir de déchets de tonte des parcs publics nantais, déchets obtenus via Nantes Ville Comestible.
De plus, la co-génératrice diffuse une fraîcheur appréciable en cette chaude journée. Cette savante machine, adaptée d’un modèle existant par des membres de PiNG férus de thermique, permet d’alimenter Plateforme C en électricité et en chaleur – ou en fraîcheur, selon la saison – à partir de méthane.
Quel méthane ? Le méthane produit par le micro-méthaniseur (libre de droit) développé par les lyonnais d’[Open Micro-Métha](https://lapaillassaone.wordpress.com/les-projets/micrometha/), construit par quelques étudiants en génie mécanique et électronique de l’IUT de Nantes dans le cadre de leur projet de fin d’études, et installé au jardin du Tillay. Des quantités importantes de méthane peuvent ainsi être produites à partir des déchets organiques qu’apportent les usagers du fablab. Les lixiviats (ou résidus solides) de méthanisation étant compostables, cette précieuse manne de nutriments reste disponible pour les cultures du jardin.

Je discute avec Alexandre, agriculteur péri-urbain en AMAP, venu à Plateforme C pour faire une réparation délicate sur un de ses outils avec Fred, spécialiste en réparation et bricoleur hors-pair de Plateforme C. Il m’annonce qu’une nouvelle toilette sèche à séparation va prochainement être installée sur son exploitation par la coopérative Ecosec.
Les matière fécales collectées viendront donner un bon coup de boost à la production de méthane. L’urine des toilettes sèches, elle, sera stockée six mois, puis directement utilisée pour l’irrigation et la fertilisation de ses cultures.
Si ça fonctionne bien chez lui, les mêmes toilettes, qui fonctionnent aussi en intérieur sans créer d’odeur, seront installées à Plateforme C, et alimenteront son exploitation.

Sur un coin de la grande table, des étudiants de l’[École d’Architecture](http://www.nantes.archi.fr/), partenaire de Plateforme C, finalisent un plan de re-végétalisation comestible de l’île de Nantes. Sur leur écran, plusieurs dizaines d’arbres fruitiers, des arbustes à petits fruits, et un certain nombre de plantes pérennes comestibles se partagent la plupart des plates-bandes et petits squares du quartier.
Ils se sont largement appuyés sur le logiciel d’aide à la conception de communautés végétales (des associations de cultures bénéfiques et synergiques entre plusieurs plantes) développé par Laurent, talentueux codeur membre de PiNG, durant les quelques derniers mois.
Leur projet de diplôme sur le sujet, mené avec le soutien de Thomas de PiNG, d’Ecos et de Nantes Villes Comestible, sera auditionné par les élus de la ville de Nantes d’ici quelques jours.
Certains de leurs camarades bossent sur la maquette, dont certaines pièces viennent juste de sortir de la découpe-laser.

Après avoir échangé avec les uns et les autres, je me relance dans mon projet du moment : dessiner le schéma de mon ordinateur construit à 100 % avec des matériaux issus du jardin. Ça avance, mais il y a encore de longues soirées en perspective !

Un article de Michka Mélo, bio-ingénieur et explorateur-associé de PiNG
