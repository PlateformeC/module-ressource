---
title: 'NEC Open source'
type_ressource: fichier
feature_image: 1536-1024.jpg
license: cc-by-nc-sa
taxonomy:
    category:
        - pratiques
    tag:
        - 'médiation numérique'
        - 'open source'
        - NEC
    author:
        - PiNG
aura:
    pagetype: website
    description: 'L’événement Numérique en Commun(s) a été conçu comme un espace de travail et de rencontres au service de tous les acteurs qui souhaitent contribuer à l’émergence d’une société numérique innovante et inclusive.'
    image: 1536-1024.jpg
show_breadcrumbs: true
metadata:
    description: 'L&rsquo;&eacute;v&eacute;nement Num&eacute;rique en Commun(s) a &eacute;t&eacute; con&ccedil;u comme un espace de travail et de rencontres au service de tous les acteurs qui souhaitent contribuer &agrave; l&rsquo;&eacute;mergence d&rsquo;une soci&eacute;t&eacute; num&eacute;rique innovante et inclusive.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/nec_open_source'
    'og:type': website
    'og:title': 'NEC Open source | Espace Ressources Num&eacute;riques'
    'og:description': 'L&rsquo;&eacute;v&eacute;nement Num&eacute;rique en Commun(s) a &eacute;t&eacute; con&ccedil;u comme un espace de travail et de rencontres au service de tous les acteurs qui souhaitent contribuer &agrave; l&rsquo;&eacute;mergence d&rsquo;une soci&eacute;t&eacute; num&eacute;rique innovante et inclusive.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/nec_open_source/1536-1024.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1536'
    'og:image:height': '1024'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'NEC Open source | Espace Ressources Num&eacute;riques'
    'twitter:description': 'L&rsquo;&eacute;v&eacute;nement Num&eacute;rique en Commun(s) a &eacute;t&eacute; con&ccedil;u comme un espace de travail et de rencontres au service de tous les acteurs qui souhaitent contribuer &agrave; l&rsquo;&eacute;mergence d&rsquo;une soci&eacute;t&eacute; num&eacute;rique innovante et inclusive.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/nec_open_source/1536-1024.jpg'
    'article:published_time': '2020-11-26T15:53:59+01:00'
    'article:modified_time': '2020-11-26T15:53:59+01:00'
    'article:author': Ping
---

*L’événement Numérique en Commun(s) a été conçu comme un espace de travail et de rencontres au service de tous les acteurs qui souhaitent contribuer à l’émergence d’une société numérique innovante et inclusive.*

Propulsé en 2018 par la Mission Société Numérique de l’Agence du Numérique, la Mednum et PiNG, les organisateurs ont souhaité documenter et mettre à disposition l’organisation de l’événement pour en faire un commun au service de la communauté des acteurs qui y a contribué.

Cette documentation, produite par les organisateurs de NEC18, a vocation à évoluer au gré de son appropriation par de nouveaux organisateurs. Elle peut également servir à toute organisation de conférence participative, même s’il ne s’agit pas d’un événement Numérique en Commun[s].

## PROGRAMMATION

- Note de cadrage
- Appel à contribution (texte & formulaire)
- Programme final

[TÉLÉCHARGER](PROG_NEC_opensource.zip?classes=btn) (1,2Mo)

## PRODUCTION ÉVÉNEMENTIELLE

- Rétroplanning global – à venir
- Gestion des intervenants
- Gestion des contributeurs
- Scénographie
- Catering
- Les ressources humaines
- Gestion des bénévoles
- Régie technique
- Accueil

[TÉLÉCHARGER](PROD_NEC_opensource.zip?classes=btn) (3Mo)

## COMMUNICATION

- Plan de communication de l’édition 2018 sous forme de retro-planning commenté
- Identité visuelle
- Réseaux sociaux, site internet, diffusion
- Supports imprimés

[TÉLÉCHARGER](COM_NEC_opensource.zip?classes=btn) (87Mo)
