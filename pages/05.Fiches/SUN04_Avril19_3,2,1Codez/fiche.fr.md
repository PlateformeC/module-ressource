---
title: 'Les doigts dans la prise #4 : le live coding'
type_ressource: audio
feature_image: IMG_2926_light.jpg
license: cc-by-nc-sa
published: true
date: '02-04-2019 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Maëlle Vimont'
aura:
    pagetype: article
    description: 'Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne .Le Fil de l’histoire.'
    image: IMG_2926_light.jpg
metadata:
    description: 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne .Le Fil de l&rsquo;histoire.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/sun04_avril19_3,2,1codez'
    'og:type': article
    'og:title': 'Les doigts dans la prise #4 : le live coding | Espace Ressources Num&eacute;riques'
    'og:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne .Le Fil de l&rsquo;histoire.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/sun04_avril19_3,2,1codez/IMG_2926_light.jpg'
    'og:image:type': image/webp
    'og:image:width': '1000'
    'og:image:height': '667'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #4 : le live coding | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne .Le Fil de l&rsquo;histoire.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/sun04_avril19_3,2,1codez/IMG_2926_light.jpg'
    'article:published_time': '2019-04-02T16:31:00+02:00'
    'article:modified_time': '2020-12-09T13:43:03+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Pour écouter le podcast c’est par [ici](http://www.lesonunique.com/content/les-doigts-dans-la-prise-le-live-coding) !

![Les doigts dans la prise - le live coding](4%20-%20Les%20doigts%20dans%20la%20prise%20-%20le%20live%20coding.mp3)

Le thème de la semaine nous invite à jouer avec des chiffres et des maths. Aussi, pour cette nouvelle chronique « les doigts dans la prise » de PiNG, je ne vais pas y aller par 4 chemins, je vais vous parler «d’algorithmes» (un mot compte triple au scrabble) et… ce n’est pas tout. Je vais aussi tenter de vous démontrer par A + B comment nous pouvons les utiliser pour nous exprimer, faire de la musique, et reprendre, tout en dansant joyeusement, un peu de contrôle sur notre environnement toujours plus numérique. Tout ça en 3 minutes oui, oui, oui.

Allez, 3, 2, 1, c’est parti !

Pour commencer, analysons le problème. C’est quoi un algorithme ? Et bien ce n’est ni plus ni moins qu’**une suite d’opérations ou d’instructions permettant de résoudre un problème.** Le mot algorithme lui, vient du nom d’un mathématicien perse du IXe siècle. **On rencontre des algorithmes partout dans notre quotidien, sans même s’en rendre compte.** Quand on fait la cuisine par exemple, on suit une recette (ou du moins on essaye) : je mets les œufs puis la farine, puis le sucre etc… ça, c’est un algorithme. Quand on fait du tissage, ou qu’on élabore des tactiques de progression en foot, on fait aussi des activités « algorithmiques ».

Mais quand on entend parler d’algorithmes aujourd’hui, on pense plutôt à ceux qui effectuent des calculs à partir de grandes masses de données numériques. On entre dans le fameux « Big Data », les débats sur « l’intelligence artificielle »… et on s’approche du problème. **Ces algorithmes là réalisent des classements, sélectionnent des informations et en déduisent un profil**, par exemple de consommation, qui est ensuite utilisé ou exploité commercialement. Ces algorithmes là sont utilisés pour l’orientation Post Bac en France, pour « retrouver ses amis » sur les réseaux sociaux… Ils sont aussi utilisés par la Police aux États-Unis dans les logiciels de reconnaissance faciale, ce qui fait débat, car les algorithmes qui sont utilisés comportent un certain nombre de **biais racistes et sexistes**. Claire Richard en parle d’ailleurs très bien dans son article sur le site socialter en décembre 2018. Il s’appelle, [« Intelligence Artificielle : la reconnaissance faciale est-elle raciste ? »](http://www.socialter.fr/fr/module/99999672/756/_ia__la_reconnaissance_faciale_est_elle_raciste_), et je vous invite à le lire.

Bref, le problème avec les algorithmes, la mathématicienne Cathy O’Neil le résume très bien dans son livre « Algorithmes : la bombe à retardement », qui est sorti en français en 2018. Voici ce qu’elle nous dit :

*« Aujourd’hui, les modèles mathématiques et les algorithmes prennent des décisions majeures, servent à classer et catégoriser les personnes et les institutions, influent en profondeur sur le fonctionnement des États sans le moindre contrôle extérieur. Et avec des effets de bords incontrôlables. […] Il s’agit d’un pouvoir utilisé contre les gens. Et pourquoi ça marche ? Parce que les gens ne connaissent pas les maths, parce qu’ils sont intimidés. »*

Eurêka ! Je ne sais pas pour vous, mais moi, en tout cas ça fait tilte ! Bah oui, les maths, moi pour ma part, je les ai tour à tour, détestées, craintes, ignorées, puis j’ai quand même dû apprendre à en dompter quelques fractions si j’ose dire, pour les utiliser dans ma vie quotidienne et mon travail. Et, jusqu’à la semaine dernière, je n’avais jamais utilisé d’algorithmes pour m’exprimer, et faire de la musique… un domaine où là, par contre, je me sens un peu plus à l’aise.

La semaine dernière donc, j’ai commencé à écrire et mixer des algorithmes avec un outil qui s’appelle « FoxDot » installé dans un logiciel libre qui s’appelle Supercollider. Concrètement, j’ai écrit des trucs sur mon clavier d’ordinateur, et ça générait de la musique. Je jouais avec d’autres personnes et on projetait sur un mur l’écran du logiciel pour que tout le monde voit ce qu’on était en train de faire (… enfin en train d’essayer de faire). Les personnes présentes pouvaient réagir à ce qu’on écrivait, et même danser, parce que la musique était entraînante. Elles pouvaient aussi copier le code écrit pour le refaire sur leur ordinateur et pour apprendre à jouer aussi ! Bref, j’ai testé un truc qui s’appelle le « live coding » (et bim, encore un mot compte triple !).

Alors concrètement, le live coding ça peut ressembler à ça :

**[extrait 1]**

à ça

**[extrait 2]**

et même à ça

**[extrait 3]**

Si, si je vous jure !

Quant à moi, perso, bah j’en suis encore là :

**[extrait 4]**

Mais ce n’est que le début, et ce petit pouvoir que j’ai pris sur les algorithmes et ben j’ai bien envie de l’additionner, de le multiplier et le partager avec d’autres personnes, en musique, en dansant. Bref, de prendre la tangente avec les algorithmes du big data et de trouver des dénominateurs communs, plutôt que des différences et des écarts types.

Et si vous aussi, alors à vous de jouer maintenant ! À vos claviers… 3, 2,1, codez !

Maëlle Vimont
