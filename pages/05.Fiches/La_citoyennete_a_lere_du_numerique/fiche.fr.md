---
title: 'La citoyenneté à l''ère du numérique'
media_order: img_6553.jpg
type_ressource: text
feature_image: img_6553.jpg
license: cc-by-sa
date: '22-10-2014 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'big brother'
        - 'edward snowden'
        - hackers
    author:
        - 'Azza Chaouch Bouraoui'
aura:
    pagetype: website
    description: 'Par Azza Chaouch Bouraoui, doctorante à l''Institut de Droit Public et Science Politique à l''Université de Rennes I, spécialisée en Droit International des conflits, membre du Hackerspace de Tunis «HackerSpace.TN» et du mouvement OpenGovTN de Tunis.'
    image: img_6553.jpg
metadata:
    description: 'Par Azza Chaouch Bouraoui, doctorante &agrave; l''Institut de Droit Public et Science Politique &agrave; l''Universit&eacute; de Rennes I, sp&eacute;cialis&eacute;e en Droit International des conflits, membre du Hackerspace de Tunis &laquo;HackerSpace.TN&raquo; et du mouvement OpenGovTN de Tunis.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/la_citoyennete_a_lere_du_numerique'
    'og:type': website
    'og:title': 'La citoyennet&eacute; &agrave; l''&egrave;re du num&eacute;rique | Espace Ressources Num&eacute;riques'
    'og:description': 'Par Azza Chaouch Bouraoui, doctorante &agrave; l''Institut de Droit Public et Science Politique &agrave; l''Universit&eacute; de Rennes I, sp&eacute;cialis&eacute;e en Droit International des conflits, membre du Hackerspace de Tunis &laquo;HackerSpace.TN&raquo; et du mouvement OpenGovTN de Tunis.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/la_citoyennete_a_lere_du_numerique/img_6553.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '620'
    'og:image:height': '165'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'La citoyennet&eacute; &agrave; l''&egrave;re du num&eacute;rique | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Par Azza Chaouch Bouraoui, doctorante &agrave; l''Institut de Droit Public et Science Politique &agrave; l''Universit&eacute; de Rennes I, sp&eacute;cialis&eacute;e en Droit International des conflits, membre du Hackerspace de Tunis &laquo;HackerSpace.TN&raquo; et du mouvement OpenGovTN de Tunis.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/la_citoyennete_a_lere_du_numerique/img_6553.jpg'
    'article:published_time': '2014-10-22T00:00:00+02:00'
    'article:modified_time': '2020-11-26T14:21:58+01:00'
    'article:author': Ping
---

*Par Azza Chaouch Bouraoui, doctorante à l'Institut de Droit Public et Science Politique à l'Université de Rennes I, spécialisée en Droit International des conflits, membre du Hackerspace de Tunis «HackerSpace.TN» et du mouvement OpenGovTN de Tunis.*

Nous abordons, le plus souvent, les nouvelles technologies de communication en tant que simples utilisateurs et consommateurs pour accéder à distance aux services et informations. L'utilisation de ces moyens de communication nous a, néanmoins, permis d'étendre notre existence dans l'espace virtuel. Nous sommes des personnages, des données fictives ou administratives et mêmes privées qui se mélangent entre elles, et qui constituent cette cybersphère qui nous entoure. En conséquence, il convient de se demander si nous ne sommes pas aussi des citoyens numériques, en plus d'être des consommateurs.

C'est à travers le soulèvement populaire en Tunisie, que l'on a pu prendre conscience de l'existence numérique du citoyen et de l'extension de ses droits et libertés. Cependant, cette émergence des droits et libertés dans l'espace numérique, fait face à l'émergence d'une surveillance vorace et incontrôlable qui consiste à stocker sans discrimination cette nébuleuse mondiale de données dans l'espace numérique.

## Le soulèvement populaire en Tunisie et le rôle d'Internet

Si on prend l'exemple de la Tunisie, qui a marqué l'histoire récente par un soulèvement populaire à la fin de l'année 2010, on remarquera l'omniprésence des nouvelles technologies de communication dans l'accompagnement des mouvements de révoltes. Des réseaux téléphoniques classiques à la 3G en passant par l'ADSL, les tunisiens, où qu'ils se trouvent, et le monde entier, ont pu suivre à travers Internet les événements et le développement de la situation. Internet, en particulier, a permis aux activistes de dénoncer et de publier, sans limite ni censure, les documents prouvant la corruption ou des vidéos montrant les exactions de l'ancien régime contre les protestants. Plusieurs sites internets, blogs, forums et réseaux sociaux ont relayé les différents documents, preuves et informations, jour par jour, heure par heure. C'est l'effet de cette nébuleuse de données se répartissant dans les serveurs et disques durs du monde entier qui a rendu toute tentative de censure gouvernementale impossible. C'est alors que l'on a compris que, s'il était facile de contrôler le contenu des médias classiques (journaux, radios, chaînes de télévision) et d'étouffer ce qui pouvait embarrasser le pouvoir en place, Internet n'était pas susceptible de subir un tel contrôle, à moins de couper de manière physique toute connexion. Certes, les autorités tunisiennes à l'époque disposaient de mécanismes technologiques leur permettant de surveiller le trafic Internet, mais la dispersion des données et le génie des cyberactivistes à masquer leurs traces a empêché l'instauration d'une censure totale.

La réussite de la résistance numérique en Tunisie, tout comme les prises de position d'Anonymous ou Telecomix pour soutenir plusieurs causes, a poussé plusieurs États, et surtout les grandes puissances, à prendre les dispositions nécessaires pour parer aux attaques cybernétiques et limiter le champ d'action dans l'espace numérique.

## Une extension numérique des droits et libertés...

Le droit de participation dans la vie politique de l'État est un droit octroyé à tout citoyen par la Déclaration Universelle des Droits de l'Homme adoptée par l'ONU en 1948, et consacré par un pacte relatif aux droits civils et politiques. Plusieurs constitutions nationales, dont les constitutions françaises et tunisiennes, consacrent cette catégorie de droits qui est la première garantie du caractère démocratique d'un État. Mais ces droits de participation à la vie politique, au fil du temps, se sont en grande partie limités au droit de vote. Sommes-nous citoyens juste lors des élections et de l'acquittement de nos impôts ?

Grâce à Internet, et aux autres moyens numériques, les citoyens ont pu bénéficier d'un nouvel espace, beaucoup plus étendu, pour exercer leurs droits et libertés. Que ce soit par des blogs, des forums, ou les différents réseaux sociaux, il est devenu très facile et pratique pour les individus de s'exprimer, de créer et de partager avec une grande partie des habitants de la Terre. De nombreuses organisations internationales se sont penchées sur l'importance d'adapter les textes de lois afin de protéger la liberté d'expression sur Internet, comme la liberté d'accès à Internet.

La plus grande concrétisation de la participation citoyenne au numérique est l' «open gouvernance», ou la gouvernance ouverte. La gouvernance consistant à faire participer les citoyens au processus des décisions politiques. Les moyens numériques permettent aux services publics de publier, de partager et de faire parvenir à tous les citoyens les informations concernant les budgets et les différents programmes d'action publique. Par ailleurs, le citoyen peut échanger avec l'administration publique par de simples sms ou des formulaires pour améliorer les services ou exprimer son avis quant aux décisions prises pour gérer les services publics.

Cet accès aux données publiques constitue la meilleure garantie pour la transparence dans la gestion des services publics, l'éradication de toute corruption ainsi qu'une meilleure efficience et performance. Pareillement, la participation à travers les nouveaux moyens de télécommunication rendent les échanges et les consultations des citoyens plus fréquentes, car plus rapides, que ce soit à travers des applications mobiles ou des sites internets.

## Contrebalancée par l'émergence d'une surveillance occulte et illimitée

Par ailleurs, l'éveil de la citoyenneté et de l'activisme dans la sphère numérique et la multiplication d'attaques de la part de hackers contre les sites gouvernementaux ou les bases de données sensibles de certains états (comme en Estonie en 2007 ou en Géorgie en 2008) a incité les grandes puissances à moderniser leurs défenses militaires afin de faire face aux «cybermenaces» pouvant causer des dégâts semblables à ceux qui résulteraient d'une attaque armée.

Depuis quelques années déjà, plusieurs activistes mènent des campagnes sur la toile afin de sensibiliser les internautes à l'importance des données que l'on soumet à Internet. Car les serveurs sur lesquels les utilisateurs stockent leurs données, aussi privées et sensibles soient-elles, ne comportent pas toujours des garanties sur les tiers qui pourraient accéder à ces données. Et c'est ce qui a été démontré par Edward Snowden, un cadre de la NSA, l'Agence de Sécurité Américaine chargée de défendre le pays entre autres des «cybermenaces».

Ce qui a scandalisé une bonne partie des activistes et des farouches défenseurs de la vie privée, est que la NSA s'était octroyée le pouvoir légal et légitime d'accéder aux diverses données stockées par les utilisateurs des géants des réseaux sociaux, de messagerie instantanée et électronique, et de cloudcomputing.

Face à la puissance de l'anonymat sur Internet, les forces sécuritaires et militaires des états n'ont pas trouvé d'autres recours que de se frayer un accès non-discriminant aux contenus et données des internautes du monde entier pour éviter et anticiper les différentes attaques pouvant être planifiées par les terroristes.

Si le reste des citoyens ne s'indignent pas de ce genre de mesure, nous ne devrions pas nous étonner si le Big Brother de George Orwell devient une réalité. Et il faudra toujours garder à l'esprit cette citation de Benjamin Franklin : *« Un peuple prêt à sacrifier un peu de liberté pour un peu de sécurité ne mérité ni l'une ni l'autre, et finit par perdre les deux. »*
