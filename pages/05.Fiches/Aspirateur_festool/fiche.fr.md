---
title: 'Aspirateur Festool'
media_order: 759px-Aspi.jpg
type_ressource: text
feature_image: 759px-Aspi.jpg
license: cc-by-nc-sa
date: '01-01-2018 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Outil servant à aspirer les poussières de bois'
    image: 759px-Aspi.jpg
metadata:
    description: 'Outil servant &agrave; aspirer les poussi&egrave;res de bois'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/aspirateur_festool'
    'og:type': website
    'og:title': 'Aspirateur Festool | Espace Ressources Num&eacute;riques'
    'og:description': 'Outil servant &agrave; aspirer les poussi&egrave;res de bois'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/aspirateur_festool/759px-Aspi.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '759'
    'og:image:height': '600'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Aspirateur Festool | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Outil servant &agrave; aspirer les poussi&egrave;res de bois'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/aspirateur_festool/759px-Aspi.jpg'
    'article:published_time': '2018-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-25T16:48:06+01:00'
    'article:author': Ping
---

*Cet outil sert à aspirer le bois et **uniquement** le bois ! Pour les autres déchets, n'utiliser que l'autre aspirateur (KARCHER), et pour les poussières de bois, n'utiliser que celui là (FESTOOL).*

Il est possible, et de ce fait, obligatoire, de le brancher sur les appareils électroportatifs (défonceuse, scie sauteuse, scie circulaire,...) pendant leur fonctionnement.

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Aspirateur_Festool?classes=btn)
