---
title: Laser-Smoothignstech
media_order: Smoothisnstech.jpg
type_ressource: text
feature_image: Smoothisnstech.jpg
license: cc-by-nc-sa
date: '01-01-2016 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
        - 'découpe laser'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cette machine sert à la découpe laser.'
    image: Smoothisnstech.jpg
metadata:
    description: 'Cette machine sert &agrave; la d&eacute;coupe laser.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/laser_smoothignstech'
    'og:type': website
    'og:title': 'Laser-Smoothignstech | Espace Ressources Num&eacute;riques'
    'og:description': 'Cette machine sert &agrave; la d&eacute;coupe laser.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/laser_smoothignstech/Smoothisnstech.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '500'
    'og:image:height': '375'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Laser-Smoothignstech | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine sert &agrave; la d&eacute;coupe laser.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/laser_smoothignstech/Smoothisnstech.jpg'
    'article:published_time': '2016-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:32:12+01:00'
    'article:author': Ping
---

*Cette machine sert à la découpe laser.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Laser-Smoothignstech?classes=btn)
