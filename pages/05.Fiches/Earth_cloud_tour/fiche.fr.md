---
title: 'Earth, cloud, tour'
type_ressource: text
feature_image: c8lw7yiuwaaf7_n.jpglarge.jpeg
license: cc-by-nc-sa
date: '18-04-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - données
        - cloud
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Récit d''une visite-rencontre autour d''un Data Center, lors des rencontres Parcours Numériques 2017.'
    image: c8lw7yiuwaaf7_n.jpglarge.jpeg
metadata:
    description: 'R&eacute;cit d''une visite-rencontre autour d''un Data Center, lors des rencontres Parcours Num&eacute;riques 2017.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/earth_cloud_tour'
    'og:type': website
    'og:title': 'Earth, cloud, tour | Espace Ressources Num&eacute;riques'
    'og:description': 'R&eacute;cit d''une visite-rencontre autour d''un Data Center, lors des rencontres Parcours Num&eacute;riques 2017.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/earth_cloud_tour/c8lw7yiuwaaf7_n.jpglarge.jpeg'
    'og:image:type': image/jpeg
    'og:image:width': '1200'
    'og:image:height': '900'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Earth, cloud, tour | Espace Ressources Num&eacute;riques'
    'twitter:description': 'R&eacute;cit d''une visite-rencontre autour d''un Data Center, lors des rencontres Parcours Num&eacute;riques 2017.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/earth_cloud_tour/c8lw7yiuwaaf7_n.jpglarge.jpeg'
    'article:published_time': '2017-04-18T00:00:00+02:00'
    'article:modified_time': '2020-11-26T13:48:22+01:00'
    'article:author': Ping
---

*Récit d'une visite-rencontre autour d'un Data Center, lors des rencontres Parcours Numériques 2017.*

Tibo Labat est architecte, sa démarche se situe à la jonction de l'art de l'architecture et de l'écologie. Dans la suite de sa formation d’architecte et à la croisée de ses recherches, il s’intéresse aujourd’hui plus particulièrement aux notions de réemploi et de métabolisme urbain. En rejoignant Matière Sociale, une association qui vise le réemploi des déchets du BTP, il amène sa connaissance du monde de l’aménagement, et ses compétences en coordination de projet de médiation et de micro-architecture.

Dans le cadre des quinzièmes rencontres Parcours Numériques, jeudi 30 mars 2017, Tibo nous embarque pour un voyage dans le Cloud, celui dont on parle comme s'il s'agissait d'une entité au dessus de nos têtes, flottant quelque part dans l'espace éthéré de la dématérialisation. Et pourtant c'est à une marche de plus d'heure et demi en direction d'un data center -en français une ferme de serveur- que nous sommes conviés. Une randonnée jusqu'aux lieux de stockage de nos données.
Une ferme de serveur donc qui contient des serveurs. Mais, un serveur c'est quoi au juste ? En réalité, un serveur, c'est un ordinateur, tout simplement. Une ferme de serveur c'est donc ni plus ni moins un ensemble de plusieurs ordinateurs, parfois des centaines, des milliers, qui, associés les uns aux autres permettent une puissance de calcul et de stockage de données conséquents.
Et ces fermes de serveurs, ces grappes de serveurs, que l'on croit n'exister qu'aux USA ou dans les pays du nord, sont aussi tout près de chez nous… le Cloud c'est près de chez vous !

![image.1a6byy.png](image.1a6byy.png)

À Nantes, il y a cinq fermes de serveurs identifiées. Parmi elles, une est gérée par l'entreprise Oceanet Technology tout près de Nantes, sur la commune de Saint Herblain. C'est celle que nous partons visiter.
Une équipe de journaliste de France 3 nous suit également, du moins au début, car nous allons jusqu'au cloud à pieds afin de mieux ressentir la matérialité de cette technologie. Une quête jusqu'à la ferme des serveurs. Les journalistes sont là dans le cadre d'un reportage sur les pollutions invisibles. Alors les données, pollutions invisibles ?

![image.ohvkyy.png](image.ohvkyy.png)

Dans les échanges animés par Tibo Labat qui ont lieu en cours de la marche, on peut entendre : « on reçoit, je pense, 100 mails de spams par jour … c'est une pollution planétaire mais aussi une pollution de temps. » En contre point et pour se rassurer que tout cela n'est pas vain : « La communication par mail, ça a ouvert beaucoup de choses. » enfin pour nuancer à nouveau : « la difficulté, c'est la démultiplication des sources d'information. » où il serait question de l'usage, bon ou mauvais, à faire des technologies numériques.
Ce chemin de pénitence jusqu'au data center est l'occasion de remettre en question ses pratiques, d'expier ses fautes, mais aussi de renouer avec une approche plus terre à terre avec le Cloud. Tibo évoque avec nous la question des flux de matières dans une ville en s'appuyant sur le cas des téléphones portables : assemblés en Chine dans des usines à partir de minerais extraits en Afrique  dans des conditions peu enviables, il se demande jusqu'où s'étend la sphère d'influence d'une ville comme Nantes ? S'en suit un échange sur la technique qui est invisible sauf quand il y a un problème, sur le fait que le Cloud permet toute cette mobilité, cette dynamique que l'on connaît actuellement, mais que tout ça reste bien opaque quand même.

![dsc05484light.jpg](dsc05484light.jpg)

Enfin c'est l'arrivée dans la ferme de serveurs ! Oceanet Technology possède six data centers uniquement en Pays de la Loire (ils appellent leurs fermes de serveurs des datas centers peut-être pour éviter la connotation paysanne de la version Française). La partie visitée fait 4000m2 dont une salle blanche de 600m2 consacrée au stockage des données, et deux locaux techniques. Le représentant d'Oceanet Technology qui nous guide dans les allées de la ferme explique qu'ils ont essentiellement une activité de location de stockage de données : « on fait limite de l'immobilier » lance t-il.
Si jamais un local technique tombe en panne et interdit l'accès à des quantités de données, l'autre prend le relais. En somme le data center tourne toujours !
En plus de ses besoins en énergie, ceux en terme de climatisation sont très importants, même si aujourd'hui d'énormes progrès ont pu être fait à ce niveau là. De plus, les bureaux sont chauffés avec la ferme de serveurs ici (un peu comme dans l'ancien temps les fermes et les bêtes pouvaient chauffer les parties habitées par les humains au-dessus d'elles).
Tout cela ne serait rien ou peu sans la boucle de fibre optique de 70km qui entoure Nantes et qui permet aux données de circuler à une vitesse proche de celle de la lumière, en théorie…

![c8lw7yiuwaaf7_n.jpglarge.jpeg](c8lw7yiuwaaf7_n.jpglarge.jpeg)

La balade se termine autour de quelques retours rapides quant à cette immersion au cœur du Cloud. On en retiendra essentiellement des termes techniques lors de la visite, ainsi qu'une importance accordée à la sécurisation de tels lieux et qui renvoie aux différentes actualités à propos des données privées notamment. Sur les questions de pollutions invisibles, les intuitions des marcheurs sont parfois confirmées par les aveux de l'entreprise au sujet de la consommation énergétique nécessaire au bon fonctionnement des serveurs, et parfois, elles restent à un stade de réflexion et ne demandent qu'à être développées, confirmées ou infirmées par des faits objectifs. Peut-être que ces investigations pourront être poursuivies lors de prochaines rencontres ou occasions dans le cadre de Parcours Numériques afin de mieux cerner encore les contours du Cloud et de nos pratiques numériques.
