---
title: 'Démarrer de zéro sur un sujet technique'
type_ressource: text
feature_image: Researchlog_Liens-1.png
license: cc-by-nc-sa
date: '22-03-2017 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - fablab
        - 'agriculture urbaine'
        - biolab
        - 'recherche ouverte'
    author:
        - 'Michka Mélo'
aura:
    pagetype: website
    description: ' Michka Mélo, bio-ingénieur, explorateur associé à PiNG sur le programme de recherche-action « Agriculture urbaine et fablabs », raconte sa première journée de recherche sur un nouveau sujet, l’analyse du sol.'
    image: Researchlog_Liens-1.png
metadata:
    description: ' Michka M&eacute;lo, bio-ing&eacute;nieur, explorateur associ&eacute; &agrave; PiNG sur le programme de recherche-action &laquo; Agriculture urbaine et fablabs &raquo;, raconte sa premi&egrave;re journ&eacute;e de recherche sur un nouveau sujet, l&rsquo;analyse du sol.'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/demarrer_de_zero_sur_un_sujet_technique'
    'og:type': website
    'og:title': 'D&eacute;marrer de z&eacute;ro sur un sujet technique | Espace Ressources Num&eacute;riques'
    'og:description': ' Michka M&eacute;lo, bio-ing&eacute;nieur, explorateur associ&eacute; &agrave; PiNG sur le programme de recherche-action &laquo; Agriculture urbaine et fablabs &raquo;, raconte sa premi&egrave;re journ&eacute;e de recherche sur un nouveau sujet, l&rsquo;analyse du sol.'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/demarrer_de_zero_sur_un_sujet_technique/Researchlog_Liens-1.png'
    'og:image:type': image/png
    'og:image:width': '910'
    'og:image:height': '615'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'D&eacute;marrer de z&eacute;ro sur un sujet technique | Espace Ressources Num&eacute;riques'
    'twitter:description': ' Michka M&eacute;lo, bio-ing&eacute;nieur, explorateur associ&eacute; &agrave; PiNG sur le programme de recherche-action &laquo; Agriculture urbaine et fablabs &raquo;, raconte sa premi&egrave;re journ&eacute;e de recherche sur un nouveau sujet, l&rsquo;analyse du sol.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/demarrer_de_zero_sur_un_sujet_technique/Researchlog_Liens-1.png'
    'article:published_time': '2017-03-22T00:00:00+01:00'
    'article:modified_time': '2020-11-26T11:22:09+01:00'
    'article:author': Ping
---

*Récit méthodologique d’une journée de recherche : Michka Mélo, bio-ingénieur, explorateur associé à PiNG sur le programme de recherche-action « Agriculture urbaine et fablabs », raconte sa première journée de recherche sur un nouveau sujet, l’analyse du sol.*
*L’objectif de cet article est de montrer une méthode permettant de s’approprier un sujet technique en autonomie, sans pré-requis particulier, et d’inviter les lecteur⋅rices à en faire de même lors d’un atelier de recherche ouverte le 21 avril prochain, au fablab Plateforme C.*

## DANS LES STARTING-BLOCKS

Il est 8h58. Je suis assis devant l’ordinateur, un thermos de thé à portée de main. Dans deux minutes, je m’apprête à relever un des défis proposés par PiNG dans le cadre de notre exploration jointe des croisements entre l’agriculture urbaine et les fablabs.
Quel défi ? Évaluer, en une quinzaine d’heures, la faisabilité d’un kit d’analyse permettant aux agriculteurs et aux jardiniers urbains de connaître la qualité de leur sol par eux-mêmes, en s’appuyant si besoin sur l’équipement et l’expertise de la communauté des fablabs.

J’ai décidé d’amorcer ce défi par une première tranche de huit heures, afin d’évaluer rapidement la situation, de glaner quelques premières informations-clés, et de garder un peu de temps pour préparer un atelier de recherche ouverte sur le sujet, qui aura lieu le 21 avril prochain à Plateforme C.
Pour mettre le plus à profit ce temps très court, j’ai choisi d’utiliser la méthode [Pomodoro](https://fr.wikipedia.org/wiki/Technique_Pomodoro), qui préconise de structurer son travail en séquences concentrées et intensives de 25 minutes, suivies de 5 minutes de pause, où il est recommandé de se lever, de bouger un peu, par exemple pour remplir à nouveau le thermos, aller contempler Bruxelles par la fenêtre de l’appartement, ou encore faire un rapide exercice de méditation pour régénérer sa concentration.
Pour éviter tout risque de procrastination, nous synchronisons nos Pomodoros avec Karine, amie lyonnaise qui travaille sur sa thèse. Nous échangeons donc brièvement par le chat Skype durant les courtes pauses, que nous nous signalons mutuellement. Nous partageons également nos objectifs en début de chaque Pomodoro, pour garder la pression sur la productivité.
Neuf heures sonnent. Une brève ligne à Karine pour partager avec elle mon premier objectif : lire l’article Wikipédia intitulé “[Analyse du sol](https://fr.wikipedia.org/wiki/Analyse_de_sol)”, et identifier dans les sources les lectures ultérieures les plus pertinentes. Et c’est parti.

## ON DÉMARRE AVEC WIKIPÉDIA

Wikipédia est une source parfois critiquée pour son inexactitude, mais elle reste souvent un excellent point de départ. Un nombre croissant d’articles sont bien référencés, et on peut donc, en vérifiant les sources, savoir si ce qu’on lit est fiable ou pas.
Ce premier article sur l’analyse est très générique, et distingue l’analyse de la qualité agronomique d’un sol de la présence d’éventuels polluants.
L’article met en lumière la complexité de l’analyse du sol, et me fait rapidement comprendre que nous allons devoir prioriser ce que nous voulons mesurer et pourquoi. J’en profite donc pour mettre de côté la qualité agronomique du sol pour l’instant, la priorité étant de donner aux jardiniers et agriculteurs urbains les outils pour ne pas empoisonner leur production, plus que pour en optimiser la croissance.
J’enchaîne donc avec un second article de Wikipédia intitulé “[Pollution des sols](https://fr.wikipedia.org/wiki/Pollution_des_sols)”, listé dans les articles connexes du premier. Celui-ci rentre plus dans le vif du sujet, avec une longue liste de polluants, des références à des normes nationales et européennes, des institutions, des laboratoires, etc.

## TENIR UN “RESEARCH LOG”

Je commence donc à remplir le petit “research log” que j’ai pris l’habitude d’initier à chaque recherche documentaire. C’est un document très basique, de type fichier texte, dans lequel je copie-colle les liens hypertextes de pages visitées avant de prendre mes notes de lecture directement dessous.
J’utilise également un système de balises, entre crochets, grâce auxquelles je peux retrouver, par la magie du Ctrl + F, un certain type de contenu par la suite.

![Reserachlog balises](Researchlog_Balises.png)

Je commence à marquer d’un [REF] les éléments de références potentiels, comme par exemple la base de donnée des sols “BASOL” du ministère de l’écologie, mentionnée dans l’article.
Un peu plus loin je pose mon premier coup de [<3] pour le Samu de l’environnement, une flotte de laboratoires mobiles régionaux qui peuvent se rendre sur un site donné et quantifier très rapidement près de 200 substances dans la nature (à quand une version associative et indépendante pour suivre de près nos sites industriels ?).
J’ouvre également à cette occasion deux sections en bas de mon “research log”, intitulées :
– “Lire ensuite”, où je copie-colle [le lien](https://fr.wikipedia.org/wiki/Samu_de_l'environnement) vers l’article Wikipédia dédié au Samu de l’environnement.
– “Idées”, dans laquelle je vais lister, au fil de la lecture, les idées qui me viennent par rapport à ce que nous pourrions développer sur ce thème dans le cadre de notre exploration associée avec PiNG.

![Reserachlog liens](Researchlog_Liens.png)

La fin du premier Pomodoro sonne avant la fin de la lecture de cet article. Je prends donc une courte pause et reprend la lecture à 9h30. J’enchaîne sur la lecture du court article Wikipédia sur le Samu de l’environnement, et c’est au tout début du troisième Pomodoro que je commence à explorer les liens externes et les références issues des articles lus, et collectés dans ma section “Lire ensuite”.

![Reserachlog liens1](Researchlog_Liens-1.png)

## SORTIR DU PORT-WIKIPÉDIA VERS LES MERS D’INTERNET

La visite du site du [Samu de l’environnement](http://www.samudelenvironnement.fr/main/) sera rapide : aucune information ou presque, rien de technique en tout cas. Quelques liens morts dans les références de Wikipédia suite à une restructuration du site du ministère de l’écologie me forcent à aller rechercher quelques publications par mots-clé sur un moteur de recherche (j’utilise [Startpage](https://www.startpage.com/), qui s’appuie sur Google en préservant votre anonymat). Et là je tombe sur une première grosse perle : un [volumineux guide méthodologique](http://infoterre.brgm.fr/rapports/RP-50128-FR.pdf) du Bureau de Recherche Géologique et Minière (BRGM) indiquant comment analyser un sol pollué, disponible intégralement en PDF.
Ce guide est très exhaustif, avec de nombreux détails sur les méthodes d’analyses de nombreux polluants. Ce document est enthousiasmant, tant il est riche en informations, mais également un peu déprimant : l’analyse du sol se préfigure comme une science très complexe, dépendant de nombreux facteurs. Ça ne va pas être une mince affaire de tout bien comprendre, tout bien vulgariser, et enfin simplifier pour rendre les méthodes abordables techniquement et financièrement à des non-spécialistes. Il va falloir commencer par quelque chose de plus simple.
Je consacre donc le quatrième Pomodoro à une recherche par mots-clés pour tenter de trouver des méthodes nécessitant moins d’équipement. Je commence avec “détecter métaux lourds bas coût”, et je trouve quelques petites choses :
– [Oligoscan](https://www.psiram.com/fr/index.php/OLIGOSCAN), pour commencer, un spectromètre portatif permettant soi-disant (la source que je lis est critique) de quantifier les métaux lourds présents dans l’organisme.
– Je trouve aussi un [kit d’indicateurs colorimétriques](http://www.bioperfection.com/metaux-lourds/test.htm) assez onéreux (quelques centaines d’euros) permettant possiblement de faire la même chose.
– Je trouve enfin [une machine très complexe](http://www.mesureo.com/pages/Analyseur_Metaux_Lourds.html), qui permet de mesurer les métaux lourds par voltamétrie.
Peu de détails sur les techniques sous-jacentes à ces trois objets, je note toutefois les termes “spectrométrie” et “voltamétrie” pour la suite. Mon Pomodoro s’achève, et je décide de changer de stratégie.

## VIRAGE DE BORD STRATÉGIQUE

Afin de prioriser quels sont les polluants les plus susceptibles de nous empoisonner, il serait intéressant de savoir lesquels on retrouve le plus fréquemment dans nos légumes. Je me mets donc en quête, pour ce cinquième Pomodoro, d’études sur le sujet, à nouveau via une recherche par mots-clés.
Je tombe alors sur plusieurs articles de médias en lignes dédiés à l’écologie. Ces articles relatent rapidement le résultat d’études plus avancées. Ils peuvent être imprécis et incomplets, mais ils sont souvent écrits pour le grand public, et peuvent donc facilement nous permettre d’évaluer la pertinence de rentrer ou non dans le détails des publications scientifiques qu’ils relatent.
[Un premier article de Consoglobe](http://www.consoglobe.com/polluants-plus-frequents-alimentation-cg) rapporte une volumineuse études faite par l’ANSES sur près de 20 000 produits alimentaires issus de 1 500 points de vente. Parmi le sous-groupe relevé par Consoglobe au sein des 445 substances nocives identifiées, on retrouve au final relativement peu de polluants qui semblent venir d’une pollution du sol contaminant des produits agricoles. On voit surtout des produits liés aux emballages, des produits nocifs apparaissant lors de la transformation des aliments, ou encore des pesticides.
[D’autres articles](http://www.consoglobe.com/potagers-urbains-quels-risques-pollution-cg) relatent une étude de l’université technique de Berlin (TU Berlin), plus au cœur de nos préoccupations : elle essaie d’évaluer l’effet de la circulation automobile sur la pollution des légumes aux métaux lourds, effet qui semble non négligeable. L’étude semble toutefois contestée méthodologiquement par d’autres scientifiques, d’après ces mêmes articles qui en parlent.
Après quatre ou cinq Pomodoros, il est recommandé de faire une pause d’une demi-heure à faire tout autre chose. Je pars donc faire la vaisselle.

## PREMIER ARTICLE SCIENTIFIQUE

J’attaque ensuite le sixième Pomodoro par le lecture de l’étude de [TU Berlin](https://www.researchgate.net/profile/Ina_Saeumel/publication/221974685_How_healthy_is_urban_horticulture_in_high_traffic_areas_Trace_metal_concentrations_in_vegetable_crops_from_plantings_within_inner_city_neighbourhoods_in_Berlin_Germany/links/0fcfd50c836e0cd427000000/How-healthy-is-urban-horticulture-in-high-traffic-areas-Trace-metal-concentrations-in-vegetable-crops-from-plantings-within-inner-city-neighbourhoods-in-Berlin-Germany.pdf), que je trouve grâce à la référence mentionnée dans un des articles qui en parlaient, via le moteur de recherche [Google Scholar](http://scholar.google.fr/), spécialisé dans la recherche de littérature scientifique. Par chance, il est disponible en libre accès sur le réseau social de chercheurs [Researchgate](http://researchgate.net/).
C’est le premier article scientifique que je parcours dans le cadre de ce défrichage. J’apprends ainsi, dans la section “Matériel et méthodes”, que les chercheurs ont utilisé la spectroscopie à absorption atomique pour quantifier les métaux lourds dans le sol.
Je vois aussi que les auteurs ont cité de nombreuses références d’études préliminaires dans leur introduction.
Dans la section “Discussion”, enfin, ils comparent les résultats qu’ils obtiennent à ceux obtenus par d’autres chercheurs, ce qui permet de révéler encore plus (si c’était nécessaire) à quel point l’interaction sol-plante-polluant est complexe : ça dépend des légumes choisis, des paramètres du sol, des polluants présents et de leurs éventuelles interactions entre eux. Les résultats des études semblent varier du tout au tout dès qu’un des paramètres change, et toutes les études ne suivent pas l’évolution de tous les paramètres…
Je copie-colle entre guillemets dans mon “research log” des extraits de l’article, comme une citation. Cela me permet de les distinguer de mes notes, et de les citer explicitement si nécessaire, lorsque je ferais mon rapport de recherche sur le wiki [fablabo.net](http://www.fablabo.net/).

## CAP SUR LE DIY

Je décide de consacrer mon septième Pomodoro à l’exploration du monde de la spectroscopie “do it yourself”. Je tombe assez rapidement sur le site [PublicLab](https://publiclab.org/), que je ne connaissais pas, mais qui est très intéressant. Il permet de répertorier sur une même page les équipements et les expériences qui ont été effectuées avec cet équipement. On trouve aussi bien les instructions pour tout faire soi-même, que des kits à commander. Enfin, il apparaît très clairement sur la page quand les expériences ont été répliquées ou non.
Je trouve donc [un spectroscope sur PublicLab](https://publiclab.org/wiki/spectrometry) à bas coût et à fabriquer soi-même, basé entre autres sur une webcam et un DVD-R. Malheureusement, cette trouvaille intéressante est relativisée un peu plus tard car il semblerait (d’après un cours universitaire sur la spectroscopie) que nous ayons besoin, pour analyser nos métaux lourds, d’un atomiseur : une machine plus complexe qui transforme nos substances à analyser de manière à les rendre lisibles par le spectroscope.

## (RE)PRISE DE HAUTEUR

Je décide donc, pour mon huitième Pomodoro, de revenir à la question des polluants dans l’alimentation, et de me consacrer à la lecture d’un rapport prometteur trouvé un peu plus tôt lors d’une recherche par mots-clés. Ce rapport, publié par Greenloop à destination de l’Institut Bruxellois pour la Gestion de l’Environnement (IBGE), adresse précisément la question de “l’incidence de la pollution urbaine sur la production alimentaire en ville”. En plein dans le mille !
Et en effet, je ne suis pas déçu par la lecture. Ce bref rapport – une vingtaine de pages, une grosse trentaine avec les annexes – donne précisément le type d’informations que je recherchais : les éléments pour prioriser les polluants les plus problématiques, et les stratégies (y compris les plus simples) à mettre en œuvre pour les neutraliser.
J’apprends ainsi que les deux métaux lourds les plus problématiques sont le plomb et le cadmium, ou encore que les métaux semblent souvent plus problématiques que les polluants organiques. Le rapport regorge également de conseils comme le fait de se méfier des récipients de jardins en zinc galvanisés (source de cadmium), de la peinture écaillée (pouvant contenir des polluants organiques ou du plomb), d’éviter de cultiver en bordure immédiate des bâtiments, etc.
Je décide donc de consacrer deux Pomodoros de plus à la lecture complète du rapport, ce qui n’est clairement pas du temps perdu, étant donné que c’est un excellent condensé d’informations.
À l’issue de cette lecture instructive, ce sont déjà dix Pomodoros, soient cinq heures, consacrés à la recherche. Je décide donc de me lancer dans la documentation.

## DOCUMENTATION

Je commence donc par créer une page sur [Fablabo](http://fablabo.net/wiki/Analyse_du_sol_%C3%A0_faire_soi_m%C3%AAme), et d’en définir la structure.
Pour cela, je reprends mon “research log” depuis le début, je refais un point sur le type d’informations trouvées, et je me mets à la place d’un.e futur chercheur.e souhaitant investiguer sur le sujet. Qu’est-ce qui l’intéresserait ? Comment organiser l’information de façon pertinente pour lui/elle ? En tout cas pas de façon chronologique… Comme vous l’avez vu, la route était sinueuse, et je me suis perdu dans quelques voies de garage.
Je décide donc de structurer ma page en quatre grandes parties :
– une première mise en contexte, pour que le/la chercheur.e comprenne notre point de départ, et notre question initiale – entre autres la focalisation sur les polluants au détriment de la qualité agronomique du sol ;
– une section sur les polluants, dans le sol et dans l’alimentation ;
– une section sur les méthodes d’analyses ;
– une section sur les pistes de projets que nous pourrions mettre en œuvre avec PiNG par la suite.
Je commence ensuite à rédiger, le plus succinctement possible et en m’appuyant au maximum sur les références. Au fur et à mesure que je pars à la pêche dans mon “research log”, je remarque des éléments qui ne rentrent pas dans les cases de la structure initiale. Je la fais donc évoluer, jusqu’à ce qu’elle atteigne sa version actuelle, visible sur la page.
La rédaction est longue. Il se dit en école d’ingénieurs que la documentation correspond en moyenne à 50 % du temps de travail. Je n’échapperai pas à la règle dans ce cas, puisque c’est au final quatre heures de travail qui me permettront, à partir de mes notes, de produire la page Fablabo.
**Pourquoi prendre des notes chronologiques brouillonnes, et ne faire une documentation lisible qu’à la fin ?**
On peut se poser la question de la pertinence de commencer par prendre des notes chronologiquement, puis de tout rédiger en structurant thématiquement, ce qui induit un gros travail de transcription entre les notes initiales et la documentation finale. Après plusieurs essais, c’est toutefois cette méthode qui me semble la plus efficace en termes de temps.
Tout structurer thématiquement trop tôt dans le processus de recherche signifie prendre le risque de partir sur une structure inadaptée au contenu global que nous allons vouloir documenter au final. Cela signifie donc que l’on va être amené à réviser la structure (parfois plusieurs fois) au fil de la recherche, ce qui nous fait perdre du temps à tout redistribuer, et à re-rédiger certaines parties en suivant les nouveaux découpages thématiques.
De plus, je trouve très difficile, sans la hauteur de vue de plusieurs heures de recherche, de savoir quels détails méritent d’être documentés ou non. Produire une documentation finale dès le départ de la recherche tend à mon goût à produire une documentation beaucoup trop détaillée, potentiellement fastidieuse à parcourir, et d’autant plus longue à produire.
Enfin, vouloir faire une documentation structurée thématiquement dès le départ réduit à mon sens la fluidité du processus de recherche. Je trouve que la recherche (procédant par rebonds, nécessitant de la créativité et de la réflexivité) et la rédaction (avant tout de la structuration, et nécessitant de la clarté) sont deux états d’esprits assez distincts, et qu’il est assez fatigant de passer constamment de l’un à l’autre. Je trouve donc plus efficace de distinguer au maximum la phase de recherche de la phase de rédaction.

## LA SUITE AVEC VOUS ?

Comme vous l’aurez compris, le développement d’un kit d’analyse des polluants du sol à destination des jardinier.e.s et agriculteur.rice.s urbain.e.s en est encore à un stade plus qu’embryonnaire. Nous sommes encore, après huit heures de travail, en train d’évaluer quels sont les polluants les plus importants à dépister, et à comprendre quelles méthodes vont nous permettre de le faire à l’échelle d’un fablab, et sans pré-requis nécessaire.
C’est pourquoi nous souhaiterions vous inviter à venir apporter, si vous le souhaitez, votre pierre à l’édifice, le 21 avril prochain à Plateforme C, de 9h à 18h.
Nous y mènerons un atelier de recherche ouverte sur cette question de la détection des polluants du sol par les jardinier.e.s et agriculteur.rice.s urbain.e.s.
Partant de ces quelques premières heures de recherche, nous explorerons plus avant certaines des pistes de projet listées dans la page Fablabo, ou d’autres qui pourraient vous venir à l’esprit d’ici là ou durant l’atelier.
Nous vous proposerons une méthode de recherche, dont les grands traits sont tracés dans cet article, et qui, selon nous, permet à tou.te.s, sans pré-requis, d’aborder des questions techniques parfois complexes, en toute autonomie.
Que faire quand on tombe sur des mots et des concepts  compliqués ?
Comment comprendre “la science” si on est non-scientifique ?
Comment simplifier des systèmes techniques jusqu’à pouvoir les fabriquer, les régler, et les utiliser nous-même ?
Cet atelier a pour vocation de vous donner des outils concrets pour aborder ces questions, de vous autonomiser dans le développement de vos propres outils techniques, et de vous permettre de devenir un.e “expert.e indépendant.e” sur les thématiques qui vous tiennent à cœur.
Soyez donc les bienvenu.e.s le 21 avril prochain à Plateforme C pour contribuer à permettre à tou.te.s de cultiver des aliments sains en ville. Les inscriptions sont par ici !

Un article de Michka Mélo, bio-ingénieur et explorateur-associé de PiNG
