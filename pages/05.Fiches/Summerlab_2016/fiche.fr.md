---
title: 'Le Summerlab 2016 en vidéo'
media_order: 615385975.jpg
type_ressource: video
license: cc-by-nc-sa
date: '01-07-2016 00:00'
taxonomy:
    category:
        - recits
    tag:
        - summerlab
    author:
        - 'Vincent Pouplard'
aura:
    pagetype: website
    description: 'Le summerlab 2016, filmé par le réalisateur Vincent Pouplard'
    image: 615385975.jpg
metadata:
    description: 'Le summerlab 2016, film&eacute; par le r&eacute;alisateur Vincent Pouplard'
    'og:url': 'https://test.ressource.pingbase.net/test2/fiches/summerlab_2016'
    'og:type': website
    'og:title': 'Le Summerlab 2016 en vid&eacute;o | Espace Ressources Num&eacute;riques'
    'og:description': 'Le summerlab 2016, film&eacute; par le r&eacute;alisateur Vincent Pouplard'
    'og:image': 'https://test.ressource.pingbase.net/test2/fiches/summerlab_2016/615385975.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '854'
    'og:image:height': '480'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Le Summerlab 2016 en vid&eacute;o | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Le summerlab 2016, film&eacute; par le r&eacute;alisateur Vincent Pouplard'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://test.ressource.pingbase.net/test2/fiches/summerlab_2016/615385975.jpg'
    'article:published_time': '2016-07-01T00:00:00+02:00'
    'article:modified_time': '2020-11-26T16:40:11+01:00'
    'article:author': Ping
---

*Vincent Pouplard est réalisateur. Son travail documentaire s’articule autour de récits de vie et d’expérience. Il a récemment signé PAS COMME DES LOUPS dont la sortie en salles est prévue pour le mois d’avril 2017.*

<iframe src="https://player.vimeo.com/video/186338838" width="100%" height="450" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
